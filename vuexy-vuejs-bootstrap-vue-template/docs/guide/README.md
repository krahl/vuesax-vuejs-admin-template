# Welcome

Thanks for purchasing our template 🙂. Let's get you up and running.

::: danger Hey! Check this out before going anywhere 📣
This documentation is for **Bootstrap Vue version(Latest)**. If you want to check documentation for **Vuesax version(Deprecated)** please visit [here](https://www.pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/documentation-vuesax-version/). For **Vuesax version(Deprecated) Demo** please visit [here](https://pixinvent.com/demo/vuesax-vuexy-vuejs-admin-dashboard-template/demo-1/)
:::

## How to use docs

This is docs for Vuexy BootstrapVue version. In this documentation you will find new features, components, composition functions, etc provided by our template other than BootstrapVue.

::: tip
You can find BootstrapVue component's docs at their official [documentation](https://bootstrap-vue.org/).
:::

Let's install your project first, please visit [installation](/guide/development/installation.md) page for detailed guide on how to install and run vuexy.

To get started you can check our new improved [folder structure](/guide/development/folder-structure.md) of our template to understand how it works.

If you want to quickly update the UI of our template just like you do using customizer, use our themeConfig file for it. You can find documentation for it [here](/guide/development/theme-configuration.md).

Next, certainly you want to create new pages and want to get started with your project, For this you can check our [Layout docs](/guide/layout/layout-types.md) to understand how to use them. Certainly, default layout is zero-config layout but still if you want to understand each layout types, Go for it.

For styling our new vuexy have managed to provide separate space for your style where you can write your own style and don't accidentally delete ours. Make sure to check out [Template Styles](/guide/development/template-styles.md) for it.

In **Components** section we have provided docs for changes or extra features provided on top of existing BootstrapVue components.

In **Custom Components** section you will find custom components created by us.

**Extensions** Section is same as Components section, you will find changes or features on top of already existing extension features

## Starter-kit vs Full Package

### Starter-kit

Starter-kit is minimal template from where you can start your project quickly instead of removing stuff which isn't needed. Below is quick notes on starter-kit:

- No Auth
- No ACL
- No i18n
- Simple Navbar
- Four pages (Two sample pages + 404 + Login)
- No Customizer
- No Axios
- No Scroll to top

Use it if you don't want to clutter your project with extra libs which isn't required.

Don't worry about configuring third party libs which if you want to use them in starter-kit, our template is so flexible that all you have to do is just import lib file in `main.js` file and provided third party package is ready to use 😍.

::: tip
Use starter-kit to provide reproduction steps if you raise any technical [support ticket](/guide/getting-started/support.md).
:::

### Full Package

This setup have all the things which you can see in live demo. Except that red Buy Now button 😅.

With this you have to remove things which you don't use or replace them with your content.

**Conclusion**

According to us, starter-kit is easy to get started with minimal setup and our folder and code structure will help you adding libs support in starter-kit **more easily than ever**. Still if your project becomes easy with full package go for it.

## Getting template update

::: warning
Please keep track of which version of vuexy you are using in your project to easily update your project with latest template release.
:::

Please refer to this [article](/articles/how-to-update-vuexy-to-latest-version.md).

## Usage of b-card-code component

Please refer to our [FAQ](/faq/#what-is-usage-and-purpose-of-b-card-code-component) section for details on usage of b-card-code and what is the purpose of this component in our template.

## FAQ

We created list of common [FAQs](/faq/) which developers ask while using our template. So it is good idea to check for our FAQ if you have any query.

## Getting Support

If you have any question related to our template feel free to raise support ticket at our support portal. Please check our guide on [how to create perfect support ticket](/guide/getting-started/support.md).

## Why we deprecated Vuesax version

Check [FAQ](/faq/#why-we-are-removing-vuesax-version)
