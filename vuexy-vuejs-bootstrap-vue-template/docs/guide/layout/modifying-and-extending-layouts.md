# Modifying/Extending Layouts

You can modify existing layout and create your own custom layouts as well.

Template's core layouts and it's components have scoped slots so you can update layout components without breaking the core layouts and easily get update of bug fixes and features.

On the other hand if you don't want to extend your layout from base layout (Which is not recommended) and want to create layout of your own with your own styles you can do that as well. Isn't this template so flexible 😍?

Our provided layouts in `src/layouts` is extended layout from our core layouts.

1. **full**: Full layout
2. **vertical**: Layout /w layout components but navigation menu type is vertical
3. **horizontal**: Layout /w layout components but navigation menu type is horizontal

::: tip INFO
`vertical` & `horizontal` are Layouts with layout components (2nd type of layout). These names are assigned based on navigation type.

1. **Vertical Layout** means Layout with layout components and navigation menu type is vertical
2. **Horizontal Layout** means Layout with layout components and navigation menu type is horizontal
   :::

## 1. Extending `full` layout

Import full layout from core layout and use it's default slot to render route using `<router-view>`.

```vue
<template>
  <layout-full>
    <router-view />
  </layout-full>
</template>

<script>
import LayoutFull from '@core/layouts/layout-full/LayoutFull.vue'

export default {
  components: {
    LayoutFull,
  },
}
</script>
```

This layout is simple and minimal so there's no extra stuff other than default slot. Still you can check core layouts section to get details.

::: tip
Check source code of core layout and it's components to find out how it handles layouts and configurations and what slots/scoped-slots are available to use.
:::

## 2. Extending `vertical` layout

Import vertical layout from core layouts and use it's default slot to render route using `<router-view>`. This layout also provide `customizer` slot which you can use to render customizer. To use customizer make sure you provide `v-if` condition to enable disabling customizer from `themeConfig.js`.

<!-- prettier-ignore-start -->
```vue
<template>
  <layout-vertical>

    <router-view />

    <!-- Using `customizer` slot to render customizer -->
    <app-customizer v-if="showCustomizer" slot="customizer" />

  </layout-vertical>
</template>

<script>
import LayoutVertical from '@core/layouts/layout-vertical/LayoutVertical.vue'
import AppCustomizer from '@core/layouts/components/app-customizer/AppCustomizer.vue'
import { $themeConfig } from '@themeConfig'

export default {
  components: {
    AppCustomizer,
    LayoutVertical,
  },
  data() {
    return {
      showCustomizer: $themeConfig.layout.customizer,
    }
  },
}
</script>
```
<!-- prettier-ignore-end -->

You can use slots/scoped-slots provided by core vertical layout to tweak this layout.

## 3. Extending `horizontal` layout

Import horizontal layout from core layouts and use it's default slot to render route using `<router-view>`. This layout also provide `customizer` slot which you can use to render customizer. To use customizer make sure you provide `v-if` condition to enable disabling customizer from `themeConfig.js`.

<!-- prettier-ignore-start -->
```vue
<template>
  <layout-horizontal>

    <router-view />

    <!-- Using `customizer` slot to render customizer -->
    <app-customizer v-if="showCustomizer" slot="customizer" />

  </layout-horizontal>

</template>

<script>
import LayoutHorizontal from '@core/layouts/layout-horizontal/LayoutHorizontal.vue'
import AppCustomizer from '@core/layouts/components/app-customizer/AppCustomizer.vue'
import { $themeConfig } from '@themeConfig'

export default {
  components: {
    LayoutHorizontal,
    AppCustomizer,
  },
  data() {
    return {
      showCustomizer: $themeConfig.layout.customizer,
    }
  },
}
</script>
```
<!-- prettier-ignore-end -->

You can use slots/scoped-slots provided by core horizontal layout to tweak this layout.

**Conclusion**

Extending layout is really simple just import the core layout and make use of slots/scoped-slots to render your custom content. All scoped-slots have default component rendering so you don't have to use all scoped-slots, like you can still update navigation menu's title, logo and items using our flexible configuration.
