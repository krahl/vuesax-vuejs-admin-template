# Card Code

`b-card-code` component is used just for **demo purposes**. We use this component to show demo and code snippet of demo in single card. **You don't have to use this component**.

## Default

`b-card-actions` component is same as BootstrapVue's [card](https://bootstrap-vue.org/docs/components/card) component. It have code slot for displaying code and it can be viewed by clicking on code icon on right top corner of card.

```vue
<template>
  <b-card-code title="Demo Card">
    <p>Content is for demo purpose</p>

    <template #code>{{ code }}</template>
  </b-card-code>
</template>

<script>
export default {
  data() {
    return {
      code: `<p>Content is for demo purpose</p>`,
    }
  },
}
</script>
```
