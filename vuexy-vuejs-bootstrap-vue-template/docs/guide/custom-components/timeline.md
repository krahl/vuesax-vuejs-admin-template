# Timeline

You can use our custom timeline components `app-timeline` & `app-timeline-item` to render timeline.

## Basic Timeline

Use props provided by `app-timeline-item` component to render basic timeline.

You can check demo in "Basic" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/timeline) page.

## With Icon

You can use `icon` prop of `app-timeline-item` to render any feather icon instead of dots.

You can check demo in "With Icon" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/timeline) page.

## Custom Content

You can also render custom data inside Timeline using default slot of `app-timeline-item`.

You can check demo in "Custom Content" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/timeline) page.

## App Timeline Item API

### Props

| Name     | Description                          | Type          | Parameters              | Default |
| :------- | :----------------------------------- | :------------ | :---------------------- | :------ |
| variant  | Bootstrap color variant for dot/item | string        | Bootstrap Color         | primary |
| title    | Timeline item title                  | string        |                         |         |
| subtitle | Timeline item subtitle               | string        |                         |         |
| time     | time to show on right                | string        |                         |         |
| icon     | feather icon name                    | string/object | valid feather icon name |         |
