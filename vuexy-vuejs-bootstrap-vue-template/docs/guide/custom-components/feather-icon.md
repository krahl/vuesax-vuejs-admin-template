# FeatherIcon

You can use our custom feather icon component to render dynamically render any feather icon.

## Basic

You can render feather icons using `icon` prop of `feather-icon` component.

```html
<feather-icon class="mr-1" icon="InfoIcon" />
<feather-icon class="mr-1" icon="HomeIcon" />
<feather-icon class="mr-1" icon="UserIcon" />
<feather-icon class="mr-1" icon="TwitterIcon" />
```

_Result:_

<img :src="$withBase('/images/demo-imgs/custom-components/feather-icon-basic.jpg')" alt="feather-icon-basic" class="medium-zoom rounded bordered">

## Change Size

To change the size of icon you can use `size` prop. You can pass number in `size` prop as string.

```html
<feather-icon class="mr-1" icon="TwitterIcon" size="20" />
<feather-icon class="mr-1" icon="TwitterIcon" size="25" />
<feather-icon class="mr-1" icon="TwitterIcon" size="30" />
<feather-icon class="mr-1" icon="TwitterIcon" size="35" />
```

_Result:_

<img :src="$withBase('/images/demo-imgs/custom-components/feather-icon-size.jpg')" alt="feather-icon-size" class="medium-zoom rounded bordered">

## Changing Color

You can use Bootstrap's color classes to change color of SVG icon.

```html
<feather-icon class="text-primary mr-1" icon="TwitterIcon" />
<feather-icon class="text-success mr-1" icon="TwitterIcon" />
<feather-icon class="text-warning mr-1" icon="TwitterIcon" />
<feather-icon class="text-danger mr-1" icon="TwitterIcon" />
```

_Result:_

<img :src="$withBase('/images/demo-imgs/custom-components/feather-icon-color.jpg')" alt="feather-icon-color" class="medium-zoom rounded bordered">

## With Badge

`badge` prop will allow you to pass content as badge to any icon. Default background color of badge is "primary". You can change background color of badge using `badge-classes` prop.

```html
<feather-icon badge="3" badge-classes="bg-primary" class="mr-1" icon="MessageSquareIcon" />
<feather-icon badge="5" badge-classes="bg-success" class="mr-1" icon="InstagramIcon" />
<feather-icon badge="2" badge-classes="bg-warning" class="mr-1" icon="AlertCircleIcon" />
<feather-icon badge="1" badge-classes="bg-danger" class="mr-1" icon="BellIcon" />
```

You can check demo in navbar on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/) page.

## Component API

### Props

| Name         | Description       | Type                 | Parameters              | Default       |
| ------------ | ----------------- | -------------------- | ----------------------- | ------------- |
| icon         | feather icon name | string/object        | valid feather icon name | -             |
| size         | size of icon      | string               | integer                 | 14            |
| badge        | badge content     | string/object/number | -                       | null          |
| badgeClasses | classes for badge | string/object/array  | -                       | badge-primary |
