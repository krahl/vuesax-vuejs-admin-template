# Collapse

We have created our custom collapse componets to make it more beautiful. Let's have a glance.

## Default Collapse

```vue
<template>
  <app-collapse>
    <app-collapse-item title="Collapse Item 1">
      Pie dragée muffin. Donut cake liquorice marzipan carrot cake topping powder candy. Sugar plum brownie brownie
      cotton candy.
    </app-collapse-item>
    <app-collapse-item title="Collapse Item 2">
      Pie dragée muffin. Donut cake liquorice marzipan carrot cake topping powder candy. Sugar plum brownie brownie
      cotton candy.
    </app-collapse-item>
    <app-collapse-item title="Collapse Item 3">
      Pie dragée muffin. Donut cake liquorice marzipan carrot cake topping powder candy. Sugar plum brownie brownie
      cotton candy.
    </app-collapse-item>
    <app-collapse-item title="Collapse Item 4">
      Pie dragée muffin. Donut cake liquorice marzipan carrot cake topping powder candy. Sugar plum brownie brownie
      cotton candy.
    </app-collapse-item>
  </app-collapse>
</template>

<script>
import AppCollapse from '@core/components/app-collapse/AppCollapse.vue'
import AppCollapseItem from '@core/components/app-collapse/AppCollapseItem.vue'

export default {
  components: {
    AppCollapse,
    AppCollapseItem,
  },
}
</script>
```

You can check demo in "Default" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/collapse) page.

## Accordion

Turn a group of `<app-collapse>` components into an accordion by supplying an accordion group identifier via the `accordion` prop.

```vue
<template>
  <app-collapse accordion>
    <app-collapse-item title="Accordion Item 1">
      Cheesecake cotton candy bonbon muffin cupcake tiramisu croissant. Tootsie roll sweet candy bear claw chupa chups
      lollipop toffee. Macaroon donut liquorice powder candy carrot cake macaroon fruitcake.
    </app-collapse-item>
    <app-collapse-item title="Accordion Item 2">
      Cheesecake cotton candy bonbon muffin cupcake tiramisu croissant. Tootsie roll sweet candy bear claw chupa chups
      lollipop toffee. Macaroon donut liquorice powder candy carrot cake macaroon fruitcake.
    </app-collapse-item>
    <app-collapse-item title="Accordion Item 3">
      Cheesecake cotton candy bonbon muffin cupcake tiramisu croissant. Tootsie roll sweet candy bear claw chupa chups
      lollipop toffee. Macaroon donut liquorice powder candy carrot cake macaroon fruitcake.
    </app-collapse-item>
    <app-collapse-item title="Accordion Item 4">
      Cheesecake cotton candy bonbon muffin cupcake tiramisu croissant. Tootsie roll sweet candy bear claw chupa chups
      lollipop toffee. Macaroon donut liquorice powder candy carrot cake macaroon fruitcake.
    </app-collapse-item>
  </app-collapse>
</template>

<script>
import AppCollapse from '@core/components/app-collapse/AppCollapse.vue'
import AppCollapseItem from '@core/components/app-collapse/AppCollapseItem.vue'

export default {
  components: {
    AppCollapse,
    AppCollapseItem,
  },
}
</script>
```

You can check demo in "Accordion" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/collapse) page.
