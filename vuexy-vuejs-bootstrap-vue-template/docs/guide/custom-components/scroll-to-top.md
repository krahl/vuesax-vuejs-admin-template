# Scroll to top

`scroll-to-top` component adds scroll to top action floating button when page is scrolled enough

## Usage

To use scroll to top component import it and render it in template tags. It is really simple component which don't have any props, event and slots. It is used in `App.vue`.

```vue
<template>
  <!-- This is minimal example with nothing but a scroll to top component -->
  <scroll-to-top v-if="enableScrollToTop" />
</template>

<script>
import ScrollToTop from '@core/components/scroll-to-top/ScrollToTop.vue'
import { $themeConfig } from '@themeConfig'

export default {
  components: {
    ScrollToTop,
  },
  setup() {
    const { enableScrollToTop } = $themeConfig.layout

    return {
      enableScrollToTop,
    }
  },
}
</script>
```
