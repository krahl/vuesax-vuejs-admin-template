# Card Actions

Use `b-card-actions` component to create card with `collapse`, `refresh` and `close` actions.

## Default

`b-card-actions` component is same as BootstrapVue's [card](https://bootstrap-vue.org/docs/components/card) component.

:::warning WARNING
As refresh action is used for async data fetching, you have to set card's `showLoading` property to false once you are done with fetching data.
:::

```vue
<template>
  <b-card-actions title="Card w/ All Actions" @refresh="refreshStop('cardAction')" ref="cardAction">
    <span>You can use default slot to render your content inside card.</span>
  </b-card-actions>
</template>

<script>
export default {
  methods: {
    // stop refreshing card in 3 sec
    refreshStop(cardName) {
      setTimeout(() => {
        this.$refs[cardName].showLoading = false
      }, 3000)
    },
  },
}
</script>
```

You can check demo in "Card Actions" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/card/action) page.

## Specific Action

You can also use only specific action if you don't want to use all actions. Just pass the name of action with `action-` prefix as prop and you will get that specific action.

e.g. `action-close`

```vue{2}
<template>
  <b-card-actions title="Card w/ Collapse Action Only" action-collapse>
    <span>You can use default slot to render your content inside card.</span>
  </b-card-actions>
</template>
```

You can check demo in "Collapsible", "Refresh Content" and "Remove Card" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/card/action) page.

## Component API

### Props

| Name           | Description                                                                               | Type    | Parameters | Default |
| :------------- | :---------------------------------------------------------------------------------------- | :------ | :--------- | :------ |
| collapsed      | make card collapsed from start                                                            | boolean | true/false | false   |
| noActions      | Remove all actions from component. (If you are using this prop just use b-card component) | boolean | true/false | false   |
| actionCollapse | Enable Collapse action                                                                    | boolean | true/false | false   |
| actionRefresh  | Enable refresh action                                                                     | boolean | true/false | false   |
| actionClose    | Enable close action                                                                       | boolean | true/false | false   |

### Events

| Name     | Description               |
| :------- | :------------------------ |
| collapse | Collapse action triggered |
| refresh  | Refresh action triggered  |
| close    | close action triggered    |

### Slots

| Name        | Description  | Slot Props |
| :---------- | :----------- | :--------- |
| description | default slot | None       |
