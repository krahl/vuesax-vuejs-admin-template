# Statistics Cards

This contains 4 components with different layout/UI.

## Statistics Card Horizontal

```vue
<template>
  <statistic-card-horizontal icon="CpuIcon" statistic="86%" statistic-title="CPU Usage" style="max-width:300px" />
</template>

<script>
import StatisticCardHorizontal from '@core/components/statistics-cards/StatisticCardHorizontal.vue'

export default {
  components: {
    StatisticCardHorizontal,
  },
}
</script>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/custom-components/stats-card-horizontal.jpg')" alt="stats-card-horizontal" class="medium-zoom rounded bordered">

`statistic-card-horizontal` component renders content in two col and single row.

Below is props supported by this component:

- **color**: Bootstrap color variant to use (e.g. `success`)
- **icon**: feather icon name
- **statistic**: Statistics text (Value)
- **statistic-title**: Statistics title

## Statistics Card Vertical

```vue
<template>
  <statistic-card-vertical
    icon="EyeIcon"
    color="info"
    statistic="36.9k"
    statistic-title="Views"
    style="max-width:160px"
  />
</template>

<script>
import StatisticCardVertical from '@core/components/statistics-cards/StatisticCardVertical.vue'

export default {
  components: {
    StatisticCardVertical,
  },
}
</script>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/custom-components/stats-card-vertical.jpg')" alt="stats-card-vertical" class="medium-zoom rounded bordered">

`statistic-card-vertical` component renders content in two row and single column.

Below is props supported by this component:

- **color**: Bootstrap color variant to use (e.g. `success`)
- **icon**: feather icon name
- **statistic**: Statistics text (Value)
- **statistic-title**: Statistics title

## Statistics Card With Area Chart

`statistic-card-with-area-chart` component renders content with area chart. It uses apex charts for chart.

<img :src="$withBase('/images/demo-imgs/custom-components/stats-card-with-area-chart.png')" alt="stats-card-with-area-chart" class="medium-zoom rounded bordered">

Below is props supported by this component:

- **color**: Bootstrap color variant to use (e.g. `success`)
- **icon**: feather icon name
- **statistic**: Statistics text (Value)
- **statistic-title**: Statistics title
- **chartData**: Chart Data
- **chartOptions**: Apex Chart options (optional - we configured already it)

## Statistics Card With Line Chart

`statistic-card-with-line-chart` component renders content with line chart. It uses apex charts for chart.

<img :src="$withBase('/images/demo-imgs/custom-components/stats-card-with-line-chart.png')" alt="stats-card-with-line-chart" class="medium-zoom rounded bordered">

Below is props supported by this component:

- **color**: Bootstrap color variant to use (e.g. `success`)
- **icon**: feather icon name
- **statistic**: Statistics text (Value)
- **statistic-title**: Statistics title
- **chartData**: Chart Data
- **chartOptions**: Apex Chart options (optional - we configured already it)

You can check all demos on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/card/statistic) page.
