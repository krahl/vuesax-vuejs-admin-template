# Tour

We have created our tour component using v-tour library to make it more beautiful

## Custom component

Just import `AppTour` component and provide `steps` prop to render styled tour instead of rendering stock tour provided by package.

```vue
<template>
  <b-card id="tour-card" title="Tour">
    <b-button v-ripple.400="'rgba(113, 102, 240, 0.15)'" variant="outline-primary" @click="tourStart">
      Start Tour
    </b-button>

    <app-tour :steps="steps" />
  </b-card>
</template>

<script>
import { BCard } from 'bootstrap-vue'
import { BButton } from 'bootstrap-vue'
import Ripple from 'vue-ripple-directive'
import AppTour from '@core/components/app-tour/AppTour.vue'

export default {
  components: {
    BCard,
    BButton,
    AppTour,
  },
  directives: {
    Ripple,
  },
  data() {
    return {
      steps: [
        // steps
      ],
    }
  },
  methods: {
    tourStart() {
      this.$tours.vuexyTour.start()
    },
  },
}
</script>

<style lang="scss">
@import '~@core/scss/vue/libs/tour.scss';
</style>
```

You can check demo on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/tour) page.

## AppTour API

| Name  | Description  | Type  | Parameters | Default |
| ----- | ------------ | ----- | ---------- | ------- |
| steps | v-tour steps | array |            |         |
