# Toastification

Toastification is greatly flexible package which let you style/render toast however you want.

## Custom Component

We have used custom component `ToastificationContent` for rendering toast content. Thanks to support for [custom component](https://github.com/Maronato/vue-toastification#using-a-custom-component) in toastification component, you can even create you own component with your own style if you like.

::: warning
Please make sure to read the docs for toastification for using it with vue composition API and normal options API.
:::

You can check demo on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/toastification) page.

## ToastificationContent API

| Name      | Description                       | Type          | Parameters              | Default |
| :-------- | :-------------------------------- | :------------ | :---------------------- | :------ |
| variant   | Bootstrap color variant for toast | string        | Bootstrap Color         | primary |
| title     | Toast title                       | string        |                         |         |
| text      | toast text/description            | string        |                         |         |
| icon      | feather icon name                 | string/object | valid feather icon name |         |
| hideClose | Hide close to close the toast     | boolean       | true/false              | false   |
