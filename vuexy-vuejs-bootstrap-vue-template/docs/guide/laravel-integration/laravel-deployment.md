# Laravel Deployment

## Root Domain Deployment

We are going to discuss basic changes in our template before deploying our template in the root domain.

:::tip
Firstly, you have to follow all the installation steps if you have not done it before. You can find these steps [here](/guide/laravel-integration/installation.md#guide).
:::

### Step 1

You have to make changes to the `webpack.mix.js` file. You can find this file in your root folder.

```js
// ------------------------------------------------
// If you are deploying on subdomain/subfolder. Uncomment below code before running 'yarn prod' or 'npm run production' command.
// Please Change below 'publicPath' and 'setResourceRoot' options as per your sub-directory path. We have kept our current live demo options which is deployed in sub-folder.
// ------------------------------------------------

/*
 if (mix.inProduction()) {
   mix.version()
   mix.webpackConfig({
     output: {
       publicPath: '/demo/vuexy-vuejs-laravel-admin-template/demo-1/',
       chunkFilename: 'js/chunks/[name].[chunkhash].js'
     }
   })
   mix.setResourceRoot('/demo/vuexy-vuejs-laravel-admin-template/demo-1/')
 }
 */

// ------------------------------------------------
// If you are deploying on subdomain/subfolder then comment out below code before running 'yarn prod' or 'npm run production' command.
// ------------------------------------------------

mix.webpackConfig({
  output: {
    chunkFilename: 'js/chunks/[name].[chunkhash].js',
  },
})
```

### Step 2

Now, run the `yarn prod` or `npm run production` command to generate the build.

Congratulation! You have successfully generated your build package.

### Step 3

You have to place all public files inside your server's `public_html` folder and your Laravel is in a separate folder.

For example:-

We have two folder in our server.

```{1,12}
├── public_html (folder for public accessible)
│   ├── css
│   ├── fonts
│   ├── images
│   ├── js
│   ├── .htaccess
│   ├── favicon.ico
│   ├── index.php
│   ├── mix-manifest.json
│   ├── web.config
│   ├── robots.txt
├── laravel  (folder where laravel live)
│   ├── app
│   ├── bootstrap
│   ├── config
│   ├── database
│   ├── frontend
│   ├── resources
│   ├── routes
│   ├── storage
│   ├── tests
│   ├── vendor
│   ├── .babelrc
│   ├── .env
│   ├── .gitattributes
│   ├── .gitignore
│   ├── .styleci.yml
│   ├── artisan
│   ├── composer.json
│   ├── docker-compose.yml
│   ├── package.json
│   ├── phpunit.xml
│   ├── server.php
│   ├── webpack.mix.js
```

As an above structure, we have to changes some paths in the index.php file in the public or public_html file. Also, we have to bind the document root to he current file path, where `index.php` exists.

```php{19,20,34,47}
<?php

use Illuminate\Contracts\Http\Kernel;
use Illuminate\Http\Request;

define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Check If Application Is Under Maintenance
|--------------------------------------------------------------------------
|
| If the application is maintenance / demo mode via the "down" command we
| will require this file so that any prerendered template can be shown
| instead of starting the framework, which could cause an exception.
|
*/

if (file_exists(__DIR__.'/../laravel/storage/framework/maintenance.php')) {
    require __DIR__.'/../laravel/storage/framework/maintenance.php';
}

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| this application. We just need to utilize it! We'll simply require it
| into the script here so we don't need to manually load our classes.
|
*/

require __DIR__.'/../laravel/vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request using
| the application's HTTP kernel. Then, we will send the response back
| to this client's browser, allowing them to enjoy our application.
|
*/

$app = require_once __DIR__.'/../laravel/bootstrap/app.php';

$app->bind('path.public', function() {
    return base_path('/../public_html');
});

$kernel = $app->make(Kernel::class);

$response = tap($kernel->handle(
    $request = Request::capture()
))->send();

$kernel->terminate($request, $response);
```

:::tip
As above code, we have changed all paths as per our folder structure.

Note: `public_html` folder may differ from server to server.
:::

Congratulation! You have successfully deployed the package.

## Subdomain Deployment

If you are deploy in your subdomain.

### step 1:

You have to make changes to the webpack.mix.js file. You can find this file in your root folder.

```php
// ------------------------------------------------
// If you are deploying on subdomain/subfolder. Uncomment below code before running 'yarn prod' or 'npm run production' command.
// Please Change below 'publicPath' and 'setResourceRoot' options as per your sub-directory path. We have kept our current live demo options which is deployed in sub-folder.
// ------------------------------------------------


 if (mix.inProduction()) {
   mix.version()
   mix.webpackConfig({
     output: {
       publicPath: '/demo/public_html/',
       chunkFilename: 'js/chunks/[name].[chunkhash].js'
     }
   })
   mix.setResourceRoot('/demo/public_html/')
 }


// ------------------------------------------------
// If you are deploying on subdomain/subfolder then comment out below code before running 'yarn prod' or 'npm run production' command.
// ------------------------------------------------

// mix.webpackConfig({
//   output: {
//     chunkFilename: 'js/chunks/[name].[chunkhash].js',
//   },
// })
```

### Step 2

_Same as root domain deployment steps._
