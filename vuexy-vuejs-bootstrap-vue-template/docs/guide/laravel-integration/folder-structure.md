# Folder Structure

Understand folder structure of template and what everything contains

## Overview

Once you download the template from ThemeForest, you will find the below folder structure in `vuexy-admin-X.X/vue-laravel-version/`. This folder contains `vuexy-bootstrapvue` and this folder contains `full-version` and `starter-kit` of laravel using Vuexy Vuejs Admin.

<br />
Before checking folder structure it is better you know some stuff related to folder structure.

- `resources/js/src` folder contains the whole vuejs code, this is the main folder where Auth, ACL, Routing, Navigation everything is managed from and not from Laravel. We have just provided Laravel integration which means all the routes are redirected to Application controller which is responsible to load the application using Application view. All the assets are compiled from this folder to Laravel's public folder. Basically, we have considered Laravel usage for API purpose only.
  <br>

## [Folder Structure](/guide/development/folder-structure.md)

Other folders and details of it will be available in docs. This overview is here so you don't get confused.

```
vuexy-vuejs-laravel-template/
├── app                      # Controllers and Models
├── bootstrap                # Contains cache and app.php
├── config                   # Application's configuration files
├── database                 # Migrations, model factories, & seeds
├── docker                   # Dockerfile
├── lang                     # Include Languages
├── public                   # index.php ,static folder & Build
│   ├── favicon.ico            -> Favicon
│   ├── logo.png               -> Template Logo
│   └── index.html             -> Main HTML
├── resources                # Views, Layouts, store and vue.js components
│   ├── assets/                # Include styles file for user override
│   ├── css/                   # Include loader.css file
│   ├── js/                    # Include all vue files
│   ├── scss/                  # Include all core scss files
│   └── views/                 # Contain Blade templates
├── routes/                  # Include Routes Web.php
├── storage/                 # Contains compile blade templates
├── tests/                   # For testing
├── .babelrc                 # Babel File
├── .browserslistrc          # Browser list File
├── .editorconfig            # Related with your editor
├── .env.example             # Include Database credentials
├── .gitattributes           # Give attributes to pathnames
├── .gitignore               # Files and Directories to ignore
├── .eslintignore            # ESLint Ignore
├── .eslintrc.js             # ESLint Configuration
├── .prettierignore          # Prettier Ignore
├── .styleci.yml
├── artisan                  # Include artisans commands
├── composer.json            # Dependencies used by composer
├── package.json             # Dependencies used by node
├── babel.config.js          # Babel configuration
├── phpunit.xml              # Related With testing
├── server.php               # For php's internal web server
├── postcss.config.js        # postcss configuration
├── vue.config.js            # VueJS Configuration File
└── webpack.mix.js           # Laravel's webpack file
└── docker-compose.yml       # Docker
```
