# Dropdown

Dropdown component is slightly modified to make it more beautiful. Let's have a glance.

## Dropdown Outline

To create a dropdown with split button use `variant="outline-outline-{color}"` prop with your dropdown toggle.

You can check demo in "Outline" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/dropdown) page.

## Flat Dropdown

To create a flat dropdown use `variant="flat-{color}"` with your &lt;b-dropdown&gt;

You can check demo in "Flat" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/dropdown) page.

## Gradient Dropdown

To create a gradient dropdown use `variant="gradient-{color}"` prop with your &lt;b-dropdown&gt;

You can check demo in "Gradient" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/dropdown) page.

## Dropdown with Icon

We have provided `.dropdown-icon-wrapper` class for dropdown with icon which is hide `caret` icon in button.

You can check demo in "Variation" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/dropdown) page.
