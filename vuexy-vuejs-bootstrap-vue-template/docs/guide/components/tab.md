# Tab

Tab component is slightly modified to make it more beautiful. Let's have a glance.

## Tab Right

You'll need to add `vertical` , `end` , prop with `nav-class="nav-right"` and `nav-wrapper-class="nav-vertical"` custom class to work perfectly.

```vue
<b-tabs vertical end nav-class="nav-right" nav-wrapper-class="nav-vertical">
  <b-tab active title="Tab 1">
    <b-card-text>
      Sweet chocolate muffin fruitcake gummies jujubes pie lollipop. 
      Brownie marshmallow caramels gingerbread jelly beans chocolate bar oat cake wafer.
    </b-card-text>
    <b-card-text>
        Sugar plum tootsie roll biscuit caramels. Sweet fruitcake cheesecake biscuit cotton candy. Cookie powder marshmallow donut. Liquorice brownie pastry cotton candy oat cake fruitcake jelly chupa chups. Pudding caramels pastry powder cake soufflé wafer caramels. Jelly-o pie cupcake.
    </b-card-text>
  </b-tab>
  <b-tab title="Tab 2">
    <b-card-text>
      Sugar plum tootsie roll biscuit caramels. Sweet fruitcake cheesecake biscuit cotton candy. Cookie powder marshmallow donut. Liquorice brownie pastry cotton candy oat cake fruitcake jelly chupa chups. Pudding caramels pastry powder cake soufflé wafer caramels. Jelly-o pie cupcake.
    </b-card-text>
    <b-card-text>
      Sweet chocolate muffin fruitcake gummies jujubes pie lollipop. Brownie marshmallow caramels gingerbread jelly beans chocolate bar oat cake wafer.
    </b-card-text>
  </b-tab>
  <b-tab title="Tab 3">
    <b-card-text>
      Sweet fruitcake cheesecake biscuit cotton candy. Cookie powder marshmallow donut. Biscuit ice cream halvah candy canes bear claw ice cream cake chocolate bar donut. Toffee cotton candy liquorice. Oat cake lemon drops gingerbread dessert caramels. Sweet dessert jujubes powder sweet sesame snaps.
    </b-card-text>
    <b-card-text>
      Candy cookie sweet roll bear claw sweet roll. Cake tiramisu cotton candy gingerbread cheesecake toffee cake. Cookie liquorice dessert candy canes jelly.
    </b-card-text>
  </b-tab>
</b-tabs>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/tab-right.jpg')" alt="tab-right" class="medium-zoom rounded bordered">

## Tab Left

You'll need to add `vertical` and `nav-wrapper-class="nav-vertical"` for left tabs.

```vue
<b-tabs vertical nav-wrapper-class="nav-vertical">
  <b-tab active title="Tab 1">
    <b-card-text>

      Oat cake marzipan cake lollipop caramels wafer pie jelly beans. Icing halvah chocolate cake carrot cake. Jelly beans carrot cake marshmallow 
      gingerbread chocolate cake. Sweet fruitcake cheesecake biscuit cotton candy. Cookie powder marshmallow donut. Gummies cupcake croissant.

    </b-card-text>
    <b-card-text>
      Sweet chocolate muffin fruitcake gummies jujubes pie lollipop. Brownie marshmallow caramels gingerbread jelly beans chocolate bar oat cake wafer.
    </b-card-text>
  </b-tab>
  <b-tab title="Tab 2">
    <b-card-text>
      Sugar plum tootsie roll biscuit caramels. Sweet fruitcake cheesecake biscuit cotton candy. Cookie powder marshmallow donut. Liquorice brownie pastry cotton candy oat cake fruitcake jelly chupa chups. Pudding caramels pastry powder cake soufflé wafer caramels. Jelly-o pie cupcake.
    </b-card-text>
    <b-card-text>
      Chocolate bar danish icing sweet apple pie jelly-o carrot cake cookie cake.
    </b-card-text>
  </b-tab>
  <b-tab title="Tab 3">
    <b-card-text>
      Sweet fruitcake cheesecake biscuit cotton candy. Cookie powder marshmallow donut. Biscuit ice cream halvah candy canes bear claw ice cream cake chocolate bar donut. Toffee cotton candy liquorice. Oat cake lemon drops gingerbread dessert caramels. Sweet dessert jujubes powder sweet sesame snaps.
    </b-card-text>
    <b-card-text>
      Candy cookie sweet roll bear claw sweet roll. Cake tiramisu cotton candy gingerbread cheesecake toffee cake. Cookie liquorice dessert candy canes jelly.
    </b-card-text>
  </b-tab>
</b-tabs>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/tab-left.jpg')" alt="tab-left" class="medium-zoom rounded bordered">
