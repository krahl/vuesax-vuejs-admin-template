# Breadcrumbs

Breadcrumbs component is slightly modified to make it more beautiful. Let's have a glance.

## Separator

You can change breadcrumb `seprator` by add our custom classes `.breadcrumb-slash` , `.breadcrumb-dots` , `.breadcrumb-dashes` , `.breadcrumb-pipes` and `.breadcrumb-chevron` .

```vue
<template>
  <div id="component-breadcrumbs">
    <b-breadcrumb class="breadcrumb-slash" :items="items" />
    <b-breadcrumb class="breadcrumb-dots" :items="items" />
    <b-breadcrumb class="breadcrumb-dashes" :items="items" />
    <b-breadcrumb class="breadcrumb-pipes" :items="items" />
    <b-breadcrumb class="breadcrumb-chevron mb-0" :items="items" />
  </div>
</template>

<script>
import { BBreadcrumb } from 'bootstrap-vue'

export default {
  components: {
    BBreadcrumb,
  },
  data() {
    return {
      items: [
        {
          text: 'Home',
        },
        {
          text: 'Library',
        },
        {
          text: 'Data',
          active: true,
        },
      ],
    }
  },
}
</script>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/breadcrumbs-separator.jpg')" alt="breadcrumbs-separator" class="medium-zoom rounded bordered">
