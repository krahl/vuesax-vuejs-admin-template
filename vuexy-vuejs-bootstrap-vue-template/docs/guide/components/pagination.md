# Pagination

Pagination component is slightly modified to make it more beautiful. Let's have a glance.

## Separated

To make space between seprated icons of pagination use `.prev-item` class for the `prev-class` prop and `.next-item` for the `next-class` prop.

```vue
<b-pagination total-rows="100" first-number last-number prev-class="prev-item" next-class="next-item">
  <template v-slot:prev-text>
    <feather-icon icon="ChevronLeftIcon" size="18"/>
  </template>
  <template v-slot:next-text>
    <feather-icon icon="ChevronRightIcon" size="18"/>
  </template>
</b-pagination>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/pagination-separated.jpg')" alt="pagination-separated" class="medium-zoom rounded bordered">
