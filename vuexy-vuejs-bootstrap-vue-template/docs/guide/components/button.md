# Button

Button component is slightly modified to make it more beautiful. Let's have a glance.

## Button Border

Use a prop `variant="outline-{color}"` to quickly create a outline button.

```vue
<b-button v-ripple.400="'rgba(113, 102, 240, 0.15)'" variant="outline-primary">Primary</b-button>
<b-button v-ripple.400="'rgba(186, 191, 199, 0.15)'" variant="outline-secondary">Secondary</b-button>
<b-button v-ripple.400="'rgba(40, 199, 111, 0.15)'" variant="outline-success">Success</b-button>
<b-button v-ripple.400="'rgba(234, 84, 85, 0.15)'" variant="outline-danger">Danger</b-button>
<b-button v-ripple.400="'rgba(255, 159, 67, 0.15)'" variant="outline-warning">Warning</b-button>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/button-outline.jpg')" alt="button-outline" class="medium-zoom rounded bordered">

## Flat Button

We have also provided flat buttons. Use `.btn-flat-{color}` custom class to create a flat button.

```vue
<b-button v-ripple.400="'rgba(113, 102, 240, 0.15)'" variant="flat-primary">Primary</b-button>
<b-button v-ripple.400="'rgba(186, 191, 199, 0.15)'" variant="flat-secondary">Secondary</b-button>
<b-button v-ripple.400="'rgba(40, 199, 111, 0.15)'" variant="flat-success">Success</b-button>
<b-button v-ripple.400="'rgba(234, 84, 85, 0.15)'" variant="flat-danger">Danger</b-button>
<b-button v-ripple.400="'rgba(255, 159, 67, 0.15)'" variant="flat-warning">Warning</b-button>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/button-flat.jpg')" alt="button-flat" class="medium-zoom rounded bordered">

## Gradient Button

We have provided Gradient Buttons in our template. Use `variant="gradient-{color}"` prop create gradient buttons.

```vue
<b-button variant="gradient-primary">Primary</b-button>
<b-button variant="gradient-secondary">Secondary</b-button>
<b-button variant="gradient-success">Success</b-button>
<b-button variant="gradient-danger">Danger</b-button>
<b-button variant="gradient-warning">Warning</b-button>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/button-gradient.jpg')" alt="button-gradient" class="medium-zoom rounded bordered">

## Relief Button

We have provided Relief Button in our template. Use `.btn-relief-{color}` to create a relief button.

```vue
<b-button variant="relief-primary">Primary</b-button>
<b-button variant="relief-secondary">Secondary</b-button>
<b-button variant="relief-success">Success</b-button>
<b-button variant="relief-danger">Danger</b-button>
<b-button variant="relief-warning">Warning</b-button>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/button-relief.jpg')" alt="button-relief" class="medium-zoom rounded bordered">

## Icon Only

You can only use `.btn-icon` when you only want icon in your button.

```vue
<div class="demo-inline-spacing">
  <b-button v-ripple.400="'rgba(255, 255, 255, 0.15)'" variant="warning" class="btn-icon">
    <feather-icon icon="ArchiveIcon" />
  </b-button>
  <b-button variant="gradient-danger" class="btn-icon">
    <feather-icon icon="UserPlusIcon" />
  </b-button>
  <b-button v-ripple.400="'rgba(255, 255, 255, 0.15)'" variant="warning" class="btn-icon rounded-circle">
    <feather-icon icon="ArchiveIcon" />
  </b-button>
  <b-button variant="gradient-danger" class="btn-icon rounded-circle">
    <feather-icon icon="UserPlusIcon" />
  </b-button>
  <b-button v-ripple.400="'rgba(113, 102, 240, 0.15)'" variant="outline-primary" class="btn-icon">
    <feather-icon icon="SearchIcon" />
  </b-button>
  <b-button v-ripple.400="'rgba(113, 102, 240, 0.15)'" variant="outline-primary" class="btn-icon" disabled>
    <feather-icon icon="SearchIcon" />
  </b-button>
  <b-button v-ripple.400="'rgba(113, 102, 240, 0.15)'" variant="outline-primary" class="btn-icon rounded-circle">
    <feather-icon icon="SearchIcon" />
  </b-button>
  <b-button v-ripple.400="'rgba(113, 102, 240, 0.15)'" variant="outline-primary" class="btn-icon rounded-circle" disabled>
    <feather-icon icon="SearchIcon" />
  </b-button>
  <b-button v-ripple.400="'rgba(40, 199, 111, 0.15)'" variant="flat-success" class="btn-icon rounded-circle">
    <feather-icon icon="CameraIcon" />
  </b-button>
  <b-button v-ripple.400="'rgba(40, 199, 111, 0.15)'" variant="flat-success" class="btn-icon">
    <feather-icon icon="CameraIcon" />
  </b-button>
</div>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/button-icon-only.jpg')" alt="button-icon-only" class="medium-zoom rounded bordered">
