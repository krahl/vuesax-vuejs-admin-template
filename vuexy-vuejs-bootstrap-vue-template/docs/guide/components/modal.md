# Modal

Modal component is slightly modified to make it more beautiful. Let's have a glance.

## Modal Themes

Use class `modal-class="modal-{color}"` with your `<b-modal>` to change theme of modal

You can check demo in "Modal Themes" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/components/modal) page.
