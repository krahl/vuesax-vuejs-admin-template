# Pill Components

Pill component is slightly modified to make it more beautiful. Let's have a glance.

## Pill Themes

Use class `.nav-pill-{color-name}` with `<b-tabs>` class to apply color according to your choice.

```vue
<div>
  <h6>Primary</h6>

  <b-tabs pills nav-class="nav-pill-primary">
  <b-tab title="Active" active/>
  <b-tab title="Link"/>
  <b-tab title="Link"/>
  <b-tab title="Disabled"disabled/>
  </b-tabs>

  <h6>Secondary</h6>

  <b-tabs pills nav-class="nav-pill-secondary">
  <b-tab title="Active" active/>
  <b-tab title="Link"/>
  <b-tab title="Link"/>
  <b-tab title="Disabled" disabled/>
  </b-tabs>

  <h6>Success</h6>

  <b-tabs pills nav-class="nav-pill-success">
  <b-tab title="Active" active />
  <b-tab title="Link"/>
  <b-tab title="Link"/>
  <b-tab title="Disabled" disabled/>
  </b-tabs>

  <h6>Warning</h6>

  <b-tabs pills nav-class="nav-pill-warning">
  <b-tab title="Active" active/>
  <b-tab title="Link"/>
  <b-tab title="Link"/>
  <b-tab title="Disabled" disabled/>
  </b-tabs>

  <h6>Danger</h6>

  <b-tabs pills nav-class="nav-pill-danger">
  <b-tab title="Active" active/>
  <b-tab title="Link"/>
  <b-tab title="Link"/>
  <b-tab title="Disabled" disabled/>
  </b-tabs>
</div>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/pill-themes.jpg')" alt="pill-themes" class="medium-zoom rounded bordered">
