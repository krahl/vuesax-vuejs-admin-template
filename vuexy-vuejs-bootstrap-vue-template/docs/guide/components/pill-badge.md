# Pill Badge

`Pill-Badge` component is slightly modified to make it more beautiful. Let's have a glance.

## Glow Badges

Use class `.badge-glow` to add glow effect with badges.

```vue
<b-badge pill variant="primary" class="badge-glow">Primary</b-badge>
<b-badge pill variant="secondary" class="badge-glow">Secondary</b-badge>
<b-badge pill variant="success" class="badge-glow">Success</b-badge>
<b-badge pill variant="danger" class="badge-glow">Danger</b-badge>
<b-badge pill variant="warning" class="badge-glow">Warning</b-badge>
<b-badge pill variant="info" class="badge-glow">Info</b-badge>
<b-badge pill variant="dark" class="badge-glow">Dark</b-badge>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/pill-badge-glow.jpg')" alt="pill-badge-glow" class="medium-zoom rounded bordered">

## Light Badges

Use `variant="light-{color}"` prop for light badge color variantion.

```vue
<b-badge pill variant="light-primary">Primary</b-badge>
<b-badge pill variant="light-secondary">Secondary</b-badge>
<b-badge pill variant="light-success">Success</b-badge>
<b-badge pill variant="light-danger">Danger</b-badge>
<b-badge pill variant="light-warning">Warning</b-badge>
<b-badge pill variant="light-info">Info</b-badge>
<b-badge pill variant="light-dark">Dark</b-badge>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/pill-badge-light.jpg')" alt="pill-badge-light" class="medium-zoom rounded bordered">

## Pill Badges As Notification

Use class `badge` prop with `<feather-icon>` components to add badges as notification. Use `badge-classes="badge-{color}"` prop for change the badge color.

```vue
<feather-icon icon="BellIcon" size="21" class="text-primary" badge="4" />
<feather-icon icon="BellIcon" size="21" class="text-info" badge="4" badge-classes="badge-info" />
<feather-icon icon="BellIcon" size="21" class="text-danger" badge="4" badge-classes="badge-danger badge-glow" />
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/pill-badge-as-notification.jpg')" alt="pill-badge-as-notification" class="medium-zoom rounded bordered">

## Block Pill Badge

Use `.d-block` class with `<b-badge>`, to display badge as a block element.

```vue
<b-badge pill class="d-block" variant="danger">Badge</b-badge>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/pill-badge-block.jpg')" alt="pill-badge-block" class="medium-zoom rounded bordered">
