# Media

Media component is slightly modified to make it more beautiful. Let's have a glance.

## Media List

Use `.media-list` class as a wrapper of all `<b-media>` componets for proper spacing between two media objects.

## Media Bordered

Wrap media list using `.media-bordered` class for bordered Media Object.
