# Progress

`Progress` component is slightly modified to make it more beautiful. Let's have a glance.

## Progress Variant

Use `variant` prop to add different colors to progressbar and add `.progress-bar-{color_name}` custom class along with progress bar to get light unfilled variant.

```vue
<b-progress value="25" max="100" />
<b-progress value="35" max="100" class="progress-bar-secondary" variant="secondary" />
<b-progress value="45" max="100" class="progress-bar-success" variant="success" />
<b-progress value="55" max="100" class="progress-bar-danger" variant="danger" />
<b-progress value="65" max="100" class="progress-bar-warning" variant="warning" />
<b-progress value="75" max="100" class="progress-bar-info" variant="info" />
<b-progress value="85" max="100" class="progress-bar-dark" variant="dark" />
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/progress-variant.jpg')" alt="progress-variant" class="medium-zoom rounded bordered">
