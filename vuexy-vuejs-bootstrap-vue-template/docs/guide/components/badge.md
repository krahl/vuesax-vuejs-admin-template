# Badge

Badge component is slightly modified to make it more beautiful. Let's have a glance.

## Badge Light Color

We have added light background color badges in our template. You have to pass `light-{color}` value to `variant` prop.

```vue
<b-badge variant="light-primary">Primary</b-badge>
<b-badge variant="light-secondary">Secondary</b-badge>
<b-badge variant="light-success">Success</b-badge>
<b-badge variant="light-danger">Danger</b-badge>
<b-badge variant="light-warning">Warning</b-badge>
<b-badge variant="light-info">Info</b-badge>
<b-badge variant="light-dark">Dark</b-badge>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/badge-light.jpg')" alt="badge-light" class="medium-zoom rounded bordered">

## Badge Glow

You can add shadow with badges. You have to use custom class `badge-glow` .

```vue
<b-badge variant="primary" class="badge-glow">Primary</b-badge>
<b-badge class="badge-glow">Secondary</b-badge>
<b-badge variant="success" class="badge-glow">Success</b-badge>
<b-badge variant="danger" class="badge-glow">Danger</b-badge>
<b-badge variant="warning" class="badge-glow">Warning</b-badge>
<b-badge variant="info" class="badge-glow">Info</b-badge>
<b-badge variant="dark" class="badge-glow">Dark</b-badge>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/badge-glow.jpg')" alt="badge-glow" class="medium-zoom rounded bordered">

## Block Badge

Use `.d-block` class with `<b-badge>`, to display badge as a block element.

```vue
<b-badge class="d-block" variant="primary">Badge</b-badge>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/components/badge-block.jpg')" alt="badge-block" class="medium-zoom rounded bordered">
