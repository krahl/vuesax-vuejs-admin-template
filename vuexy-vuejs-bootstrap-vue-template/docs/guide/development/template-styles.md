# Template Styles

Here you will find where you can find template styles and how you can modify/override it. Also you will learn where to add your own styles.

## Core styles

`@core/scss` folder contains template's core styles. In this folder you will:

- **base directory:** Holds Template's base style independent of any framework (In this case - VueJS). e.g. Bootstrap components' styles, template layout styles, etc.
- **vue:** This folder contains styles for Vue version specific styles which isn't part of base styles. e.g. Some vue extension styles, bootstrap-vue extends, Vue specific page styles, etc.
- **core.scss:** This fille will get included in `main.js` file. This files imports all other styles of templates. So you can say that it is entry point of template styles.

## Writing your styles

You can write your styles in `src/assets/scss/style.scss` file. You can add your own structure in `src/assets/scss/` folder. `src/assets/scss/` Folder is completely yours to tweak it except you shall not remove or rename `src/assets/scss/variables` folder provided by us.

Let's understand what each file has to do with style in `src/assets/scss/` folder:

- `style.scss`: This files is your entry point of styles. Write your styles in this file or import other files. This file is imported in `main.js`
- `variables/_variables.scss`: You can override bootstrap variables using this file.
- `variables/_variables-components.scss`: Use this file to override component variables.

## Using @core for your styles

There might be case that you want to use style from our `@core` folder.

e.g. You want to create your own FAQ page with some extra components and want to extend our current page. In this case you can create a new SCSS file which imports our faq page style and write your own styles beneath it, just like a normal SCSS import. Lastly just import your newly created file in your SFC(Vue file).

Let's understand what each folder contains:

- `base`: Base styles
- `vue/apps`: This contains apps' styles. You **can import** it in your SFC and create your own app with our styles.
- `vue/bootstrap-vue`: This is bootstrap-vue extends and overrides of template. You **don't have to import** them in your SFC.
- `vue/libs`: This contains third party packages' styles. You **have to import** respective file while using third party package. e.g. If you are using _vue-select_ in your page then you need to import `vue-select.scss` file in your page.
- `vue/pages`: This contains styles for template pages. If you want style of specific package **import it** from this folder.
- `_animation.scss`: CSS Animations (Isn't used till now - Might get removed in future updates)
- `_base-extended.scss`: Extra styles of component or part of layout which isn't covered in `base` folder
- `_dark-layout.scss`: Extended dark-layout styles from base dark-layout
- `_index.scss`: all vue specific styles are imported in this file. And this file is imported in `core.scss` file.
- `_misc.scss`: misc styles
- `_rtl.scss`: RTL specific styles. Styles which isn't covered by postcss-rtl or RTL override styles.
- `_transitions.scss`: Vue Transition Styles. Used in router transition mainly.

::: tip
All SCSS files starting with `_`[underscore] (SCSS partials) are already imported in template styles so there's no need to import it. Other SCSS Files are meant to get imported only if they are required.
:::
