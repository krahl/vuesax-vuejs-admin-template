# Routing

In this page you will find how to add new routes and how we handled existing routes.

## Overview

You can find our template's router configuration in `src/router/index.js`. `src/router/routes` folder contains all routes of our template.

You can follow our pattern or use `src/router/index.js` to write your routes, it's completely up to you.

There's nothing special in `src/router/index.js` file, just ignore `router.beforeEach` for now we will explore it in ACL section. Still if there's something which is new to you, make sure you read `vue-router` docs.

## Route Meta

Our routes just isn't simple routes. Their meta is also required to render proper page. Let's find out each of them:

- **pageTitle**: This meta is used by breadcrumb to render Page Title. You will find this meta used in every route which have breadcrumb.
- **breadcrumb**: This is for rendering breadcrumb. Value of this meta is same as value of `items` prop of BootstrapVue's [breadcrumb](https://bootstrap-vue.org/docs/components/breadcrumb) component. There will always be home at the start of breadcrumb so don't mention it in meta value.
- **navActiveLink**: Navigation link to active in navigation menu. This shall match to any of [navigation menu item's route](/guide/development/navigation-menus.md). Useful if you have dynamic param and want to only set single item for it in navigation menu. e.g. Our Email & ToDo App.
- **resource**: CASL's subject value for route protection. You can read more about it in [Access Control](/guide/development/access-control.md) page.
- **action**: CASL's action value for route protection. You can read more about it in [Access Control](/guide/development/access-control.md) page.

Below two route meta is only for apps mentioned in [content render](/guide/layout/content-renderer.md) page.

- **contentRenderer**: Which [content render](/guide/layout/content-renderer.md) to use.
- **contentClass**: Meta to add class to `.content` wrapper. You won't use unless you use our four apps mentioned in content render docs.

## Route Protection (ACL)

You will learn how to add route protection for particular route in [Access Control](/guide/development/access-control.md) page.
