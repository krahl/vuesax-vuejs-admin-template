# Folder Structure

Understand folder structure of template and what everything contains

## Overview

Before checking folder structure it is better you know some stuff related to folder structure.

- `@core` folder contains core files of template which **shall not get modified** unless our support team guide you to do it.
- Outside of `@core` folder is files you can move and modify as your wish. Basically that is your playground where you can modify anything.
- `@core` folder contains layouts and styles but those are core files so you can still write your our layout using our layout and override our styles.
- `@fake-db` folder just contains dummy data which we get in response of API call. This enables us to take step forward in providing **API ready template**.
- `/src/layouts` folder outside of `@core` is your layouts which you can modify however you like. Template will render this layouts.

Other folders and details of it will be available in docs. This overview is here so you don't get confused.

## Main Package

Once you unzip the package from ThemeForest you will find folder named `vuexy-vuejs-bootstrap-vue-vX.X` folder which contains actual files and folders required to run Vuexy Admin.

```
├── public
│   ├── favicon.ico            -> Favicon
│   └── index.html             -> Main HTML
├── src
│   ├── @core                  -> Vuexy Admin Core Files
│   ├── @fake-db               -> Fake Database for mocking axios requests (Fake API Calls)
│   ├── layouts                -> Your Layouts
│   │   ├── horizontal
│   │   ├── full
│   │   └── vertical
│   ├── auth                   -> Your Authentication Files/Configuration
│   ├── navigation             -> Vertical & Horizontal Navigation menu files
│   │   ├── horizontal
│   │   └── vertical
│   ├── store                  -> Vuex Store
│   │   ├── app                -> General App module
│   │   ├── vertical-menu      -> Vertical Navigation Menu module
│   │   ├── index.js           -> Store index
│   │   └── app-config         -> App/Theme/Template Configuration module
│   │── router                 -> vue-router files
│   │   ├── index.js           -> Router index file
│   │   └── routes             -> Template routes
│   ├── views                  -> View files for all pages
│   ├── assets
│   │   ├── scss
│   │   └── images
│   ├── libs                   -> Third party libraries and their configurations
│   ├── App.vue                -> Application main vue file
│   ├── main.js                -> Application main js file
│   └── global-components.js   -> Application main js file
├── babel.config.js            -> Babel configuration
├── package.json               -> Package json
├── .gitignore                 -> gitignore
├── README.md                  -> README
├── postcss.config.js          -> postcss configuration
├── jsconfig.json              -> JavaScript language service file [VS Code]
├── .eslintrc.js               -> ESLint Configuration
├──  .prettierignore           -> Prettier Ignore
├── vue.config.js              -> VueJS configuration
├── .browserslistrc            -> Browser support
└── themeConfig.js             -> Vuexy Admin configuration
```

## @core folder

`@core` folder is core of our template which includes core files like layouts, composition functions, template components, etc.

**@core folder isn't meant to get modified.** When you will update of our template replacing this `@core` folder will hopefully update the template with minimum changes. This is improvement over our old structure which will ease your update process.

It's good idea to have a look at it and know what it contains to use stuff we already invented so **you don't have to reinvent the wheel**.

e.g. we created composition function for form validation which make 3rd party validation (vee-validate) easy to use.

### Understanding Core folder

Understanding `@core` folder will save your development time and you will know how to get most of our template.

- **app-config**

This folder contains composition function for app-config store module which make modifying themeConfig easy.

All you have to do is import this and update the property. Under the hood it will handle all store mutations and other actions like updating localStorage or updating other config.

```js{5,8}
import useAppConfig from '@core/app-config/useAppConfig'

export default {
  setup() {
    const { skin } = useAppConfig()

    const setDarkTheme = () => {
      skin.value = 'dark'
    }

    return { setDarkTheme }
  },
}
```

::: tip
Refer to [theme configuration](/guide/development/theme-configuration.md) section to get all valid values for config option.
:::

- **assets**

Currently this folder just contains feather font icons.

:::danger
In next release, feather font icon assets may get moved outside of @core folder.
:::

- **auth**

This folder contains auth related files. Currently, It has JWT service and other supporting files. Please refer to [JWT page](/guide/development/authentication.md) for getting detailed information on this.

- **comp-functions**

It contains some useful composition function. You can find detailed explanation on this in [composition function](/#) page.

- **components**

It contains core component of template. Make sure to check them all in our custom components section.

- **directives**

As the name suggest this contains directives of our template. Currently, it only contains animation directive and we only have one directive for alert for now. You can find usage of this directive in our [alert animation demo](/guide/components/alert.md#alert-animation).

- **layouts**

This contains layouts of our template and layout components. You can find detailed information in [layout](/guide/layout/layout-types.md) section.

- **mixins**

This contains vue mixins. It there till we completely migrate to Vue 3 where we will convert them to composition functions.

- **scss**

This folder contains core styles of template. You can find more details and folder structure of it in [SCSS folder structure](/guide/development/template-styles.md) section.

- **utils**

It has some utils which can be useful in your development. It's `validations` folder contains vee-validate validators which you can use in form validation. Other than this everything else in folder is self explanatory.

However, `filter.js` file is just filter (Vue 2 template filter) functions.
