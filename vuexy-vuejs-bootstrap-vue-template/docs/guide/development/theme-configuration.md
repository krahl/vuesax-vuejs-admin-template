# Theme Configurations

Change template to fit your needs. To configure template we will use `themeConfig.js` file, which have all template configurations listed with their valid values.

## Using themeConfig.js

In `themeConfig.js` file you will find three constants

1. `$themeColors`: Bootstrap Colors in JS. Will get updated in `App.vue`. Don't touch 😅
2. `$themeBreakpoints`: Bootstrap breakpoints. Will get updated in `App.vue`. Don't touch 😅
3. `$themeConfig`: This is the file where you can update template behavior.

`$themeConfig` object options and it's valid values are already listed there. Hence, it is self explanatory. Just changed configuration as your requirement and you are done.

## Change template colors

To update template colors just update bootstrap color variables in `src/assets/scss/variables/_variables.scss`

```scss
$primary: #00cfe8; // Changing primary color
```
