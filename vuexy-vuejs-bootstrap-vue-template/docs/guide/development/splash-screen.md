# Splash Screen

Vuexy now provides splash screen for heavy projects which requires more time to load.

## Usage

Splash screen is already included in full version and starter-kit so you don't have to include it separately. If you are using older version of template please follow this [commit](https://gitlab.com/pixinvent/vuesax-vuejs-admin-template/-/commit/87dac308fc2c9a16437374d0109f4e46d3ef3870) changes for adding it in your project.

::: warning
GitLab access is required to view above commit. Please checkout GitLab access [page](/guide/getting-started/gitlab-access.md) in our docs.
:::

## Customization

### Logo

Please update your company logo in public directory to show your desired loading.

### Color

As Vue isn't initialized you can't use Bootstrap's variables in `public/loader.css`. Please update the color value in `public/loader.css` according to desired color.
