# Radio

Radio component also supports bootstrap colors to give extra colors to radio

## Radio Colors

To change the color of the radio use the `.custom-control-{color}` class.

You can check demo in "Color" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/forms/form-element/radio) page.
