# Form Wizard

`vue-form-wizard` component is slightly modified to make it more beautiful. Let's have a glance.

## Requirement

Form wizard requires font icon version of feather icons. So please make sure you **import feather font icons** in your app if you are using form wizard.

```js
// File: main.js

require('@core/assets/fonts/feather/iconfont.css')
```

## Basic Wizard

We have modified the look of the wizard as per the template theme. Add `vue-wizard.scss` file for basic theme styles.

## Horizontal transparent step wizard

Add class `steps-transparent` along with `<form-wizard>` component for transparent background styling of steps.

## Vertical Wizard

Add `layout="vertical"` Prop for vertical layout wizard and add class `wizard-vertical` along with `<form-wizard>` component for theme styling.

## Vertical Transparent Wizard

Add class `vertical-steps steps-transparent` along with `<form-wizard>` component for transparent background styling of steps for the vertical wizard.

You can find all demos on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/forms/form-wizard) page.
