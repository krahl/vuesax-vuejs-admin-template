# Flat Pickr

We added some styles for Flat Picker component.

## Validation

`flatPickr` component don't provide validation styles. Hence, we managed to style it for falsy validation.

Just like [vue-select validation](/guide/forms/vue-select.md#validation) you need to wrap component in `b-form-group` and provide `state` prop to `b-form-group`.

You can check demo in Calendar App's add new event form on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/apps/calendar) page.
