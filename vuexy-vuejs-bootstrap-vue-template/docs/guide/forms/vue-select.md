# Vue Select

vue-select component have extra styles regarding validation and component size

## Validation

To get style for invalid state of vue-select wrap it in `b-form-group` and provide `state` prop to `b-form-group`.

<!-- prettier-ignore-start -->
```vue{11}
<template>
  <validation-observer ref="infoRules" tag="form">
    <validation-provider
      #default="{ errors }"
      name="Country"
      rules="required"
    >
      <b-form-group
        label="Country"
        label-for="country"
        :state="errors.length > 0 ? false:null"
      >
        <v-select
          id="country"
          v-model="selectedContry"
          :options="countryName"
          :selectable="option => ! option.value.includes('select_value')"
          label="text"
        />
        <b-form-invalid-feedback :state="errors.length > 0 ? false:null">
          {{ errors[0] }}
        </b-form-invalid-feedback>
      </b-form-group>
    </validation-provider>

    <b-button @click="validationFormInfo" variant="primary" v-ripple.400="'rgba(255, 255, 255, 0.15)'">Submit</b-button>

  </validation-observer>
</template>

<script>
import vSelect from 'vue-select'
import { ValidationProvider, ValidationObserver } from 'vee-validate'
import { required } from '@validations'

export default {
  components: {
    ValidationProvider,
    ValidationObserver,
    vSelect,
  },
  data() {
    return {
      selectedContry: '',
      countryName: [
        { value: 'select_value', text: 'Select Value' },
        { value: 'Russia', text: 'Russia' },
        { value: 'Canada', text: 'Canada' },
        { value: 'China', text: 'China' },
        { value: 'United States', text: 'United States' },
        { value: 'Brazil', text: 'Brazil' },
        { value: 'Australia', text: 'Australia' },
        { value: 'India', text: 'India' },
      ],
    }
  },
  methods: {
    validationFormInfo() {
      return new Promise((resolve, reject) => {
        this.$refs.infoRules.validate().then(success => {
          if (success) {
            resolve(true)
          } else {
            reject()
          }
        })
      })
    },
  },
}
</script>


<style lang="scss">
  @import '~@core/scss/vue/libs/vue-select.scss';
</style>
```
<!-- prettier-ignore-end -->

_Result:_

<img :src="$withBase('/images/demo-imgs/forms/vue-select-validation.jpg')" alt="vue-select-validation" class="medium-zoom rounded bordered">

## Size

For different sizes of vue-select, Use classes `.select-size-sm` & `.select-size-lg`.

You can check demo in "Size" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/forms/form-element/vue-select) page.
