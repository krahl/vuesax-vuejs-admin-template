# Checkbox

Checkbox component is slightly modified to make it more beautiful

## Checkbox Colors

To change the color of the CheckBox use the `.custom-control-{color}` class.

```vue
<template>
  <div class="demo-inline-spacing">
    <b-form-checkbox v-model="selected" value="A" class="custom-control-primary">
      Primary
    </b-form-checkbox>
    <b-form-checkbox v-model="selected" value="CC" class="custom-control-secondary">
      Secondary
    </b-form-checkbox>
    <b-form-checkbox v-model="selected" value="C" class="custom-control-success">
      Success
    </b-form-checkbox>
    <b-form-checkbox v-model="selected" value="F" class="custom-control-danger">
      Danger
    </b-form-checkbox>
    <b-form-checkbox v-model="selected" value="D" class="custom-control-warning">
      Warning
    </b-form-checkbox>
    <b-form-checkbox v-model="selected" value="G" class="custom-control-info">
      Info
    </b-form-checkbox>
  </div>
</template>
<script>
export default {
  data() {
    return {
      selected: ['A', 'C', 'CC', 'B', 'D', 'E', 'F', 'G'],
    }
  },
}
</script>
```

_Result:_

<img :src="$withBase('/images/demo-imgs/forms/checkbox-colors.jpg')" alt="checkbox-colors" class="medium-zoom rounded bordered">
