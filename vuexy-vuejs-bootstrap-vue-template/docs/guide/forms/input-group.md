# Input Group

We have customized input group little bit in our template to make it more beautiful

## Merged input group

Add class `.input-group-merge` to `b-input-group` component to create input group without border.

You can check demo in "Merged" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/forms/form-element/input-group) page.

## Validation with merged input group

You'll need to add `.is-valid` or `.is-invalid` class to `b-input-group` conditionally to get validation state styles.

```vue
<template>
  <b-input-group class="input-group-merge" :class="validation ? 'is-valid' : 'is-invalid'">
    <!-- Other stuff -->
  </b-input-group>
</template>
```

You can check demo in "Form Feedback helpers" card on [this](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/forms/form-layout) page.
