---
home: true
heroImage: /logo.svg
heroText: Vuexy - Vuejs Admin Dashboard Template
tagline: Best selling, Production Ready, Carefully Crafted, Extensive Admin Template
actionText: Get Started →
actionLink: /guide/
footer: COPYRIGHT © 2021 Pixinvent, All rights Reserved
---

::: danger Hey! Check this out before going anywhere 📣
This documentation is for **Bootstrap Vue version(Latest)**. If you want to check documentation for **Vuesax version(Deprecated)** please visit [here](https://www.pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/documentation-vuesax-version/). For **Vuesax version(Deprecated) Demo** please visit [here](https://pixinvent.com/demo/vuesax-vuexy-vuejs-admin-dashboard-template/demo-1/)
:::

<div class="features">
  <div class="feature">
    <h2>Based on BootstrapVue</h2>
    <p>With BootstrapVue you can build responsive, mobile-first, and ARIA accessible projects on the web</p>
  </div>
  <div class="feature">
    <h2>Composition API</h2>
    <p>Composition API is a set of additive, function-based APIs that allow flexible composition of component logic.</p>
  </div>
  <div class="feature">
    <h2>JWT Auth, ACL, etc.</h2>
    <p>Vuexy provides extendable JWT auth which can be configured easily and provides ready to use ACL</p>
  </div>
</div>
