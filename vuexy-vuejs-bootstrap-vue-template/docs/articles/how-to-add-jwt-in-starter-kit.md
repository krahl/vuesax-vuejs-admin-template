# How to add JWT in starter-kit

We already provided JWT folder for developer usage in `src/auth/jwt` even if you are using starter-kit. It just isn't used in Login.

All you have to do is using it's jwt service:

<!-- prettier-ignore-start -->
```vue{7,13-22}
<template>
  <!-- Login Form UI -->
</template>

<script>
// Other imports
import useJwt from '@/auth/jwt/useJwt'

export default {
  // Other stuff
  methods: {
    login() {
      useJwt.login({
        email: this.userEmail,
        password: this.password,
      })
      .then(() => {
        // Do something on success
      })
      .catch(() => {
        // Do something on error
      })
    },
  },
}
</script>
```
<!-- prettier-ignore-end -->

This is same for register. You can use JWT helper methods like `setToken` as well.

Certainly, if you have any configuration regrading JWT then you might have to modify it a little bit. Please check our docs on [JWT](/guide/development/authentication.md).
