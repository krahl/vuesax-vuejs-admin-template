# How To Update Vuexy To Latest Version

We are making template update as easy as possible from our side. This time from v6.1 we updated our folder structure so updating process can be easy than ever. 😍

::: tip
If you are using Vue only (Not Vue + Laravel) then mostly replacing `@core` folder in your project is enough. Still, verifying if something outside of `@core` is changed or not is always helpful and keeps you error free. 😇
:::

Let's check how you can view each and every change in our template's latest release with ease.

## Prerequisite

- [VS Code](https://code.visualstudio.com/download)
- VS Code Extension: [Compare Folders](https://marketplace.visualstudio.com/items?itemName=moshfeu.compare-folders)
- Vuexy Package you started your project with (Older version of vuexy package which you used)
- [Vuexy Latest Package](https://themeforest.net/downloads)

## Step 1

Install VS Code and install "Compare Folders" extension using above link.

## Step 2

unzip/extract/decompress Vuexy Latest Package and Vuexy Old package in your desired folder

## Step 3

Use compare folders extension and compare both packages for changes to adopt them in your project.
