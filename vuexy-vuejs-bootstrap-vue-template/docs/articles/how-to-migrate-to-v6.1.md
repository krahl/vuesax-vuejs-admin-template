# How to migrate to v6.1

As v6.1 is built upon a completely new UI framework [BootstrapVue](https://bootstrap-vue.org/), the best way to get started or migrate to this version is to use v6.1's starter-kit and implement your code from [Vuesax](https://lusaxweb.github.io/vuesax/) version to this v6.1 starter-kit.

As this is not updated over the existing framework you have to move your code from your existing project to a new update rather than moving a new update in your existing project.

As we introduced a new UI framework you might have to update your UI code as well.

If you have questions regarding the deprecation of Vuesax version, please check our [FAQs](/faq/) for the answer.
