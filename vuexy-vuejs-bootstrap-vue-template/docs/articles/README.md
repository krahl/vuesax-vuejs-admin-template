---
pageClass: page-article-list
---

# Articles

[How to migrate to v6.1](/articles/how-to-migrate-to-v6.1.md)

[Frontend Authentication Flow](/articles/frontend-authentication-flow.md)

[How to add JWT in starter-kit](/articles/how-to-add-jwt-in-starter-kit.md)

[How to add i18n in starter-kit](/articles/how-to-add-i18n-in-starter-kit.md)

[How to add ACL in starter-kit](/articles/how-to-add-acl-in-starter-kit.md)

[How To Update Vuexy To Latest Version](/articles/how-to-update-vuexy-to-latest-version.md)

[How To Remove Fake DB And Use Real API](/articles/how-to-remove-fake-db-and-use-real-api.md)

<br />
<br />

:::tip Loved our Documentation and Template?
If you liked our template and detailed docs, please give us ⭐⭐⭐⭐⭐ on ThemeForest. Your review motivates us to work even hard and provide top-notch features and support.
:::
