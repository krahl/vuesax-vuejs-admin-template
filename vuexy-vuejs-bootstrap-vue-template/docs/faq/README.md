---
sidebar: 'auto'
---

# FAQ

::: tip
If you can't find your problem. Try searching related term as well.
:::

::: tip I can't find my issue/problem/question listed here, What to do now?
If you can't find your issue/problem/question listed here then you can [raise support](/guide/getting-started/support.md) at our support portal. For **getting your solution quickly with minimum conversation**, please check out our getting support page which provides some tips on how to raise perfect support ticket to get solution quickly so our development team don't have to ask for things which takes time and waste your precious development time.
:::

## Vue 3 Update

Our base framework [BootstrapVue](https://github.com/bootstrap-vue/bootstrap-vue) still don't have support for Vue 3. You can find relative issue on [GitHub](https://github.com/bootstrap-vue/bootstrap-vue/issues/5196), please follow it for upcoming Vue 3 updates.

As we also use third party libraries, till they provide support for vue 3 or till we find alternative libraries for them it is not advisable to use template with Vue 3.

However, we are providing Vue 3 Ready template (still using Vue 2 but with power of Vue 3 Composition API as plugin).

Once we have all things required to move to Vue 3, We will start development for it.

## Bootstrap 5 support

Same as Vue 3 support, once BootstrapVue support Bootstrap 5 we will start development for it. You can find relative issue on [GitHub](https://github.com/bootstrap-vue/bootstrap-vue/issues/5507).

## npm install or yarn install errors

Causes of npm install or yarn install issue can be due to various things which include:

- Missing or inappropriate dependencies like node or some other environmental issues
- Dependency resolved by package manager (npm/yarn) conflict with other installed dependency
- The dependency of the package we use have an internal issue or that dependency has some issue with your environment
- Package or dependency of the package requires some additional step or configuration to work in your environment
- Downloaded package is broken or is got tampered

To resolve such installation issue:

- Please try again downloading fresh package/zip from ThemeForest and perform installation again.
- Please make sure you are using LTS version of node which is recommended and not one with latest features.
- run `yarn cache clean` or `npm cache clean`
- Try using yarn if possible (Recommended).

After following about steps still, you are getting the error please [raise support](/guide/getting-started/support.md) at our support portal with the below details:

- Your OS information, Node version, npm/yarn version, Template/Package version
- Mention if you are able to run fresh VueJS project using Vue CLI without our template
- Attache log file of error you are getting in your console (Provide full log)
- Mention which command you are running
- Mention If you have any other machine, are you able to run our template in that machine

## npm install or yarn install warnings or installation warnings

You might get some warnings while running `npm install` or `yarn install` like below:

```bash
info fsevents@2.3.1: The platform "linux" is incompatible with this module.
info "fsevents@2.3.1" is an optional dependency and failed compatibility check. Excluding it from installation.
info fsevents@1.2.13: The platform "linux" is incompatible with this module.
info "fsevents@1.2.13" is an optional dependency and failed compatibility check. Excluding it from installation.
[3/4] Linking dependencies...
warning " > bootstrap@4.5.3" has unmet peer dependency "jquery@1.9.1 - 3".
warning " > bootstrap@4.5.3" has unmet peer dependency "popper.js@^1.16.1".
warning " > vue2-leaflet@2.6.0" has unmet peer dependency "@types/leaflet@^1.5.7".
warning "@vue/eslint-config-airbnb > eslint-import-resolver-webpack@0.13.0" has unmet peer dependency "webpack@>=1.11.0".
warning " > sass-loader@10.1.0" has unmet peer dependency "webpack@^4.36.0 || ^5.0.0".
```

The warnings you are receiving while installing is from library/packages we used.

We always keep our packages up to date when we make major release. However, the writer of that package may use an older dependency that is no longer maintained for backward compatibility or any other code related issue. But, that's not an issue. Those packages will work fine with our template.

Even if you like to try you can install this packages in fresh VueJS project without our template and you will get the same.

## How to disable linting in template

To disable linting in your project you can add new property in `vue.config.js` file as below:

```js{3}
module.exports = {
  ...
  lintOnSave: false,
  ...
}
```

## Getting error: `Syntax Error: Support for the experimental syntax ‘classProperties’ isn’t currently enabled`

For solving this issue all you have to add is new plugin in babelrc file.

Please update your `.babelrc` file as below:

```js{5}
{
  "presets": [    ...
  ],
  "plugins": [    ...
    "@babel/plugin-proposal-class-properties",
  ]
}
```

This will hopefully solve this error.

## Getting error: `Support for the experimental syntax 'jsx' isn't currently enabled`

For solving this issue all you have to add is new plugin in babelrc file.

Please update your `.babelrc` file as below:

```js{5}
{
  "presets": [    ...
  ],
  "plugins": [    ...
    "transform-vue-jsx",
  ]
}
```

This will hopefully solve this error.

## Why reproduce issue/bug in starter-kit?

Reproducing the issue/bug in our starter-kit with minimal dependencies can help in resolving issue/bug as quickly as possible rather than asking to try various things which will cost you valuable time.

If your bug/issue is related to UI and doesn't include laravel/any-other-backend and your issue/bug needs to show some dummy data please provide an example with dummy data via JSON file rather than providing full database and other backend related stuff.

I hope you will understand reproducing the issue in starter-kit as Q&A for technical issue/bug is a matter of trial and error. With the reproduced issue in the starter-kit, we will be able to provide you a worthy response in a single response.

::: tip
If your issue is not related to laravel, please reproduce the issue in vue only starter-kit even if you are using Laravel + Vue.
:::

::: danger NOTE
Please do not upload source code to public GitHub repo or any publicly available sources.
:::

## Authentication/ACL gives "Unexpected token u in JSON at position 0"

You might get this if you have corrupted or wrong data in your `localStorage`. **Please clear localStorage to resolve this issue.**

This error can be caused by below code:

```js
JSON.parse(localStorage.getItem('userData'))
```

In above code we are getting `userData` from localStorage and due to invalid JSON value in your localStorage you might get this error.

## ACL - I have role based system for access control instead ability based

If you are using our ACL provided in our template then it is ability-based ACL not role-based. You can learn more about using role-based system using the same package we used. Please read in their official docs:

[CASL - Roles With Static Permissions](https://casl.js.org/v5/en/cookbook/roles-with-static-permissions)

## I don't have refresh token in my authentication flow

Our JWT service expects refresh token flow and provided flow might need some code update according to your API response.

If your authentication system don't have refresh token flow then please consider creating your own JWT service instead of using one provided in template. You can refer to existing JWT service for inspiration.

## How to check what frontend expects in API response

You can find all our fake API responses in "src/@fake-db" folder.

You can get list of fake API endpoints by searching for `mock.on` in "src/@fake-db" folder.

::: tip
You can find authentication related fake responses in "src/@fake-db/jwt/index.js" file.
:::

## How to migrate to v6.1 or Migration guide for v6.1

As v6.1 is built upon a completely new UI framework BootstrapVue, the best way to get started or migrate to this version is to use v6.1's starter-kit and implement your code from Vuesax version to this v6.1 starter-kit.

As this is not updated over the existing framework you have to move your code from your existing project to a new update rather than moving a new update in your existing project.

As we introduced a new UI framework you might have to update your UI code as well.

If you have questions regarding the deprecation of Vuesax version, please check our FAQs for the answer.

## How to update logo and text

You can update the logo and text from "themeConfig.js" file. Please update `$themeConfig` variable's `app.appName` and `app.appLogoImage` to update the text and logo.

## NuxtJS support and its integration

As Admin templates are meant for backend and it doesn't require SEO we didn't provide support for NuxtJS. If you want to integrate NuxtJS in our template please consider checking some online articles on how to migrate VueJS project to NuxtJS.

Here's some of what we have found:

- [Official NuxtJS Docs](https://nuxtjs.org/)
- [How to migrate a VueJs project to NuxtJS in 8 steps](https://dev.to/ngranja19/how-to-migrate-a-vuejs-project-to-nuxtjs-in-8-steps-41dm)

## How to update navbar, breadcrumb or footer

You can use layout slots to add your custom navbar, breadcrumb or footer. Please refer to [layout](/guide/layout/layout-types.md) docs for example with code snippet.

## What is usage and purpose of b-card-code component

In our full version package you will find usage of `b-card-code` component in most pages. This is custom component created by us for demo purposes to show code snippet (click code icon in right top corner) of the demo your viewing. This component is just enhancement over b-card component for showing code, you don't have to use it.

**What to do with usage of this component in page?**

Just remove the `b-card-code` component from page and replace with [b-card](https://bootstrap-vue.org/docs/components/card) component provided by BootstrapVue. Generally code snippet is imported from `code.js` file so once you removed `b-card-code` from page also **remove code snippet import** from page and finally **remove that code.js** file.

Again, `b-card-code` is just for demo purpose, you can remove it.

## BootstrapVue table vs vue-good-table

It is **recommended** that you use **BootstrapVue's table** rather than vue-good-table because BootstrapVue's table will have feature updates and bug fixes quicker than any third party table package. However, we can't the same for vue-good-table, also there's no certainty that vue-good-table will have Vue 3 support.

## eCommerce Shop filters' not working

In older version of vuexy we provided Algolia Search. We used it's large dataset to apply filters on. As we removed Algolia Search in latest Vuexy Release we removed that large dataset and used our own data. Filtering on small size of data is not appropriate so we disabled filters and kept just UI.

## Why agGrid got removed

In our v6.1 version we removed agGrid. This is because agGrid was not completely free as it requires another purchase to get access to all of its features. Since [BootstrapVue](https://bootstrap-vue.org/) provides a decent table component and we also added [vue-good-table](https://github.com/xaksis/vue-good-table)(3rd party - Not Vue 3 ready yet) package, it is easier to use them over agGrid and also you don't require any additional purchase.

So, we highly recommend you use BootstrapVue table component for your projects over any other table.

However, this doesn't mean we won't support agGrid table for our Vuesax version. We just didn't provide it in our BootstrapVue version. So, till we provide Vuesax version there will be support for agGrid table for our template, don't worry.

We care for our customers and that's why we didn't remove Vuesax version and provided both versions in our v6.1 update. So, you can still raise support tickets for Vuesax version.

## What about Vuesax version and it's Support

We haven't abandoned Vuesax version of our template. As of v6.1 Vuesax version of our template is deprecated. Still, you can use it but with a caution that in future it will be removed. As Vuesax version will be removed there will be no updates of it and no support will be provided.

However, We care for our customers. We won't just remove vuesax according to our will. Once, most customers find BootstrapVue version is more useful, productive, future-oriented, stable, and robust we will start to take action on Vuesax version of the framework.

If you have question on Why we are removing Vuesax version of the template, you can find the answer in our FAQ section.

## Why we are removing Vuesax version

There are some issues we are facing with Vuesax version:

- Buggy Framework
- Framework v3 Abandoned by the author (Working on a completely new version with new license)
- Not active in resolving issues
- Still in BETA since we started using it
- No PR approval
- No Vue 3

Due to these reasons, we have to create our own forked version of the framework where we are fixing bugs on our own. Finally, due to the increasing demand for [BootstrapVue](https://bootstrap-vue.org/) version of Vuexy. We are providing Vuexy with more stable and robust framework with a bigger community which is BootstrapVue.

You can find how it will affect support for Vuesax version in our FAQ section.
