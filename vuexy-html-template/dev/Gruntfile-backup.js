module.exports = function(grunt) {

    var myLayout = grunt.option('Layout'); // Jade layout name
    var myLayoutName = grunt.option('LayoutName'); // Created layout folder name
    var myTextDirection = grunt.option('TextDirection'); // Text direction (Eg. LTR, RTL)

    // configure the tasks
    grunt.initConfig({

        /*
         * Copy all the required content to dist folder
         */
        copy: {
            dist: {
                cwd: '',
                // src: ['**/*', '!**/node_modules/**', '!**/sass/**', '!**/dist/**', '!**/bin/**', '!**/temp/**', '!**/templates/**', '!**/jade/**', '!.gitgnore', '!package.json', '!package.js', '!bower.json', '!Gruntfile.js'],
                src: ['**/assets/**/*','**/stack-assets/**/*'],
                dest: 'dist',
                expand: true,
            }
        },

        /*
         * SASS Compilation to css and minification
         */
        sass: { // Task
            // Compile main theme css
            expanded: {
                options: {
                    outputStyle: 'expanded',
                    sourcemap: false,
                },
                files: {
                    'stack-assets/css/bootstrap.css': 'stack-assets/scss/bootstrap.scss',
                    'stack-assets/css/bootstrap-stack.css': 'stack-assets/scss/bootstrap-stack.scss',
                    'stack-assets/css/components.css': 'stack-assets/scss/components.scss',
                    'stack-assets/css/colors.css': 'stack-assets/scss/colors.scss',
                    'stack-assets/css-rtl/custom-rtl.css': 'stack-assets/scss/custom-rtl.scss',
                    'assets/css/style.css': 'assets/scss/style.scss',
                    'assets/css/style-rtl.css': 'assets/scss/style-rtl.scss',
                }
            },

            // Compile the dist css
            dist: {
                options: {
                    outputStyle: 'compressed',
                    sourcemap: false,
                },
                files: {
                    'dist/stack-assets/css/bootstrap.min.css': 'stack-assets/scss/bootstrap.scss',
                    'dist/stack-assets/css/bootstrap-stack.min.css': 'stack-assets/scss/bootstrap-stack.scss',
                    'dist/stack-assets/css/components.min.css': 'stack-assets/scss/components.scss',
                    'dist/stack-assets/css/colors.min.css': 'stack-assets/scss/colors.scss',
                    'dist/stack-assets/css-rtl/custom-rtl.min.css': 'stack-assets/scss/custom-rtl.scss',
                    'dist/assets/css/style.min.css': 'assets/scss/style.scss',
                    'dist/assets/css/style-rtl.min.css': 'assets/scss/style-rtl.scss'
                }
            },

            // Minify dist css for speed optimization
            /*min: {
                options: {
                    outputStyle: 'compressed',
                    sourcemap: false
                },
                files: {
                    'dist/stack-assets/css/bootstrap.min.css': 'dist/stack-assets/scss/bootstrap.scss',
                    'dist/stack-assets/css/bootstrap-stack.min.css': 'dist/stack-assets/scss/bootstrap-stack.scss',
                    'dist/stack-assets/css/components.min.css': 'dist/stack-assets/scss/components.scss',
                    'dist/stack-assets/css/colors.min.css': 'stack-assets/scss/colors.scss',
                    'dist/stack-assets/css-rtl/custom-rtl.min.css': 'stack-assets/scss/custom-rtl.scss',
                    'dist/assets/css/style.min.css': 'dist/assets/scss/style.scss',
                    'dist/assets/css/style-rtl.min.css': 'dist/assets/scss/style-rtl.scss',
                }
            },*/

            // Compile bin css
            /*bin: {
                options: {
                    outputStyle: 'expanded',
                    sourcemap: false
                },
                files: {
                    'bin/stack-assets/css/bootstrap.css': 'stack-assets/scss/bootstrap.scss',
                    'bin/stack-assets/css/bootstrap-stack.css': 'stack-assets/scss/bootstrap-stack.scss',
                    'bin/stack-assets/css/components.css': 'stack-assets/scss/components.scss',
                    'bin/stack-assets/css/colors.css': 'stack-assets/scss/colors.scss',
                    'bin/stack-assets/css-rtl/custom-rtl.css': 'stack-assets/scss/custom-rtl.scss',
                    'bin/assets/css/style.css': 'assets/scss/style.scss',
                    'bin/assets/css/style-rtl.css': 'assets/scss/style-rtl.scss',
                }
            }*/
        },

        /*
         * Auto prefix css using PostCss Autoprefixer
         */
        postcss: {
            options: {
                processors: [
                    require('autoprefixer')({
                        browsers: [
                            'last 2 versions',
                            '> 5%', //versions selected by global usage statistics.
                            'Chrome >= 25',
                            'Safari >= 8',
                            'Firefox >= 15',
                            'Opera >= 12',
                            'ie >= 9', // Sorry IE :< 9
                        ]
                    })
                ]
            },
            main: {
                src: [
                    'stack-assets/css/bootstrap.css',
                    'stack-assets/css/bootstrap-stack.css',
                    'stack-assets/css/components.css',
                    'stack-assets/css/colors.css',
                    'stack-assets/css-rtl/custom-rtl.css',
                    'assets/css/style.css',
                    'assets/css/style-rtl.css'
                ]
            },
            dist: {
                src: [
                    'dist/stack-assets/css/bootstrap.min.css',
                    'dist/stack-assets/css/bootstrap-stack.min.css',
                    'dist/stack-assets/css/components.min.css',
                    'dist/stack-assets/css/colors.min.css',
                    'dist/stack-assets/css-rtl/custom-rtl.min.css',
                    'dist/assets/css/style.min.css',
                    'dist/assets/css/style-rtl.min.css'
                ]
            },
            /*expended: {
                src: [
                    'dist/stack-assets/css/bootstrap.css',
                    'dist/stack-assets/css/bootstrap-stack.css',
                    'dist/assets/css/style.css',
                    'dist/assets/css/style-rtl.css'
                ]
            },
            min: {
                src: [
                    'dist/stack-assets/css/bootstrap.min.css',
                    'dist/stack-assets/css/stack.min.css',
                    'dist/assets/css/style.min.css',
                    'dist/assets/css/style-rtl.min.css'
                ]
            },
            bin: {
                src: [
                    'bin/stack-assets/css/bootstrap.css',
                    'bin/stack-assets/css/bootstrap-stack.css',
                    'bin/assets/css/style.css',
                    'bin/assets/css/style-rtl.css'
                ]
            }*/
        },

        /*
         * Browser Sync integration
         */
        browserSync: {
            bsFiles: ["bin/*.js", "bin/*.css", "*.jade" , "!**/node_modules/**/*"],
            options: {
                server: {
                    baseDir: "./", // make server from root dir
                    directory: true
                },
                port: 8000,
                ui: {
                    port: 8080,
                    weinre: {
                        port: 9090
                    }
                },
                open: false
            }
        },

        /*
         * Concat js files
         */
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                // the list of files to concatenate
                src: [
                    "js/initial.js",
                    "js/jquery.easing.1.3.js",
                ],
                // the location of the resulting JS file
                dest: 'dist/js/stack.js'
            },
            temp: {
                // the files to concatenate
                src: [
                    "js/initial.js",
                    "js/jquery.easing.1.3.js",
                ],
                // the location of the resulting JS file
                dest: 'temp/js/stack.js'
            },
        },

        /*
         * Uglify : To minify JS files
         */
        uglify: {
            dist: {
                options: {
                    // mangle: true
                },
                files: [{
                    expand: true,
                    src: '**/*.js',
                    dest: 'dist/stack-assets',
                    cwd: 'stack-assets/js',
                }]
            }
        },
        // uglify: {
        //     options: {
        //         // Use these options when debugging
        //         // mangle: false,
        //         compress: true,
        //         //beautify: true

        //     },
        //     dist: {
        //         /*files: {
        //             'dist/stack-assets/js/core/stack.min.js': ['stack-assets/js/core/stack.js'],
        //             'dist/stack-assets/js/core/stack-menu.min.js': ['stack-assets/js/core/stack-menu.js'],
        //             'dist/stack-assets/js/core/stack-rtl.min.js': ['stack-assets/js/core/stack-rtl.js'],
        //         },*/
        //         files: {
        //             src: 'stack-assets/js/core/**/*.js',  // source files mask
        //             dest: 'dist/all.js',    // destination folder
        //             expand: true,    // allow dynamic building
        //             flatten: true,   // remove all unnecessary nesting
        //             ext: '.min.js'   // replace .js to .min.js
        //         }
        //     },
        //     /*bin: {
        //         files: {
        //             'bin/stack-assets/js/core/stack.min.js': ['temp/js/stack.js'],
        //         }
        //     },*/
        //     extras: {
        //         files: {
        //             //for other custom js
        //             //'extras/noUiSlider/nouislider.min.js': ['extras/noUiSlider/nouislider.js']
        //         }
        //     }
        // },


        /*
         * Compress final build in zip folder
         */
        compress: {
            main: {
                options: {
                    archive: 'bin/stack.zip',
                    level: 6
                },
                files: [{
                    expand: true,
                    cwd: 'dist/',
                    src: ['**/*'],
                    // dest: 'LTR/'
                }, {
                    expand: true,
                    cwd: './',
                    src: ['LICENSE', 'README.md'],
                    // dest: 'LTR/'
                }, ]
            },

            src: {
              options: {
                archive: 'bin/materialize-src.zip',
                level: 6
              },
              files: [{
                  expand: true,
                  cwd: 'font/',
                  src: ['**/*'],
                  dest: 'materialize-src/font/'
                }, {
                  expand: true,
                  cwd: 'sass/',
                  src: ['materialize.scss'],
                  dest: 'materialize-src/sass/'
                }, {
                  expand: true,
                  cwd: 'sass/',
                  src: ['components/**/*'],
                  dest: 'materialize-src/sass/'
                }, {
                  expand: true,
                  cwd: 'js/',
                  src: [
                    "initial.js",
                    "jquery.easing.1.3.js",
                    "animation.js",
                    "velocity.min.js",
                    "hammer.min.js",
                    "jquery.hammer.js",
                    "global.js",
                    "collapsible.js",
                    "dropdown.js",
                    "leanModal.js",
                    "materialbox.js",
                    "parallax.js",
                    "tabs.js",
                    "tooltip.js",
                    "waves.js",
                    "toasts.js",
                    "sideNav.js",
                    "scrollspy.js",
                    "forms.js",
                    "slider.js",
                    "cards.js",
                    "pushpin.js",
                    "buttons.js",
                    "transitions.js",
                    "scrollFire.js",
                    "date_picker/picker.js",
                    "date_picker/picker.date.js",
                    "character_counter.js",
                    "carousel.js",
                  ],
                  dest: 'materialize-src/js/'
                }, {
                  expand: true,
                  cwd: 'dist/js/',
                  src: ['**/*'],
                  dest: 'materialize-src/js/bin/'
                }, {
                  expand: true,
                  cwd: './',
                  src: ['LICENSE', 'README.md'],
                  dest: 'materialize-src/'
                }

              ]
            },
        },


        /*
         * Clean folder
         */
        clean: {
            dist: {
                src: ['dist/']
            },
            temp: {
                src: ['temp/']
            },
        },

        //  Jade
        jade: {
            html: {
                options: {
                    basedir: 'jade/stack-builder/pages',
                    pretty: true,
                    client: false,
                    data: {
                        debug: true,
                        useLayout: myLayout, // Predefined layout name i.e vertical-light-sidebar
                        useDirection: myTextDirection
                    }
                },
                files: [{
                    cwd: 'jade/stack-builder/pages',
                    src: ['*.jade', '!**/template.jade'],
                    dest: 'html/' + myTextDirection + '/' + myLayoutName,
                    expand: true,
                    ext: ".html"
                }]
            },
            dist: {
                options: {
                    basedir: 'jade/stack-builder/pages',
                    pretty: true,
                    client: false,
                    data: {
                        debug: true,
                        useLayout: myLayout, // Predefined layout name i.e vertical-light-sidebar
                        useDirection: myTextDirection
                    }
                },
                files: [{
                    cwd: 'jade/stack-builder/pages',
                    src: ['*.jade', '!**/template.jade'],
                    dest: 'dist/html/',
                    expand: true,
                    ext: ".html"
                }]
            },
        },

        /*
         * Watch files changes and compile
         */

        watch: {
            jade: {
                files: ['jade/**/*.jade', '!gmaps-maps.jade'],
                tasks: ['jade:html'],
                options: {
                    interrupt: false,
                    spawn: false,
                    // livereload: true,
                },
            },

            js: {
                files: ["js/**/*" /*, "!js/init.js"*/ ],
                tasks: ['js_compile'],
                options: {
                    interrupt: false,
                    spawn: false,
                    // livereload: true,
                },
            },

            sass: {
                files: ['stack-assets/scss/**/*.scss'],
                tasks: ['sass:expanded', 'postcss:main'],
                options: {
                    interrupt: false,
                    spawn: false,
                    // livereload: true,
                },
            }
        },


        //  Concurrent
        concurrent: {
            watch: {
                tasks: ["watch:jade", /*"watch:js",*/ "watch:sass", "notify:watching" /*, 'server'*/ ],
                options: {
                    logConcurrentOutput: true
                }
            },
            monitor: {
                tasks: ["watch:jade", "watch:js", "watch:sass", "notify:watching" /*, 'server'*/ ]
            },
        },

        /*
         * Browser Sync integration
         */
        browserSync: {
            bsFiles: ["bin/*.js", "bin/*.css", "!**/node_modules/**/*"],
            options: {
                server: {
                    baseDir: "./", // make server from root dir
                    directory: true
                },
                port: 8000,
                ui: {
                    port: 8080,
                    weinre: {
                        port: 9090
                    }
                },
                open: false
            }
        },


        //  Notifications
        notify: {
            watching: {
                options: {
                    enabled: true,
                    message: 'Watching Files!',
                    title: "Materialize", // defaults to the name in package.json, or will use project directory's name
                    success: true, // whether successful grunt executions should be notified automatically
                    duration: 1 // the duration of notification in seconds, for `notify-send only
                }
            },

            sass_compile: {
                options: {
                    enabled: true,
                    message: 'Sass Compiled!',
                    title: "Materialize",
                    success: true,
                    duration: 1
                }
            },

            js_compile: {
                options: {
                    enabled: true,
                    message: 'JS Compiled!',
                    title: "Materialize",
                    success: true,
                    duration: 1
                }
            },

            jade_compile: {
                options: {
                    enabled: true,
                    message: 'Jade Compiled!',
                    title: "Materialize",
                    success: true,
                    duration: 1
                }
            },

            server: {
                options: {
                    enabled: true,
                    message: 'Server Running!',
                    title: "Materialize",
                    success: true,
                    duration: 1
                }
            }
        },

        // Text Replace
        replace: {
            min: {
                src: ['*.html'], // source files array (supports minimatch)
                dest: 'dist/', // destination directory or file
                replacements: [{
                    from: '/bootstrap.css', // string replacement
                    to: '/bootstrap.min.css'
                }, {
                    from: '/bootstrap-stack.css',
                    to: '/stack.min.css'
                }, {
                    from: '/style.css',
                    to: '/style.min.css'
                }, {
                    from: '/stack.js',
                    to: '/stack.min.js'
                }]
            },
            version: { // Does not edit README.md
                // src: [
                //   'bower.json',
                //   'package.json',
                //   'package.js',
                //   'jade/**/*.html'
                // ],
                // overwrite: true,
                // replacements: [{
                //   from: grunt.option("oldver"),
                //   to: grunt.option("newver")
                // }]
            },
            readme: { // Changes README.md
                // src: [
                //   'README.md'
                // ],
                // overwrite: true,
                // replacements: [{
                //   from: 'Current Version : v' + grunt.option("oldver"),
                //   to: 'Current Version : v' + grunt.option("newver")
                // }]
            },
        },

        // Create Version Header for files
        usebanner: {
            release: {
                // options: {
                //   position: 'top',
                //   banner: "/*!\n * Materialize v" + grunt.option("newver") + " (http://materializecss.com)\n * Copyright 2014-2015 Materialize\n * MIT License (https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE)\n */",
                //   linebreak: true
                // },
                // files: {
                //   src: ['dist/css/*.css', 'dist/js/*.js']
                // }
            }
        },

        // Rename files
        rename: {
            rename_src: {
                src: 'bin/stack-src' + '.zip',
                dest: 'bin/stack-src-v' + grunt.option("newver") + '.zip',
                options: {
                    ignore: true
                }
            },
            rename_compiled: {
                src: 'bin/stack' + '.zip',
                dest: 'bin/stack-v' + grunt.option("newver") + '.zip',
                options: {
                    ignore: true
                }
            },
        },

        // Removes console logs
        removelogging: {
            source: {
                src: ["js/**/*.js" /*, "!js/velocity.min.js"*/ ],
                options: {
                    // see below for options. this is optional.
                }
            }
        },

        rtlcss: {
            myTask: {
                // task options
                options: {
                    // generate source maps
                    map: { inline: false },
                    // rtlcss options
                    opts: {
                        clean: false
                    },
                    // rtlcss plugins
                    plugins: [],
                    // save unmodified files
                    saveUnmodified: true,
                },
                expand: true,
                cwd: 'stack-assets/css',
                dest: 'stack-assets/css-rtl',
                src: ['*.css']
            }
        },
    });


    var JadeInheritance = require('jade-inheritance');
    var changedFiles = [];

    var onChange = grunt.util._.debounce(function() {
        var options = grunt.config('jade.html.options');
        var dependantFiles = [];

        changedFiles.forEach(function(filename) {
            var directory = options.basedir;
            var inheritance = new JadeInheritance(filename, directory, options);
            dependantFiles = dependantFiles.concat(inheritance.files);
        });

        var config = grunt.config('jade.html.files')[0];
        config.src = dependantFiles;
        grunt.config('jade.html.files', [config]);

        changedFiles = [];
    }, 200);

    grunt.event.on('watch', function(action, filepath) {
        changedFiles.push(filepath);
        onChange();
    });


    // load the tasks
    // grunt.loadNpmTasks('grunt-gitinfo');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-notify');
    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-banner');
    grunt.loadNpmTasks('grunt-rename');
    grunt.loadNpmTasks('grunt-remove-logging');
    grunt.loadNpmTasks('grunt-rtlcss');
    grunt.loadNpmTasks('grunt-browser-sync');
    //grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-postcss');
    // define the tasks

    grunt.registerTask('sass-compile', ['sass:expanded', 'sass:bin', 'postcss:main', 'postcss:bin', 'notify:sass_compile']);
    grunt.registerTask('jade-compile', ['jade', 'notify:jade_compile']);
    grunt.registerTask('js-compile', ['concat:temp', 'uglify:bin', 'notify:js_compile', 'clean:temp']);
    grunt.registerTask('server', ['browserSync', 'notify:server']);
    grunt.registerTask('lint', ['removelogging:source']);
    grunt.registerTask('monitor', ["concurrent:monitor"]);
    grunt.registerTask('set-global', 'Set a global variable.', function(name, val) {
        global[name] = val;
    });
    grunt.registerTask(
        'dist', [
            'lint',
            'copy',
            'sass:dist',
            //'sass:min',
            //'postcss:expended',
            //'postcss:min',
            'postcss:dist',
            'jade:dist',
            /*'concat:dist',
            'uglify:dist',
            'uglify:extras',*/

            //'usebanner:release',
            // 'compress:main',
            // 'compress:src',
            // 'compress:starter_template',
            // 'compress:parallax_template',

            // 'replace:min',

            //'replace:version',
            //'replace:readme',
            // 'rename:rename_src',
            // 'rename:rename_compiled'
        ]
    );
};
