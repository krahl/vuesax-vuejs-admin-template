=====================
/* Form Components */
=====================

-----------------
|  Basic Input  |
-----------------
- Basic Inputs doesn't need any classes. Simple '.form-control' is used as its a bootstrap field
- To set Input in a style use '.form-group-style' class to wrapper.
- Add class ".floating-label-form-group" to wrapper for floting label input.
- Another input label style using, '.labelUp', '.labelDown', '.labelIcon' & '.labelIconAfter' to input type text control.
- For diffrent validation use classes '.form-control-danger' or '.form-control-success' or '.form-control-warning' or '.form-control-info'
- Align using '.text-left' or '.text-right' or '.text-center' or '.text-justify'
- Input font classes '.font-weight-normal', '.font-weight-bold', '.font-weight-italic', '.text-lowercase', '.text-capitalize', '.text-uppercased', '.font-size-large', '.font-size-small', '.font-size-xsmall'
- To add icon use '.has-feedback' class to the wrapper of the input
- To set icon on left of the input, add '.has-icon-left' class to wrapper of input
- For Input sizes, Use '.input-lg' or '.input-sm' or '.input-xs' for large, small and extra small inputs
- For Input Icon sizes, Use '.icon-lg' or '.icon-sm' or '.icon-xs' for large, small and extra small input icons
- For spin icon, '.pe-spin' class for Pe-media-icons and '.fa-spin' class for Font Awesome spinner.
- Use '.square' class to input for square shaped input field
-> TOOLTIP
- To add tooltip to field, you need to add 'data-toggle="tooltip"'.
- To set position of tooltip, use 'data-placement="top"' attribute. [values = top/bottom/right/left]
- To set when to triggler a tooltip, use 'data-trigger="focus"' attribute [values = hover/focus]
- Use attribute 'data-title', for tooltip message.
- Control group sizes class : ".form-group-xl" , '.form-group-lg', '.form-group-sm', '.form-group-xs'
- input sizes class : '.input-xl', '.input-lg', '.input-sm', '.input-xs'
- For field text color, use ".COLORNAME" class like ".blue, .red, etc..."
- For field border Color, use ".border-COLORNAME" class like ".border-teal, .border-orange, etc..."
- For field background color, use ".bg-COLORNAME" class like ".bg-warning, bg-green, etc..."
-> INPUT TYPES
- input type="datetime"
- input type="date"
- input type="month"
- input type="week"
- input type="time"
- input type="number"
- input type="email"
- input type="url"
- input type="search"
- input type="tel"
- input type="color"
- input type="range"

-------------------------
|  Checkboxes & Radios  |
-------------------------
-> CHECKBOXES
- input type="checkbox" for simple checkbox
- for black styled checkbox, add a wrapper of ".styledCheckBlack" class to checkbox & add blank span after input for better result
- for custom checkbox, add ".c-input.c-checkbox" as wrapper class, and ".c-indicator" span class after checkbox input field.
- Wrap with ".icheck1" to use icheck checkbox.
- ".icheck2.skin" for skin color for check icon style of checkbox
- Wrap with ".skin.skin-square" for square and filled color checkbox. Have to do color changes using JS.
- Wrap with ".skin.skin-flat" for Flat colored checkbox. Have to do color changes using JS.
- Wrap with ".skin.skin-polaris" for polaris theme checkbox.
- Wrap with ".skin.skin-futurico" for futurico theme checkbox.
- Add .bg-COLORNAME to span to set according to theme color. [COLORNAME = primary, success, info, purple, etc..]
- Add ".inline" class to the checkbox wrapper for inline checkbox.
-> Image Checkbox
- Add ".img-thumbnail" class to <img> tag after input type checkbox.
- Add ".btn-COLORNAME" class to wrapper to use theme colors. [COLORNAME = primary, success, info, purple, etc..]
- wrap with right-checkbox for right aligned checkbox.

-> RADIOS
- input type="radio" for simple radio
- wrap with ".right-radio" class for right aligned radio.
- Wrap radio with ".styledRadioBlack" class & add blank span after input for better result
- Add ".c-input.c-radio" as a single wrapper & add <span class="c-indicator"></span> after inpur radio field for better output.
- Add ".bg-COLORNAME" class to span to set according to theme color. [COLORNAME = primary, success, info, purple, etc..]
- Wrap with ".icheck1" class to use default icheck radio button.
- Wrap with ".icheck2" class to use icheck radio style with diffrent colors.
- Wrap with ".skin.skin-square" for square and filled color radio. Have to do color changes using JS.
- Wrap with ".skin.skin-flat" for Flat colored radio. Have to do color changes using JS.
- Wrap with ".skin.skin-polaris" for polaris theme radio.
- Wrap with ".skin.skin-futurico" for futurico theme radio.
- Add ".inline" class to the radio wrapper.

-> Bootstrap Checkbox Toggle
- Add ".switch" class to checkbox to set as switch
- Add "data-reverse" attribute to reverse toggle
- Add "data-switch-always" attribute to switch on every click
- Add 'data-group-cls="btn-group-sm"' attribute for small switch
- Add 'data-group-cls="btn-group-lg"' attribute for large switch
- Add 'data-group-cls="btn-group-vertical" ' attribute for verticle switch
- Add 'data-off-title="VALUE"' attribute for OFF hover title and 'data-on-title="VALUE" ' attribute for ON hover title
- Add 'data-icon-cls="fa"' to set font family you are using 'data-off-icon-cls="fa FONT_AWESOME_ICON"' attribute for off switch icon & 'data-on-icon-cls="fa FONT_AWESOME_ICON"' attribute for on switch icon
- To set only icon switch set 'data-off-label="false"' & 'data-off-label="false"'
- Add 'data-off-label="OffValue"' attribute for off switch label & 'data-on-label="OnValue"' attribute for on switch label

-> Bootstrap Switch Toggle
- Add '.switchBootstrap' class to set bootstrap switch
- Add 'data-inverse="true"' attribute for Inverse switch direction.
- Add 'data-on-text="TEXT"' to change text of the left side of the switch
- Add 'data-off-text="TEXT"' to change text of the right side of the switch
- Use 'data-label-text="TEXT"' for text of the center handle of the switch and that will work with 'data-indeterminate="true"' attribute only.
- Add 'data-on-color="COLORNAME"' attribute to field for color options.[COLORNAME = primary, success, info, purple, etc..]

-> SWITCHERY
- To set Switchery toggle, add '.switchery' class to checkbox.
- To set Switchery toggle to right, wrap checkbox with '.pull-right' class.
- To set Large Switchery toggle, add '.switchery-lg' class to checkbox.
- To set Small Switchery toggle, add '.switchery-sm' class to checkbox.
- To set XSmall Switchery toggle, add '.switchery-xs' class to checkbox.
- To get Color Switchery, add '.switchery-COLORNAME' class to checkbox. [COLORNAME = primary, success, info, purple, etc..]

---------------------
|	Dual Listbox	|
---------------------
- Add '.duallistbox' class to select for basic dual listbox.
- Add '.duallistbox-no-filter' class without filter dual listbox.
- Add '.duallistbox-multi-selection' class for Dual ListBox with Multiple Selection
- Add '.duallistbox-with-filter' class for dual listbox with filter.
- All the listbox labels and placeholders are fully editable through specified options, Like: 'filterTextClear, filterPlaceHolder, infoText, infoTextFiltered, infoTextEmpty'. You can use custom language also.
- Use 'selectorMinimalHeight' setting option to set dual listbox custom height.
- Add an option to the Listbox Using Add Options button. Add an option and also clearing highlights of listbox option using Add with clearing highlight button

-----------------
|	Input Grid	|
-----------------
- Check http://v4-alpha.getbootstrap.com/layout/grid/

---------------------
|	Input Groups	|
---------------------
- Add span with '.input-group-addon' class to before / after <input> field
- Add span with '.input-group-addon' class before / After <input> field and Add checkbox/ radio/ switchery inside span.
-> Sizing
- Add '.input-group-lg' class to '.input-group' for Large addon.
- Add '.input-group-sm' class to '.input-group' for Small addon.
- Add '.input-group-xs' class to '.input-group' for Extra Small addon.
-> Input Group Button
- Add span with '.input-group-btn' class and add button inside span, use <input> field before / after
- Add color using '.bg-COLORNAME' class to the addon.[COLORNAME = primary, success, info, purple, etc..]
- Add color using '.bg-COLORNAME' class to the addon and input with 'lighten/darken' color.
-> Validation
- Wrap using '.has-danger' class for danger validation. [Use success/info/warning in place of danger]
- Wrap using '.has-danger' and wrap sub element with '.form-control-danger' to display with icon. [Use success/info/warning in place of danger]

-> Bootstrap TouchSpin Spinners
- Add '.touchspin' class to input to add touchspin input group.
- Add 'data-bts-postfix="POSTFIX_VALUE"' & 'data-bts-prefix="PREFIX_VALUE"' attribute to input to add postfix & prefix to touchspin input group.
- Use 'data-bts-min="VALUE"' 'data-bts-max="VALUE"' attribute to input to set min and max value of touchspin input group.
- Add 'data-bts-init-val="VALUE"' attribute to set initial value for input group.
- Add 'data-bts-step="VALUE"' attribute for increament and decrement steps to touchspin input group.
- Use 'data-bts-decimal="VALUE"' attribute to use decimal value of touchspin input.
- Add '.touchspin-verticle' class for verticle touchspin input group.
- Add '.touchspin-stop-mousewheel' class to diable mousewheel.
- Add 'data-bts-button-down-class' & 'data-bts-button-up-class' attribute to change button Class.
- Add icon class in 'data-bts-postfix' attribute to icon to postfix as well prefix.
- Use 'data-bts-button-down-txt' & 'data-bts-button-up-txt' attribute to set touchspin icon button.
- Use 'data-bts-prefix' & 'data-bts-postfix' attribute to set Prefix and Postfix to touchspin input with button.
-> Spinners Sizes
- Add '.input-group-lg' class to input-group and add '.input-lg' class to input.
- Add '.input-group-sm' class to input-group and add '.input-sm' class to input.
- set 'data-bts-button-down-class' & 'data-bts-button-up-class' attribute and add value as btn btn-primary for Primary color spinner. [Use success/info/warning in place of danger]


---------------------
|	Select Box It	|
---------------------
- Add '.selectBox' class to select field for basic SelectBoxIt
- Add '.selectBox-hide-first' class to hide first field of select
- Use '.selectBox-label-default' class to add label to Select field
- Add 'data-size' attribute to select to show number of options at a time
- Add 'data-selectedtext' attribute to select option for custom text value
- Add 'data-preventclose' attribute to prevent closing
- Add '.selectBox-effect' class to select for opening & Closing effects
- Aggressive Change Mode will select a drop down option when user navigates option using up and down arrow keys via the keyboard
- Add '.selectBox-animation' class to select for basic SelectBoxIt field
- Click and hold down your mouse, hover over a drop down option, and then release the mouse to select that option.

-> Advance Options Manipulations [Can do changes from javascript file according to your choice]
- Use '.options-dynamic' class with .selectBox class to add dynamic option
- Use '.option-remove' class with .selectBox class to remove option dynamically
- Use '.options-remove' class with .selectBox class to remove multiple options
- You can remove all options on click of button also.
- Use 'hideCurrent: true' in javascript to Hide Current selected option from dropdown
- Use '.selectBox-right-icon' class to change down arrow icon.
- Use 'data-iconurl' attribute and Image URL as value to show Image.
- Use '.selectBox.bootstrap-icon' class to change down arrow icon as bootstrap icon.
- Use '.custom-bootstrap-icon' class to change down arrow icon as bootstrap icon.
- Use 'rel="popover"' attribute so it will popover 'title' & 'data-contenttext' on hover to options
- You can change select box width using 'width' option by javascript
- Use .selectBox-auto class for Auto width of selectBox

-> Sizes
- Use '.selectBox-xl' class for Extra Large selectBox
- Use '.selectBox-lg' class for Large selectBox
- Use '.selectBox-sm' class for Small selectBox

-> Border Color Options
- Use '.border-COLORNAME' class for primary color selectBox [COLORNAME = primary, success, info, purple, etc..]

-> SelectBox Color Options
- Use '.bg-COLORNAME' class for primary color selectBox [COLORNAME = primary, success, info, purple, etc..]


---------------------
|	Selectivity 	|
---------------------
- Add '.single-input' class to span/div for basic Selectivity
- Add '.single-select-box' class to <select> input
- Add '.without-search-box' class for without a search field
- Add '.single-input-with-labels' class to set select with labels
- Add '.single-input-with-submenus' class to get input with submenu
- Add '.multiple-input' class to select multiple options
- Add 'multiple' attribute to select more than one options
- Add '.tags-input' class to set & select tags
- Add '.emails-input' class to set multiple email addresses
- Add '.repository-input' class to select a GitHub repository
- Add '.bg-COLORNAME' '.border-COLORNAME' classes to get background and border color for selectivity select box[COLORNAME = primary, success, info, purple, etc..]
- Add '.border-THEME_COLORNAME' class to get only border color for selectivity select box

-> Sizings
- Add '.select-xl' class to span/div for Extra Large Selectivity field
- Add '.select-sm' class to span/div for Small Selectivity field
- Add '.select-xs' class to span/div for Extra Small Selectivity field


---------------------
|	Selectize		|
---------------------
- Add '.input-selectize' class to input field and add value comma saperated.
- Add '.selectize-multiple' class multiple selectize field
- Add '.selectize-select' class to select for basic Selectize select field
- Add '.selectize-sort' class to sort select options
- Add '.repositories' class to Integrate github repository
- Add '.selectize-locked' class to lock single select
- Add '.selectize-junk' class to lock single select
- Add '.remove-tags' class to get select with remove button
- Add '.backup-restore' class to restore on backspace
- you can add options using API call
- Add '.selectise-drap-drop' class to sort options by drag and drop.
- Add '.selectize-rtl-input' class supports RTL to input
- Add '.selectize-rtl-select' class supports RTL to select

---------------------
|	Validation 		|
---------------------
- Add 'novalidate' attribute to form tag.
- Add 'required' attribute to field for required validation.
- Add 'data-validation-required-message' attribute for Custom required failure message
- Add 'data-validation-match-match' attribute with the field name as value to match with it.
- Add 'maxlength' attribute for maximum number of characters to accept. you can use 'data-validation-maxlength-message' attribute for maxlength failure message
- Add 'minlength' attribute for minimum number of characters to accept. You Can use 'data-validation-minlength-message' attribute for minlength failure message
- Add 'max' attribute for maximum number to accept. Also use 'data-validation-max-message' attribute for max failure message
- Add 'min' attribute for minimum number to accept. Also use 'data-validation-min-message' attribute for min failure message
- Use 'data-validation-containsnumber-regex="(\d)+"' attribute to enter only numbers
- Add 'pattern' attribute to set input pattern. Also use 'data-validation-pattern-message' attribute for pattern failure message
- Add 'data-validation-regex-regex' attribute for regular expression. Also use 'data-validation-regex-message' attribute for regex failure message
- Use 'data-validation-maxchecked-maxchecked' attribute for maximum number of checkbox checked. Also use 'data-validation-maxchecked-message' attribute for maxchecked failure message
- Use 'data-validation-minchecked-minchecked' attribute for minimum number of checkbox checked. Also use 'data-validation-minchecked-message' attribute for minchecked failure message


================
/* Components */
================

-----------
| Buttons |
-----------
1. Use 'btn btn-*' classes for buttons [* = primary, secondary, success, danger, warning, info]
2. Use 'btn-round' for round button for any type of button
3. Dropdown Button
- Use ".dropdown-toggle" class.
- Add attribute 'data-toggle="dropdown"' to button.
- For Dropdown list ".dropdown-menu" is wrapper class.
- ".dropdown-items" class for each items of dropdown
4. Split button dropdowns
- Need to add 2 button tags here
- 1 is for text/icon title
- same as dropdown button
5. Button group
- use ".btn-group" class for more than one button to make a group
6. You can add icon to any button by adding icon classes with text of the button. Don't add text if you want only icon button or dropdown.
7. sizes
- use '.btn-lg' or '.btn-sm' for large and small buttons
- To button group add '.btn-group-lg' or '.btn-group-sm'.
8. Block Button
- Use ".btn-block" class to display button block
9. Button Tags
- You can use button classes with <a></a>, <button></button>, <input type="button"> or <input type="submit"> any of these.
10. Outline buttons
- Need to add ".btn-*-outline" class for all the buttons, dropdowns or button groups. [* = primary, secondary, success, danger, warning, info]
11. Use ".btn-round" for round  buttons, dropdowns or button groups
12. Vertical variation
- Add class ".btn-group-vertical" to wrap all the buttons to make a group. You can also add dropdowns.



--------------------
| Extended Buttons |
--------------------
1. Floating button
- Use ".btn-float" class for floating buttons
- Use ".btn-float .btn-float-lg" classes for large floating buttons
- Use ".btn-float .btn-round" classes for round floating buttons
2. Outline Floating button
- Use ".btn-float .btn-*-outline" class for outline floating buttons
- Use ".btn-float .btn-*-outline .btn-float-lg" classes for large outline floating buttons
- Use ".btn-float .btn-*-outline .btn-round" classes for round outline floating buttons
3. Social Media Buttons
- Use ".btn-social .btn-*[social name ex. .btn-facebook]" for Social Button & add social icon and text as required
- Use ".btn-social .btn-*[social name ex. .btn-facebook] .btn-block" for Block Social Button
- Use ".btn-lg" or "btn-sm" or "btn-xs" for diffrent sizes
4. Outline Social Media Buttons
- Use ".btn-social .btn-*-outline[social name ex. .btn-facebook-outline]" for Social outline Button & add social icon and text as required
- Use ".btn-social .btn-*-outline[social name ex. .btn-facebook-outline] .btn-block" for Block Outline Social Button
5. Ladda Buttons
- Use ".ladda-button" class for all the animation buttons. 
- For expand animation '.data-style="expand-*"' is uesd [* = left/right/up/down]
- For slide animation '.data-style="slide-*"' is uesd [* = left/right/up/down]
- For zoom animation '.data-style="zoom-*"' is uesd [* = left/right/up/down]
- For sizing, use attribute 'data-size="l"'. values can be [l=large/s=small]
- Add wrapper class ".progress-demo" for all the progress buttons.

---------------------
| 	Progress Bar 	|
---------------------
To change bar color( to use custom color progress bar), Add "progress-variant(COLOR)" mixin in progress.scss file in variant section.