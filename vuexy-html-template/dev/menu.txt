MENU:
-----
Your license key: 1-jqmm-3021-20180110-734b-032e19
GENERAL

- Dashboards
==============
--Dashboard 1
--Dashboard 2

- Layouts (As single page)
============================
--Vertical Basic Menu
--Vertical Multi Level Menu
--Vertical Overlay Menu
--Vertical Compact Menu
--Vertical Content Menu
--------------------------
--Horizontal Basic Menu
--Horizontal TopIcon Menu


LAYOUT

-Page Layouts
==============
-1 Column
-2 Column
-3 Column left/  (Not for content) 3-col-left-sidebar
-3 Column detached left/right/stickey 3-col-detached-left-sidebar
--------------
-Fixed Navbar
-Fixed Navigation
-Fixed Footer
-Fixed Navbar & Navigation
-Fixed Navbar & Footer
-Hideable navbar
-Hideable & fixed navigation
----------------
-Static Layout
-Boxed Layout
--Default sidebar
--Collapsed sidebar
--Full width
-Center Layout
----------------
-Light Layout
-Dark Layout
-Semi Dark Layout

-Starters
=========
-1 Column
-2 Column
-3 Column left/  (Not for content) 3-col-left-sidebar
-3 Column detached left/right/stickey 3-col-detached-left-sidebar
-Hideable navbar (R)
-Hideable & fixed navigation (R)
----------------
-Fixed Layout
-Fixed Navbar
-Fixed Navigation
-Fixed Navbar & Navigation
-Fixed Navbar & Footer
----------------
-Static Layout
-Boxed Layout
--Default sidebar (R)
--Collapsed sidebar (R)
--Full width (R)
-Content Center Layout
----------------
-Light Layout
-Dark Layout
-Semi Dark Layout


LAYOUT
-Vertical Navigation
====================
-Menu Types
--Verticle Menu
--Verticle MMenu
--Verticle Overlay
--Verticle Compact
--Verticle Content

-Navigation Header
-Navigation Footer

-Collapsible menu
-Accordion menu

-Dark menu (.menu-dark)
-Light menu (.menu-light)

-Flipped menu (.menu-flipped)
-Fixed menu (.menu-fixed)

-Right side icons (.menu-icon-right)
-Labels and badges

-Native scroll (.menu-native-scroll)
-Menu styling (.menu-border, .menu-shadow)
-Disabled navigation (.disabled)


-Horizontal Navigation
======================
-Navigation Types
--left-icon menu
--top-icon menu
-megaMenu
-submenu on click
-submenu on hover
-Navigation-light
-Navigation-dark
-Navigation-shadow /menu-border
-Navigation-without-dd-arrow
-disabled


-Sidebars
=========

-Navbars
========
-navbar-light
-navbar-dark
-navbar-semi-dark
-navbar-brand-center
-navbar-fixed-top
-navbar-border /navbar-shadow
-hide on scroll
-Navbar components
-disabled


-Footer
=======
-footer-light
-footer-dark
-footer-transperant-
-footer-fixed
-footer components
-hide on scroll (if possible)


CONTENT
USER INTERFACE
COMPONENTS
FORMS
TABLES
CHARTS
APPS
PAGES