## Product Name

Vuexy HTML Admin Template

## Product Description

Vuexy Admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities. It includes 7 pre-built templates with organized folder structure, clean & commented code, 2500+ pages, 1000+ components, 500+ charts, 100+ advance cards (widgets), 100+ plugins – extensions and many more. Vuexy Admin provides internationalization, RTL support, searchable navigation, unique menu layouts, advance cards and incredible support. Vuexy Admin can be used for any type of web applications: Project Management, eCommerce backend, CRM, Analytics, Fitness or any custom admin panels. It comes with 5 niche dashboards. Vuexy Admin template is powered with HTML 5, SASS, GRUNT, JADE [Pug] & Twitter Bootstrap 4 which looks great on Desktops, Tablets, and Mobile Devices. Vuexy Admin comes with starter kit which will help developers to get started quickly.

## Online Documentation

You will find documentation in your downloaded zip file from ThemeForest. You can access documentation online as well.
Documentation URL: https://pixinvent.com/demo/vuexy-html-bootstrap-admin-template/documentation

## 7 Pre-Built Admin Templates

1. Default Template: Classic admin template with dark sidebar, fixed semi dark navbar, light footer and searchable navigation
2. Compact Menu: Modern admin template with compact light sidebar, dark navbar with center branding and dark footer
3. Content Menu: Minimal admin template with light sidebar, light navbar which auto hides, light footer and searchable navigation
4. Overlay Menu: Full width admin template with dark sidebar, light navbar with center branding, transparent footer and searchable navigation
   5.Multi-level Menu: Unique admin template with dark multi-level menu sidebar, built-in search, fixed navbar and dark footer
5. Horizontal Menu: Centered layout template with light navigation menu, light navbar with center branding and light footer
6. Horizontal Top Icon Menu: Full width admin template with light navigation menu, dark navbar, dropdowns working on hover and dark footer

## Prerequisites [To run gulp / gulp]

Node JS must be installed on your system to run gulp / gulp template generation and dist commands.
You can download and install nodejs from this URL: https://nodejs.org/en/

## Install Required Node Modules

    1. Gulp
    --------
    - IF you are using gulp copy and paste package.gulp.json file in the root folder and rename it to package.json.
    - Run npm install command to install required node modules

## gulp Commands

    1. Build Template
    -----------------

    	Arguments
    	-----------
    	--Layout - which layout to use from the available 7 pre-built layouts to build your layout
    	--LayoutName - Folder name that your html files will be built into.
    	--TextDirection - LTR / RTL [Which text direction to use]

    	- Vertical Menu Template

    		Compile Command [HTML Generation command]
    		-----------------------------------------
    		gulp dist-html --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="LTR"

    		Monitor Command [Watch for any changes in jade file]
    		----------------------------------------------------
    		gulp monitor --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="LTR"

    	- Horizontal Menu Template

    		Compile Command [HTML Generation command]
    		-----------------------------------------
    		gulp dist-html --Layout="horizontal-menu-template" --LayoutName="horizontal-menu-template" --TextDirection="LTR"

    		Monitor Command [Watch for any changes in jade file]
    		----------------------------------------------------
    		gulp monitor --Layout="horizontal-menu-template" --LayoutName="horizontal-menu-template" --TextDirection="LTR"

    	- Vertical Menu Template Dark

    		Compile Command [HTML Generation command]
    		-----------------------------------------
    		gulp dist-html --Layout="vertical-menu-template-dark" --LayoutName="vertical-menu-template-dark" --TextDirection="LTR"

    		Monitor Command [Watch for any changes in jade file]
    		----------------------------------------------------
    		gulp monitor --Layout="vertical-menu-template-dark" --LayoutName="vertical-menu-template-dark" --TextDirection="LTR"

    	- Vertical Menu Template Semi Dark

    		Compile Command [HTML Generation command]
    		-----------------------------------------
    		gulp dist-html --Layout="vertical-menu-template-semi-dark" --LayoutName="vertical-menu-template-semi-dark" --TextDirection="LTR"

    		Monitor Command [Watch for any changes in jade file]
    		----------------------------------------------------
    		gulp monitor --Layout="vertical-menu-template-semi-dark" --LayoutName="vertical-menu-template-semi-dark" --TextDirection="LTR"

    	- Vertical Collapsed Menu Template

    		Compile Command [HTML Generation command]
    		-----------------------------------------
    		gulp dist-html --Layout="vertical-collapsed-menu-template" --LayoutName="vertical-collapsed-menu-template" --TextDirection="LTR"

    		Monitor Command [Watch for any changes in jade file]
    		----------------------------------------------------
    		gulp monitor --Layout="vertical-collapsed-menu-template" --LayoutName="vertical-collapsed-menu-template" --TextDirection="LTR"


    2. Starter Kit
    --------------

    	- Vertical Menu Template

    		Compile Command [HTML Generation command]
    		-----------------------------------------
    		gulp dist-sk-html --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="LTR"

    	- Vertical Menu Template Dark

    		Compile Command [HTML Generation command]
    		-----------------------------------------
    		gulp dist-sk-html --Layout="vertical-menu-template-dark" --LayoutName="vertical-menu-template-dark" --TextDirection="LTR"

    	- Vertical Menu Template Semi Dark

    		Compile Command [HTML Generation command]
    		-----------------------------------------
    		gulp dist-sk-html --Layout="vertical-menu-template-semi-dark" --LayoutName="vertical-menu-template-semi-dark" --TextDirection="LTR"

    	- Vertical Collapsed Menu Template

    		Compile Command [HTML Generation command]
    		-----------------------------------------
    		gulp dist-sk-html --Layout="vertical-collapsed-menu-template" --LayoutName="vertical-collapsed-menu-template" --TextDirection="LTR"


    3. Dist
    -------
    	- gulp dist [To clean css, js and build distributable css and js files]
    	- gulp dist-clean [To clean css, js files]
    	- gulp dist-js [To clean js files and build distributable js files]
    	- gulp sass-compile [Compile scss files]
    	- gulp dist-css [To clean css files and build distributable css files]

    4. Documentation
    ------------------
    gulp dist-documentation --Layout="documentation" --LayoutName="documentation" --TextDirection="LTR"

    1. HTML beautify (For both)
    -------------------------
    gulp beautify-html

    1. Replace (For dist only)
    -------------------------
    gulp replacement

    1. Copy to laravel
    -------------------------
    gulp copy-to-laravel

## Change Log

Read CHANGELOG.md file

## License

Read LICENSE.md file
