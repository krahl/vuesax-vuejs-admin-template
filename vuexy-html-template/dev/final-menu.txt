
-Dashboards
-Templates

-Layouts
-- Page Layouts
-- Navbars
-- Vertical Navigation
-- Horizontal Navigation
-- Footers

-General
-- Color Pallet
-- Starter Kit
-- Changelog
-- Disabled Menu
-- Menu Levels

- PAGES
-- Project
-- Scrumboard
-- Invoice
-- Timelines
-- Users
-- Gallery
-- Search
-- Authentication
-- Error
-- Others
--- Coming Soon
--- Maintenance

-User Interface
-- Cards
-- Content
--- Grid
--- Typography
--- Text styling
--- Text utilities
--- Syntax highlighter
--- Helper classes

-- Components
--- Alerts
--- Callout
--- Breadcrumb
--- Buttons
---- Basic Buttons
---- Extended Buttons
--- Carousel
--- Collapse
--- Dropdowns
--- List Group
--- Modals
--- Pagination
--- Navs Component
--- Tabs Component
--- Pills Component
--- Tooltips
--- Popovers
--- Tags
--- Badges
--- Progress
--- Media Objects
--- Scrollable
--- Loaders

-- Extra Components
--- Sweet Alerts
--- Tree Views
--- Toastr
--- Ratings
--- Context Menu
--- NoUI Slider
--- Date Time Dropper
--- Lists
--- Toolbar
--- Unslider
--- Knob
--- Long Press
--- Offline
--- Zoom

-- Icons
--- Feather
--- Ionicons
--- FPS Line Icons
--- Ico Moon
--- Font Awesome
--- Meteocons
--- Evil Icons
--- Linecons

- FORMS
----------------
-- Form Elements
--- Form Inputs
--- Input Groups
--- Input Grid
--- Extended Inputs
--- Checkboxes & Radios
--- Switch
--- Select
---- Select2
---- Selectize
---- Selectivity
---- Select Box It
--- Dual Listbox
--- Tags Input
--- Validation

-- Form Layouts
--- Basic Forms
--- Horizontal Forms
--- Hidden Labels
--- Form Actions
--- Row Separator
--- Bordered
--- Striped Rows
--- Striped Labels

-- Form Wizard
--- Circle Style
--- Notification Style

-- Form Repeater

- TABLES
--------------------
-- Bootstrap Tables
--- Basic Tables
--- Table Border
--- Table Sizing
--- Table Styling
--- Table Components

-- DataTables
--- Basic Initialization (Spell check in page)
--- Advanced initialization (Spell check in page)
--- Styling
--- Data Sources
--- API

-- DataTables Extensions
--- AutoFill
--- Buttons
---- Basic Buttons
---- HTML 5 Data Export
---- Flash Data Export
---- Column Visibility
---- Print
---- API
--- Column Reorder
--- Fixed Columns
--- Key Table
--- Row Reorder
--- Select
--- Fix Header
--- Responsive
--- Column Visibility

-- Handson Table
--- Handson Table
--- Appearance
--- Rows Columns
--- Rows Only
--- Columns Only
--- Data Operations
--- Cell Features
--- Cell Types
--- Integrations
--- Utilities

-- jsGrid

- Add Ons
---------------
-- Editors
--- Quill
--- CKEditor
--- Summernote
--- TinyMCE
--- Code Editor
---- CodeMirror
---- Ace

-- Pickers
--- Date & Time Picker
--- Color Picker

-- jQuery UI
--- Interactions
--- Navigations
--- Date Pickers
--- Autocomplete
--- Buttons & Select
--- Slider & Spinner
--- Dialog & Tooltip

-- Block UI
-- Image Cropper
-- Drag & Drop
-- File Uploader
--- Dropzone
--- jQuery File Upload

-- Event Calendars
--- Full calendar
---- Basic
---- Events
---- Extra

--- CLNDR
--- Internationalization

- Charts & Maps
------------------
-- Google Charts
-- Echarts
-- Chartjs
-- D3 Charts
-- C3 Charts
-- Chartist
-- Dimple Charts
-- Morris Charts
-- Flot Charts
-- Rickshaw Charts
-- Maps

- Support
-----------------
-- Changelog
-- Raise Support
-- Documentation
