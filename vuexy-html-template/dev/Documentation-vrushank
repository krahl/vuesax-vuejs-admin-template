/********************************************************
*					Card Structure						*
********************************************************/
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Special title treatment</h4>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-body collapse in" aria-expanded="true">
        <div class="card-block">
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
    </div>
    <div class="card-footer">
        <span class="text-muted">ABCD</span>
    </div>
</div>

It's mandatory to add class card-title to your heading element. Add mb-1 class to card-title element if you require subheading.

Q : How to remove card border
A: Add class no-border to card

Q : How to remove card shadow
A : Add class box-shadow-0 to card

Q : How to change card-header border color
A : Add class border-bottom-primary & border-bottom-lighten-3 to card-header

-----------------------------------------------------------------------------------

/********************************************************
*					JADE Structure						*
********************************************************/

block pageVars
	- var pageTitle    = "2 Columns"
	- var pageSubTitle = "2 columns layout with navigation"
	- var description  = "The full width 2 columns layout with content & navigation section."
	- var activeMenu   = "layout-2-columns"

extends template

append breadcrumbs
	+breadcrumbs([{url:"index.html",name:"Home"},{url:"#",name:"Page Layouts"}, {name:"2 Column"}])

append page-header
	include page-headers/breadcrumbs-top-with-description

//- Include page content in page block
append content
	include ../contents/layout-2-columns.html

//- Vendor CSS
//------------------------------
//- Add Vendor specific CSS
append vendorcss
	link(rel='stylesheet', type='text/css', href='../../../stack-assets/css/plugins/ui/prism.min.css')

//- Page CSS
//------------------------------

//- Add custom page specific CSS
append pagecss
	link(rel='stylesheet', type='text/css', href='../../../stack-assets/css/plugins/ui/prism.min.css')

//- Vendor JS
//------------------------------
//- Add vendor specific JS
append vendorjs
	script(src='../../../stack-assets/js/plugins/ui/prism.min.js', type='text/javascript')

//- Page JS
//------------------------------
//- Add custom page specific JS
append pagejs
	script(src='../../../stack-assets/js/plugins/ui/prism.min.js', type='text/javascript')

-----------------------------------------------------------------------------------

- Remove class "custom-nav-scroll" to have the browser default scroll [jade/ltr/contents/menu-content/vertical-dark or vetical - light]

Vertical menu jade
------------------
1. Define id, class, data attributes of the vertical menu
#main-menu-container.sidebar-menu-content(data-mcs-theme='minimal-dark')

2. Include mixin file
relative path to your mixin jade file
include ../../includes/mixins

3. Call vertical menu mixin to pass our json menu object and create menu html
+verticalMenu([])

4. How to create Navigation header?
{navheader:"Main",icon:"pe-is-i-ellipsis"},

5. How to create menu item?
{url:"index.html",name:"Dashboard",slug:"dashboard",icon:"pe-mi-home"},

6. How to create sub menu?

	- Stack theme support 4 level submenu as shown in the below example

	//- Menu Levels Start
	{url:"#",name:"Menu levels",icon:"pe-mi-windows",slug:"",
	  //- Menu Levels Submenu
	  submenu:[
	    {url:"#",name:"Second level",slug:""},
	    {url:"#",name:"Second level with child",slug:"",
	      submenu:[
	        {url:"#",name:"Third level",slug:""},
	        {url:"#",name:"Third level with child",slug:"",
	          submenu:[
	            {url:"#",name:"Fourth level",slug:"",},
	            {url:"#",name:"Fourth level",slug:"",},
	          ]
	        },
	      ]
	    },
	  ]
	},
	//- Menu Levels End

7. Available options while creating your personalized menu in jade
	- url : Link your menu item to actual page
	- name : Name of the menu item
	- icon : Suitable icon for your menu item i.e. (icon:"pe-mi-home"). This option does not work for submenu items.
	- slug : slug of your menu item all in lowercase without any spaces containing hyphens(-) i.e. (slug="google-bar-chart"). slug must match the activeMenu variable specified in the menu item page for active class. We have defined - var activeMenu = "dashboard" in the dashboard.jade and slug as "dashboard" when creating json menu object for dashboard.
	- classlist : pass as many classes you want to your menu item using spaces i.e. ("disabled xyz")
	- label : label to show on the right end of menu item
	- menuSubtitle : subtitle alongside menu item i.e. (menuSubtitle:"Coming soon")



jQuery File Upload
-------------------
jquery.fileupload-ui.js changed "aria-valuenow" attribute to "value" to enable compatability with bootstrap 4.



Loaders
-----------------
.loader-wrapper must have height to center loader vertically.


Scroll to active menu in siderbar
------------------------------------
if data-scroll-to-active parameter is set to true then scroll the menu to currently active menu in the sidebar on page load.



Normal Layout
--------------
grunt jade-compile --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="LTR"
gulp monitor --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="LTR"
grunt monitor --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="LTR"

grunt jade-compile --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="RTL"
grunt monitor --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="RTL"

grunt sk-jade-compile --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="LTR"
grunt sk-jade-compile --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="RTL"

grunt dist --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="LTR"

Overlay Menu Layout
-------------------
grunt jade-compile --Layout="vertical-overlay-menu-template" --LayoutName="vertical-overlay-menu-template" --TextDirection="LTR"
grunt monitor --Layout="vertical-overlay-menu-template" --LayoutName="vertical-overlay-menu-template" --TextDirection="LTR"

grunt jade-compile --Layout="vertical-overlay-menu-template" --LayoutName="vertical-overlay-menu-template" --TextDirection="RTL"
grunt monitor --Layout="vertical-overlay-menu-template" --LayoutName="vertical-overlay-menu-template" --TextDirection="RTL"

grunt sk-jade-compile --Layout="vertical-overlay-menu-template" --LayoutName="vertical-overlay-menu-template" --TextDirection="LTR"
grunt sk-jade-compile --Layout="vertical-overlay-menu-template" --LayoutName="vertical-overlay-menu-template" --TextDirection="RTL"

grunt dist --Layout="vertical-overlay-menu-template" --LayoutName="vertical-overlay-menu-template" --TextDirection="LTR"


Content Menu Layout
-------------------
grunt jade-compile --Layout="vertical-content-menu-template" --LayoutName="vertical-content-menu-template" --TextDirection="LTR"
grunt monitor --Layout="vertical-content-menu-template" --LayoutName="vertical-content-menu-template" --TextDirection="LTR"

grunt jade-compile --Layout="vertical-content-menu-template" --LayoutName="vertical-content-menu-template" --TextDirection="RTL"
grunt monitor --Layout="vertical-content-menu-template" --LayoutName="vertical-content-menu-template" --TextDirection="RTL"

grunt sk-jade-compile --Layout="vertical-content-menu-template" --LayoutName="vertical-content-menu-template" --TextDirection="LTR"
grunt sk-jade-compile --Layout="vertical-content-menu-template" --LayoutName="vertical-content-menu-template" --TextDirection="RTL"


Horizontal Menu Template
-------------------------------
grunt jade-compile --Layout="horizontal-menu-template" --LayoutName="horizontal-menu-template" --TextDirection="LTR"
grunt monitor --Layout="horizontal-menu-template" --LayoutName="horizontal-menu-template" --TextDirection="LTR"

grunt jade-compile --Layout="horizontal-menu-template" --LayoutName="horizontal-menu-template" --TextDirection="RTL"
grunt monitor --Layout="horizontal-menu-template" --LayoutName="horizontal-menu-template" --TextDirection="RTL"

grunt sk-jade-compile --Layout="horizontal-menu-template" --LayoutName="horizontal-menu-template" --TextDirection="LTR"
grunt sk-jade-compile --Layout="horizontal-menu-template" --LayoutName="horizontal-menu-template" --TextDirection="RTL"

Documentation
---------------
grunt jade:documentation --Layout="vertical-menu-documentation-template" --LayoutName="documentation" --TextDirection="LTR"

grunt watch:documentation --Layout="vertical-menu-documentation-template" --LayoutName="documentation" --TextDirection="LTR"

// Generate PHP documentation
grunt jade:documentation --Layout="vertical-menu-documentation-template-php" --LayoutName="documentation-php" --TextDirection="LTR"
grunt copy:documentation --Layout="vertical-menu-documentation-template-php" --LayoutName="documentation-php" --TextDirection="LTR"
grunt replace:documentation --Layout="vertical-menu-documentation-template-php" --LayoutName="documentation-php" --TextDirection="LTR"
grunt rename:documentation --Layout="vertical-menu-documentation-template-php" --LayoutName="documentation-php" --TextDirection="LTR"





gulp jade-compile --useLayout="vertical-menu-template" --generateTemplate="vertical-menu-template" --textDirection="LTR"

RTL CSS
--------
grunt sass-rtl-compile --Layout="vertical-menu-template" --LayoutName="vertical-menu-template" --TextDirection="RTL"
