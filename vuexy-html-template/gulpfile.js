// Require the modules.
var gulp = require('gulp');
var gutil = require('gulp-util');

var minimist = require('minimist');
var config = require('./config.json');

var options = minimist(process.argv.slice(2));

// Global Variables
global.myLayout = options.Layout;
global.myLayoutName = options.LayoutName;
global.config = config;
global.pugSrc = ['*.pug', '!**/template.pug'];
global.dashboardRename = '';
global.style = '';
global.rtl = '';
global.dist = false;

if (options.Style !== undefined) {
  global.style = '-' + options.Style;
}

if (options.dist !== undefined) {
  dist = options.dist;
}

if (options.TextDirection !== undefined) {
  global.myTextDirection = options.TextDirection.toLowerCase();
  if (myTextDirection == 'rtl') rtl = '-rtl';
} else {
  global.myTextDirection = '';
}

gutil.log(gutil.colors.green('Starting Gulp!!'));

// Exclude template specific files
if (myLayout == 'vertical-menu-template') {
  dashboardRename = config.vertical_menu_template.dashboardRename;
  pugSrc = config.vertical_menu_template.pugSrc;
} else if (myLayout == 'vertical-modern-menu-template') {
  dashboardRename = config.vertical_modern_menu_template.dashboardRename;
  pugSrc = config.vertical_modern_menu_template.pugSrc;
} else if (myLayout == 'vertical-menu-template-dark') {
  dashboardRename = config.vertical_menu_template_dark.dashboardRename;
  pugSrc = config.vertical_menu_template_dark.pugSrc;
} else if (myLayout == 'vertical-menu-template-semi-dark') {
  dashboardRename = config.vertical_menu_template_semi_dark.dashboardRename;
  pugSrc = config.vertical_menu_template_semi_dark.pugSrc;
} else if (myLayout == 'vertical-menu-template-bordered') {
  dashboardRename = config.vertical_menu_template_bordered.dashboardRename;
  pugSrc = config.vertical_menu_template_bordered.pugSrc;
} else if (myLayout == 'vertical-collapsed-menu-template') {
  dashboardRename = config.vertical_collapsed_menu_template.dashboardRename;
  pugSrc = config.vertical_collapsed_menu_template.pugSrc;
} else if (myLayout == 'vertical-overlay-menu-template') {
  dashboardRename = config.vertical_overlay_menu_template.dashboardRename;
  pugSrc = config.vertical_overlay_menu_template.pugSrc;
} else if (myLayout == 'horizontal-menu-template') {
  dashboardRename = config.horizontal_menu_template.dashboardRename;
  pugSrc = config.horizontal_menu_template.pugSrc;
} else if (myLayout == 'horizontal-menu-template-dark') {
  dashboardRename = config.horizontal_menu_template.dashboardRename;
  pugSrc = config.horizontal_menu_template.pugSrc;
}
// else if (myLayout == "horizontal-menu-template-nav") {
//   dashboardRename = config.horizontal_menu_template_nav.dashboardRename
//   pugSrc = config.horizontal_menu_template_nav.pugSrc
// }

const autoPrefixTasks = require('./gulp-tasks/autoprefix')(gulp);
const beautifyTasks = require('./gulp-tasks/beautify')(gulp);
const cleanTasks = require('./gulp-tasks/clean')(gulp);
const copyTask = require('./gulp-tasks/copy')(gulp);
const cssTasks = require('./gulp-tasks/css')(gulp);
const fileWriteTasks = require('./gulp-tasks/file-write')(gulp);
const pugTasks = require('./gulp-tasks/pug')(gulp);
const replaceTasks = require('./gulp-tasks/replace')(gulp);
const scssTasks = require('./gulp-tasks/scss')(gulp);
const uglifyTasks = require('./gulp-tasks/uglify')(gulp);
const notifyTasks = require('./gulp-tasks/notify')(gulp);
const validateHTML = require('./gulp-tasks/validate-html')(gulp);
const vendorsTasks = require('./gulp-tasks/vendors')(gulp);
const copyLaravel = require('./gulp-tasks/laravel')(gulp);
const copyAngular = require('./gulp-tasks/angular')(gulp);
const copyReact = require('./gulp-tasks/react')(gulp);
const copyVue = require('./gulp-tasks/vue')(gulp);
const copyVueLaravel = require('./gulp-tasks/vue-laravel')(gulp);

// Create Vendors files
gulp.task('dist-vendor-js', vendorsTasks.js);
gulp.task('dist-vendor-css', gulp.series(vendorsTasks.css, cleanTasks.css_rtl_vendor, cssTasks.css_rtl_vendor));

// Clean CSS & JS
gulp.task('dist-clean', cleanTasks.css, cleanTasks.js);

// Dist HTML
gulp.task(
  'dist-html',
  gulp.series(cleanTasks.html, fileWriteTasks.file_write, pugTasks.html, pugTasks.rename, notifyTasks.html)
);

// Monitor Changes
gulp.task('monitor', gulp.series(fileWriteTasks.file_write, gulp.parallel(pugTasks.watch, scssTasks.watch)));
// Dist JS
gulp.task('dist-js', gulp.series(cleanTasks.js, copyTask.js, uglifyTasks.js, notifyTasks.js));

// SASS Compile
gulp.task(
  'sass-compile',
  gulp.parallel(scssTasks.main, scssTasks.core, scssTasks.pages, scssTasks.plugins, scssTasks.themes, scssTasks.style)
);
// SASS Compile RTL
gulp.task('sass-compile-rtl', scssTasks.rtl);

// CSS Distribution Task.
gulp.task(
  'dist-css',
  gulp.series(
    cleanTasks.css,
    'sass-compile',
    autoPrefixTasks.css,
    cssTasks.css_comb,
    cssTasks.css_min,
    gulp.parallel(notifyTasks.css)
  )
);

// RTL CSS Distribution Task.
gulp.task(
  'dist-css-rtl',
  gulp.series(
    cleanTasks.css_rtl,
    'sass-compile',
    'sass-compile-rtl',
    cssTasks.css_rtl,
    autoPrefixTasks.css_rtl,
    cssTasks.css_rtl_comb,
    cssTasks.css_rtl_min
  )
);

// DEFAULT
gulp.task('dist', gulp.parallel('dist-css', 'dist-js'));

gulp.task('default', gulp.parallel('dist-css', 'dist-js'));

// Dist SK HTML
gulp.task(
  'dist-sk-html',
  gulp.series(cleanTasks.sk_html, fileWriteTasks.sk_file_write, pugTasks.sk_html, notifyTasks.html)
);
// Dist Documentation
gulp.task('dist-documentation', gulp.series(cleanTasks.documentation, pugTasks.documentation));

// Beautify HTML
gulp.task('beautify-html', gulp.series(beautifyTasks.html, beautifyTasks.starter_kit));

// Replacement Tasks
gulp.task('replacement', gulp.series(replaceTasks.css, replaceTasks.js));

// Validate HTML
gulp.task('w3c', validateHTML.validate);

// Clean CSS & JS
gulp.task('laravel-clean', gulp.series(cleanTasks.laravel_js, cleanTasks.laravel_scss));

// Copy scss, js & Static assets to Laravel HTML
gulp.task('copy-to-laravel', gulp.series(cleanTasks.laravel_js, cleanTasks.laravel_scss, copyLaravel.all));
gulp.task('copy-to-laravel:starter', copyLaravel.allSk);
gulp.task('copy-to-angular', copyAngular.all);
gulp.task('copy-to-angular:starter', copyAngular.allSk);
gulp.task('copy-to-react', copyReact.all);
gulp.task('copy-to-react:starter', copyReact.allSk);
gulp.task('copy-to-vue', copyVue.all);
gulp.task('copy-to-vue:starter', copyVue.allSk);
gulp.task('copy-to-vue-laravel', copyVueLaravel.all);
gulp.task('copy-to-vue-laravel:starter', copyVueLaravel.allSk);
