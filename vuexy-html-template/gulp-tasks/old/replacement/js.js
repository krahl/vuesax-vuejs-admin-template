var replace = require('gulp-replace');

module.exports = function (gulp, callback) {
  return gulp
    .src('html-demo/**/*.html')
    .pipe(replace(/(<script\b.+src=")(?!http)(.+\bapp-assets\b.[^vendors].+[^min])(\.js)(".*>)/g, '$1$2.min$3$4'))
    .pipe(gulp.dest('./html-demo/'));
};
