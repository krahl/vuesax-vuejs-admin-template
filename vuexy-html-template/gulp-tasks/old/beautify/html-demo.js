var htmlbeautify = require('gulp-html-beautify');

module.exports = function (gulp, callback) {
  return gulp
    .src('html-demo/**/*.html')
    .pipe(htmlbeautify({ indentSize: 2 }))
    .pipe(gulp.dest('./html-demo/'));
};
