var htmlbeautify = require('gulp-html-beautify');

module.exports = function (gulp, callback) {
  return gulp
    .src('html/**/*.html')
    .pipe(htmlbeautify({ indentSize: 2 }))
    .pipe(gulp.dest('./html/'));
};
