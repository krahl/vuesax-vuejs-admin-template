var pug = require('gulp-pug');
var rename = require('gulp-rename');
var watch = require('gulp-watch');

module.exports = (gulp, callback) => {
  const pugDocumentationTask = function () {
    return gulp
      .src(pugSrc, { cwd: config.source.documentation + '/pages/' })
      .pipe(
        pug({
          pretty: true,
          data: {
            // debug: false,
            useLayout: myLayout, // Predefined layout name i.e vertical-light-sidebar
            useDirection: myTextDirection,
            rtl: rtl,
            style: style,
            app_assets_path: config.app_assets_path,
            assets_path: config.assets_path
          }
        })
      )
      .pipe(gulp.dest(config.html + '/' + myTextDirection + '/' + myLayoutName));
  };

  const pugHtmlTask = function () {
    return gulp
      .src(pugSrc, { cwd: config.source.template + '/pages/' })
      .pipe(
        pug({
          pretty: true,
          data: {
            // debug: false,
            useLayout: myLayout, // Predefined layout name i.e vertical-light-sidebar
            useDirection: myTextDirection,
            rtl: rtl,
            style: style,
            app_assets_path: config.app_assets_path,
            assets_path: config.assets_path
          }
        })
      )
      .pipe(gulp.dest(config.html + '/' + myTextDirection + '/' + myLayoutName));
  };

  const pugRenameTask = function () {
    return gulp
      .src(dashboardRename, {
        cwd: config.html + '/' + myTextDirection + '/' + myLayoutName + '/'
      })
      .pipe(rename('index.html'))
      .pipe(gulp.dest(config.html + '/' + myTextDirection + '/' + myLayoutName + '/'));
  };

  const pugSkHtmlTask = function () {
    return gulp
      .src(pugSrc, { cwd: config.source.template + '/pages/starter-kit/' })
      .pipe(
        pug({
          pretty: true,
          data: {
            // debug: false,
            useLayout: myLayout, // Predefined layout name i.e vertical-light-sidebar
            useDirection: myTextDirection,
            rtl: rtl,
            style: style,
            app_assets_path: config.app_assets_path,
            assets_path: config.assets_path
          }
        })
      )
      .pipe(gulp.dest(config.starter_kit + '/' + myTextDirection + '/' + myLayoutName));
  };

  const pugWatchTask = function () {
    return watch(config.source.template + '/pages/*.pug')
      .pipe(
        pug({
          pretty: true,
          data: {
            // debug: false,
            useLayout: myLayout, // Predefined layout name i.e vertical-light-sidebar
            useDirection: myTextDirection,
            rtl: rtl,
            style: style,
            app_assets_path: config.app_assets_path,
            assets_path: config.assets_path
          }
        })
      )
      .pipe(gulp.dest(config.html + '/' + myTextDirection + '/' + myLayoutName));
  };

  // ---------------------------------------------------------------------------
  // Exports

  return {
    documentation: pugDocumentationTask,
    html: pugHtmlTask,
    rename: pugRenameTask,
    sk_html: pugSkHtmlTask,
    watch: pugWatchTask
  };
};
