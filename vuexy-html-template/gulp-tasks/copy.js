var gulpCopy = require('gulp-copy');

module.exports = (gulp, callback) => {
  const copyJsTask = function () {
    return gulp.src(config.source.js + '/**/*.js').pipe(gulp.dest(config.destination.js));
  };

  const copyLaravelJSTask = function () {
    return gulp.src(config.source.js + '/**/*.js').pipe(gulp.dest(config.destination.laravel_js));
  };

  const copyLaravelScssTask = function () {
    return gulp.src(config.source.sass + '/**/*.scss').pipe(gulp.dest(config.destination.laravel_css));
  };

  // ---------------------------------------------------------------------------
  // Exports

  return {
    js: copyJsTask,
    laravel_js: copyLaravelJSTask,
    laravel_scss: copyLaravelScssTask
  };
};
