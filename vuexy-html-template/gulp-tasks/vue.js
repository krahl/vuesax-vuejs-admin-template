const gulpCopy = require('gulp-copy');
const { parallel } = require('gulp');

module.exports = (gulp, callback) => {
  const vueStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.vue_css));
  };

  const vueSkStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.vue_sk_css));
  };

  // ---------------------------------------------------------------------------
  // Exports

  return {
    all: vueStyleTask,
    allSk: vueSkStyleTask
  };
};
