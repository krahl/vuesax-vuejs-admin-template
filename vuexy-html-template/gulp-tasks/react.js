const gulpCopy = require('gulp-copy');
const { parallel } = require('gulp');

module.exports = (gulp, callback) => {
  const reactStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.react_css));
  };

  const reactSkStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.react_sk_css));
  };

  // ---------------------------------------------------------------------------
  // Exports

  return {
    all: reactStyleTask,
    allSk: reactSkStyleTask
  };
};
