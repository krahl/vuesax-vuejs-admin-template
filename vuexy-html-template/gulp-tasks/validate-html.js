var validateHTML = require('gulp-w3c-html-validator');
var through2 = require('through2');
var fs = require('fs');
module.exports = (gulp, callback) => {
  const validate = function () {
    fs.writeFile('./validationErrors.log', '', function (err) {
      console.log(err);
    });
    const createErrorLog = function (file, encoding, callback) {
      callback(null, file);
      if (!file.w3cjs.success) {
        let err = `
[file : ${file.history}, Line : ${JSON.stringify(
          file.w3cjs.messages.filter(i => i.lastLine)
        )},  Error : ${JSON.stringify(file.w3cjs.messages.filter(j => j.message))}]
        `;
        fs.appendFile('./validationErrors.log', err, function () {});
      }
    };
    return gulp
      .src(config.html + '/' + myTextDirection + '/' + myLayoutName + '/*.html')
      .pipe(validateHTML())
      .pipe(through2.obj(createErrorLog));
  };
  return {
    validate
  };
};
