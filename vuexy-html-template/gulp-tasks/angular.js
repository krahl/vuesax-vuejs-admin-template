const gulpCopy = require('gulp-copy');
const { parallel } = require('gulp');

module.exports = (gulp, callback) => {
  const angularStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.angular_css));
  };

  const angularSkStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.angular_sk_css));
  };

  // ---------------------------------------------------------------------------
  // Exports

  return {
    all: angularStyleTask,
    allSk: angularSkStyleTask
  };
};
