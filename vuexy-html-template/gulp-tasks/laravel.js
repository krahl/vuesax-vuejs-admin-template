const gulpCopy = require('gulp-copy');
const { parallel } = require('gulp');

module.exports = (gulp, callback) => {
  const laravelStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.laravel_css));
  };

  const laravelJsTask = function () {
    return gulp
      .src(config.source.js + '/**/*', {
        base: config.source.js
      })
      .pipe(gulp.dest(config.destination.laravel_js));
  };

  const laravelAssetsTask = function () {
    return gulp
      .src(
        [
          'app-assets/data/**/*',
          '!app-assets/data/locales',
          '!app-assets/data/locales/*',
          'app-assets/fonts/**/*',
          'app-assets/images/**/*',
          'app-assets/vendors/**/*'
        ],
        {
          base: 'app-assets'
        }
      )
      .pipe(gulp.dest(config.destination.laravel_public));
  };

  const laravelSkStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.laravel_sk_css));
  };

  const laravelSkJsTask = function () {
    return gulp
      .src(config.source.js + '/**/*', {
        base: config.source.js
      })
      .pipe(gulp.dest(config.destination.laravel_sk_js));
  };

  const laravelSkAssetsTask = function () {
    return gulp
      .src(
        [
          'app-assets/data/**/*',
          '!app-assets/data/locales',
          '!app-assets/data/locales/*',
          'app-assets/fonts/**/*',
          'app-assets/images/**/*',
          'app-assets/vendors/**/*'
        ],
        {
          base: 'app-assets'
        }
      )
      .pipe(gulp.dest(config.destination.laravel_sk_public));
  };

  // ---------------------------------------------------------------------------
  //

  const laravelAllTask = parallel(laravelStyleTask, laravelJsTask, laravelAssetsTask);
  const laravelSkAllTask = parallel(laravelSkStyleTask, laravelSkJsTask, laravelSkAssetsTask);

  // ---------------------------------------------------------------------------
  // Exports

  return {
    style: laravelStyleTask,
    js: laravelJsTask,
    assets: laravelAssetsTask,
    all: laravelAllTask,
    styleSk: laravelSkStyleTask,
    jsSk: laravelSkJsTask,
    assetsSk: laravelSkAssetsTask,
    allSk: laravelSkAllTask
  };
};
