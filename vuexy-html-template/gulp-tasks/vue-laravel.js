const gulpCopy = require('gulp-copy');
const { parallel } = require('gulp');

module.exports = (gulp, callback) => {
  const vueLaravelStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.vue_laravel_css));
  };

  const vueLaravelSkStyleTask = function () {
    return gulp
      .src(config.source.sass + '/**/*', {
        base: config.source.sass
      })
      .pipe(gulp.dest(config.destination.vue_laravel_sk_css));
  };

  // ---------------------------------------------------------------------------
  // Exports

  return {
    all: vueLaravelStyleTask,
    allSk: vueLaravelSkStyleTask
  };
};
