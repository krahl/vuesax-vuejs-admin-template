import os, shutil

src = ['app-assets', 'assets', 'gulp-tasks-dist', 'html', 'html-demo', 'documentation.html', 'src/js', 'src/scss', 'starter-kit', '.csscomb.json', 'package.json', 'CHANGELOG.md', 'README.md', 'index.html', 'config.json', 'gulpfile-dist.js']
dest = 'package'

print("Deleting package folder...")
if os.path.isdir('package'):
  shutil.rmtree('package')

print("Creating the package...")
def copy_files(src, dest):
	if not os.path.isdir(dest):
		os.mkdir(dest)
	for item in src:
		if os.path.isfile(item):
			shutil.copy(item, dest)
		if os.path.isdir(item):

			# go in directory
			os.chdir(dest)

			# copy now with os.apth join
			shutil.copytree(os.path.join('..', item), item)

			# step back
			os.chdir('..')

copy_files(src, dest)
os.chdir(dest)
# renaming directory & file
os.rename("html-demo","html-customizer")
os.rename("gulpfile-dist.js","gulpfile.js")
os.rename("gulp-tasks-dist","gulp-tasks")
print("All Done ('_')")
