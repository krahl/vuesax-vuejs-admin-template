
# Custom Navbar

<box header>
	
Vuexy Admin uses slightly modified version of stock `vs-navbar`

</box>


<box>
	
## Creating Custom Navbar

Stock navbar provided by Vuesax Framework is responsive and good. Although in some cases we don't that kind of responsiveness. To give better UX, Vuexy Admin hides some of it's element in navbar, making it easier to access.

To create such navbar, Vuexy Admin used `navbar-custom` class in `vx-navbar` component. This class changes some of styles applied to stock navbar. Below is how you can add that class also.

```<vs-navbar class="vx-navbar navbar-custom">```

You can check Vuexy Admin's custom navbar in [demo](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1).

</box>
