
# Tailwind Colors

<box header>
	
Explore what else you can do with tailwind colors

</box>

<box>

## Tailwind's Color
	
Vuexy Admin is already packed with some nice theme colors like `primary`, `success` etc.

tailwind provides much more colors than this colors. But for consistency we removed all useless colors which are not required anymore in Vuexy Admin.

:::tip
You can add more colors for creating more classes in `tailwind.js`
:::

</box>


<box>

## Vuexy Admin colors
	
Vuexy Admin's main colors is not based on tailwind. classes like `text-primary`, `bg-primary` etc. are created manually in `src/assets/scss/vuexy/_customClasses.scss`

:::tip
We created classes manually, So that you can configure all your theme configurations at one place in `themeConfig.js`.
:::

</box>
