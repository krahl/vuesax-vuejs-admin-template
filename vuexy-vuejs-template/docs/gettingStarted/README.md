
# Libraries

<box header>

Things you need to know before starting with Vuexy dashboard admin template

</box>

<box>

## [VueJS](https://vuejs.org/)

VueJS is a progressive framework for building user interfaces. Vuexy Admin template is build upon VueJS. It is mandatory to know what is VueJS, If you don't know what is VueJS and how to use it, we highly recommend learninng VueJS before proceeding.

</box>

<box>

## [Vue CLI 3](https://cli.vuejs.org/)

Vue CLI is a full system for rapid Vue.js development. Vue CLI aims to be the standard tooling baseline for the Vue ecosystem.

</box>

<box>

## [Vuesax](https://lusaxweb.github.io/vuesax/)

Vuesax is a library of Vuejs components that facilitates front-end development. It is beautifully designed and pleasant looking framework.

</box>

<box>

## [Tailwind](https://tailwindcss.com)

Tailwind is a utility-first CSS framework for rapidly building custom user interfaces.

</box>
