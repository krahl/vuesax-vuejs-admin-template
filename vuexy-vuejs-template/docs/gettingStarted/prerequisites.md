
# Prerequisites

<box header>

Things you need to setup Vuexy Admin.

</box>

<box>

## Node.js

To install and use Vuexy Admin, It's required to install [Node.js](https://nodejs.org/) on your computer. Node is required only to run and deploy the application. It's not required to have detailed knowledge of Node.js

</box>

<box>

## Node Package Manager

Vuexy Admin uses [npm](https://www.npmjs.com/) package manager to install and manage 3rd party components and libraries.

</box>
