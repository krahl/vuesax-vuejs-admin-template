# RTL

<box header>

Here's some FAQ regarding RTL.

</box>

<box>
  
## Where's RTL defined

You can find RLT declaration in `themeConfig.js` file in root of your project folder. If you want to change RTL option on or off just change property `rtl` in `themeConfig` object.

</box>

<box>
  
## How to disable RTL

Just set `false` in `themeConfig` object's `rtl` property to disable RTL in your project.

</box>

<box>
  
## How to add RTL language

For this you all have to do is use i18n plugin/package & add your RTL language in your project. Please read guide "Localization" on adding new locale in our docs.

</box>
