# Other FAQs

<box header>

Here is some development related FAQs. Please make sure to check this before creating support ticket on our portal.

</box>

<box>
  
<vs-collapse accordion type="margin">
 <vs-collapse-item>
   <div slot="header">
     <span class="text-base">How to upgrade to latest version?</span>
   </div>
   <p>Please refer to <a target="_blank" rel="nofollow" href="https://pixinvent.ticksy.com/article/15342">this article</a> on our support portal.</p>
 </vs-collapse-item>

 <vs-collapse-item>
   <div slot="header">
     <span class="text-base">Access Control(vue-acl) route protection is not working?</span>
   </div>
   <p>As we mentioned in documentation of vue-acl package. We used vue-acl library for providing ACL in our application. Creator of vue-acl rewritten whole code since 4.0.7(approx). Since then route protection is not working.</p>
   <br>
   <p>So, we highly recommend till creator of that library don't fix that bug, Please use vue-acl@4.0.7.</p>
 </vs-collapse-item>

 <vs-collapse-item>
   <div slot="header">
     <span class="text-base">Which is better <code>vs-table</code> or <code>ag-grid</code>?</span>
   </div>
   <p>Please refer to <a target="_blank" rel="nofollow" href="https://pixinvent.ticksy.com/article/15336">this article</a> on our support portal.</p>
 </vs-collapse-item>

 <vs-collapse-item>
   <div slot="header">
     <span class="text-base">Where is native Laravel Auth?</span>
   </div>
   <p>Please refer to <a target="_blank" rel="nofollow" href="https://pixinvent.ticksy.com/article/15338">this article</a> on our support portal.</p>
 </vs-collapse-item>

 <vs-collapse-item>
   <div slot="header">
     <span class="text-base"><code>npm run dev</code> gives "Support for the experimental syntax ‘classProperties’ isn’t currently enabled"?</span>
   </div>
   <p>Please refer to <a target="_blank" rel="nofollow" href="https://pixinvent.ticksy.com/article/15431">this article</a> on our support portal.</p>
 </vs-collapse-item>

</vs-collapse>

Please consider checking our [Articles](https://pixinvent.ticksy.com/articles/100014569) if you find your Question listed there.

</box>
