
# Third-Party Plugins

<box header>
  
Vuexy Admin comes with useful third party plug-ins to help you building your app quickly.

</box>


<box>
  
## agGrid Table

**Description:** THE best javascript grid in the world

**Demo:**  [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/ui-elements/ag-grid-table)
  
**Docs:** [Documentation](https://www.ag-grid.com/vue-getting-started/)

</box>


<box>
  
## Algolia

**Description:** Algolia’s full suite APIs enable teams to develop unique search and discovery experiences for their customers across all platforms and devices.

**Demo:**  [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/apps/eCommerce/shop)
  
**Docs:** [Documentation](https://www.algolia.com/doc)

</box>

<box>
  
## Axios

**Description:** Promise based HTTP client for the browser and node.js

**Demo:**  [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/ui-elements/data-list/thumb-view)
  
**Docs:** [Documentation](https://github.com/axios/axios)

</box>


<box>
  
## Vue InstantSearch

**Description:** InstantSearch is a family of open-source, production-ready UI libraries that eases the usage and installation of the Algolia search engine.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/apps/eCommerce/shop)

**Docs:** [Documentation](https://www.algolia.com/doc/guides/building-search-ui/what-is-instantsearch/vue/)

</box>


<box>
  
## Auth0

**Description:** Auth0 provides universal authentication & authorization platform for web, mobile and legacy applications.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/pages/login)

**Docs:** [Documentation](https://auth0.com/docs/quickstart/spa/vuejs)

</box>


<box>
  
## Firebase

**Description:** Firebase is Google's mobile platform that helps you quickly develop high-quality apps and grow your business.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/pages/login)

**Docs:** [Documentation](https://firebase.google.com/docs/)

</box>


<box>
  
## Apex Charts

**Description:** ApexCharts.js - An open-source HTML5 JavaScript charting library that helps developers to create responsive & interactive JS charts for web pages.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/charts-and-maps/charts/apex-charts)

**Docs:** [Documentation](https://apexcharts.com/docs/vue-charts/)

</box>


<box>
  
## ChartsJS

**Description:** Easy and beautiful charts with Chart.js and Vue.js

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/charts-and-maps/charts/chartjs)

**Docs:** [Documentation](https://vue-chartjs.org/guide/)

</box>


<box>
  
## Clipboard

**Description:** A simple vuejs 2 binding for clipboard.js

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/clipboard)

**Docs:** [Documentation](https://github.com/Inndy/vue-clipboard2)

</box>


<box>
  
## Context Menu

**Description:** `vue-content` provides a simple yet flexible context menu for Vue.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/context-menu)

**Docs:** [Documentation](https://vue-context.randallwilk.com/docs)

</box>


<box>
  
## ECharts

**Description:** ECharts component for Vue.js.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/charts-and-maps/charts/echarts)

**Docs:** [Documentation](https://github.com/ecomfe/vue-echarts)

</box>


<box>
  
## VeeValidate

**Description:** Template Based Validation Framework for Vue.js

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/forms/form-validation)

**Docs:** [Documentation](https://baianat.github.io/vee-validate/guide/)

</box>


<box>
  
## Vue Datepicker

**Description:** A simple Vue.js datepicker component. Supports disabling of dates, inline mode, translations

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/datepicker)

**Docs:** [Documentation](https://github.com/charliekassel/vuejs-datepicker)

</box>


<box>
  
## Vue Flatpickr Component

**Description:** Vue.js component for Flatpickr datetime picker

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/datetime-picker)

**Docs:** [Documentation](https://github.com/ankurk91/vue-flatpickr-component)

</box>


<box>
  
## Vue2 Hammer

**Description:** Hammer.js wrapper for Vue 2.x to support some touching operation in the mobile.[Swipe In/Out sidemenu]

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/)

**Docs:** [Documentation](https://github.com/bsdfzzzy/vue2-hammer)

</box>


<box>
  
## Vue Form Wizard

**Description:** Vue-form-wizard is a vue based component with no external depenendcies which simplifies tab wizard management.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/forms/form-wizard)

**Docs:** [Documentation](https://binarcode.github.io/vue-form-wizard/#/)

</box>


<box>
  
## Vue Full Calendar

**Description:** vue calendar fullCalendar. no jquery required. Schedule events management.

**Demo:** _Removed_

**Docs:** [Documentation](https://github.com/Wanderxx/vue-fullcalendar)

</box>


<box>
  
## Vue Simple Calendar

**Description:** Simple Vue component to show a month-grid calendar with events

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/apps/calendar/vue-simple-calendar)

**Docs:** [Documentation](https://github.com/richardtallent/vue-simple-calendar)

</box>


<box>
  
## Vue Select

**Description:** A Vue.js select component that provides similar functionality to Select2/Chosen without the overhead of jQuery.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/select)

**Docs:** [Documentation](https://sagalbot.github.io/vue-select/docs/)

</box>


<box>
  
## Vue Draggable

**Description:** Vue drag-and-drop component based on Sortable.js

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/drag-and-drop)

**Docs:** [Documentation](https://github.com/SortableJS/Vue.Draggable)

</box>


<box>
  
## Vue Video Player

**Description:** videojs component for vuejs

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/ui-elements/card/basic)

**Docs:** [Documentation](https://surmon-china.github.io/vue-video-player/)

</box>


<box>
  
## Vue Feather Icons

**Description:** Simply beautiful open source icons as Vue functional components.

<!-- **Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/) -->

**Docs:** [Documentation](https://github.com/egoist/vue-feather-icons)

</box>


<box>
  
## Vue Google Maps

**Description:** 

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/charts-and-maps/maps/google-map)

**Docs:** [Documentation](https://github.com/xkjyeah/vue-google-maps)

</box>


<box>
  
## Vue I18n

**Description:** Vue I18n is internationalization plugin for Vue.js

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/i18n)

**Docs:** [Documentation](https://kazupon.github.io/vue-i18n/introduction.html)

</box>


<box>
  
## Vue Perfect Scollbar

**Description:** scrollbar for vue, depend on perfect-scrollbar

**Demo:** Main Sidebar

**Docs:** [Documentation](https://github.com/Degfy/vue-perfect-scrollbar)

</box>


<box>
  
## Vue Prism Component

**Description:** highlight code using prism.js and vue component

**Demo:** Card Code Toggle

**Docs:** [Documentation](https://github.com/egoist/vue-prism-component)

</box>


<box>
  
## Vue Quill Editor

**Description:** Quill editor component for Vue, support SPA and SSR.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/quill-editor)

**Docs:** [Documentation](https://github.com/surmon-china/vue-quill-editor)

</box>


<box>
  
## Vue Simple Suggest

**Description:** Feature-rich autocomplete component for Vue.js

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/autocomplete)

**Docs:** [Documentation](https://github.com/KazanExpress/vue-simple-suggest)

</box>


<box>
  
## Vue Star Ratings

**Description:** A simple, highly customisable star rating component for Vue 2.x

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/star-ratings)

**Docs:** [Documentation](https://github.com/craigh411/vue-star-rating)

</box>


<box>
  
## Vue Tour

**Description:** Vue Tour is a lightweight, simple and customizable guided tour plugin for use with Vue.js. It provides a quick and easy way to guide your users through your application.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/)

**Docs:** [Documentation](https://github.com/pulsardev/vue-tour)

</box>


<box>
  
## Vue Tree Halower

**Description:** tree and multi-select component based on Vue.js 2.0

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/tree)

**Docs:** [Documentation](https://github.com/halower/vue-tree)

</box>


<box>
  
## Vue BackToTop

**Description:** Quill editor component for Vue, support SPA and SSR.

**Demo:** [Visit - Scroll down in page](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/)

**Docs:** [Documentation](https://github.com/caiofsouza/vue-backtotop)

</box>


<box>
  
## Vue Awesome Swiper - Image Slider

**Description:** Quill editor component for Vue, support SPA and SSR.

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/carousel)

**Docs:** [Documentation](https://surmon-china.github.io/vue-awesome-swiper/)

</box>


<box>
  
## Vue Clipboard

**Description:** A simple vue2 binding to clipboard.js

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/clipboard)

**Docs:** [Documentation](https://github.com/Inndy/vue-clipboard2)

</box>


<box>
  
## Vue ACL

**Description:** Help you to control the permission of access in your app for yours components and routes

**Demo:** [Visit](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/demo-1/extensions/access-control-editor)

**Docs:** [Documentation](https://github.com/leonardovilarinho/vue-acl)

</box>
