# Introduction

<box header>

Vuexy - VueJS admin dashboard template based on Vuesax Component Framework, CLI, Vue Router and Vuex.

</box>

<box>

::: warning
Vuesax version of vuexy is deprecated from v6.1

We provided vuexy with new BootstrapVue framework which is more stable than Vuesax framework.

Please check our new [docs](https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/documentation/) to find out why we will remove Vuesax version of vuexy.
:::

Vuexy is the most developer friendly & highly customisable VueJS Admin Dashboard Template based on Vue CLI, Vuex & Vuesax component framework.

It comes with unique features like **fuzzy search, bookmarks, floating navbar, dark layout, advance cards and charts**. It comes with **2 niche dashboards**, **5 workable apps** like email, chat, todo, e-commerce and calendar.
Vuexy Admin can be used for any type of web applications: Project Management, eCommerce backends, CRM, Analytics, Banking, Education, Fitness or any custom admin panels.

<br/>

We’ve followed best industry standards to make our product easy, fast & highly scalable to work with. Vuexy is the most convenient vuejs admin dashboard template for developers which allows you to build **eye-catching, high-quality and high-performance responsive** single page applications which looks great on Desktops, Tablets, and Mobile Devices.

</box>

<box>

## Features

- Pure Vue js, No Jquery Dependency
- Created with Vue CLI
- Utilizes Vuex, Vue Router, Webpack
- Code Splitting, Lazy loading
- Multi-lingual Support
- 3 chart libraries
- 2 Dashboard
- SASS Powered
- Feather Icons
- 5 Workable Applications
  _ email
  _ chat
  - todo
  - eCommerce
  - calendar
- Unlimited Color Options
- Google Map
- Import/Export
- Context Menu
- Video Player
- Drag & Drop
- Fully Responsive Layout
- Organized Folder Structure
- Clean & Commented Code
- Well Documented
- FREE Lifetime Updates
- 6 months of free support included

</box>

<box>

## Technical Specification (Credits :pray:)

- Powered by [Vue](https://vuejs.org)
- [Vue CLI 3](https://cli.vuejs.org/)
- [Vuesax](https://lusaxweb.github.io/vuesax/) - Vuejs component Library
- [Tailwind](https://tailwindcss.com/)
- [Vuex](https://vuex.vuejs.org/)
- [Vue Router](https://router.vuejs.org/)
- [VuePress](https://vuepress.vuejs.org/)
- [Axios](https://github.com/axios/axios)
- [Algolia Search](https://www.algolia.com/)
- [Vue InstantSearch](https://www.algolia.com/doc/guides/building-search-ui/what-is-instantsearch/vue/)
- [Auth0](https://auth0.com/)
- [Firebase](https://firebase.google.com/)
- SASS Powered
- Charts
  - [Apex Charts](https://apexcharts.com/vue-chart-demos/)
  - [Chartjs](https://vue-chartjs.org/)
  - [eCharts](https://ecomfe.github.io/vue-echarts/demo/)
- [VeeValidate](https://baianat.github.io/vee-validate/)
- [Vue Form Wizard](https://github.com/BinarCode/vue-form-wizard)
- [Vue Full Calendar](https://github.com/Wanderxx/vue-fullcalendar)
- [Vue Select](https://sagalbot.github.io/vue-select/)
- [Vue Draggable](https://github.com/SortableJS/Vue.Draggable)
- [Vue Video Player](https://github.com/surmon-china/vue-video-player)
- [Vue Feather Icons](https://github.com/egoist/vue-feather-icons)
- [Vue Google Maps](https://github.com/xkjyeah/vue-google-maps)
- [Vue I18n](https://kazupon.github.io/vue-i18n/)
- [Vue Perfect Scollbar](https://github.com/Degfy/vue-perfect-scrollbar)
- [Vue Prism Component](https://github.com/egoist/vue-prism-component)
- [Vue Quill Editor](https://github.com/surmon-china/vue-quill-editor)
- [Vue Code](https://github.com/lusaxweb/vuecode)
- [Vue Datepicker](https://github.com/charliekassel/vuejs-datepicker)
- [Vue Tour](https://github.com/pulsardev/vue-tour)
- [Vue Backtotop](https://github.com/caiofsouza/vue-backtotop)
- [Vue Awesome Swiper - Image Slider](https://surmon-china.github.io/vue-awesome-swiper/)
- [Vue Clipboard](https://github.com/Inndy/vue-clipboard2)
- [Vue ACL](https://github.com/leonardovilarinho/vue-acl)

</box>
