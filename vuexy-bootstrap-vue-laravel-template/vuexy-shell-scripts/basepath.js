var fs = require('fs')

var demo = 'demo-1'
var demoArgs = process.argv.slice(2)

if (demoArgs[0] !== undefined) {
  demo = demoArgs[0]
}

var filePath = '/Applications/XAMPP/htdocs/vuexy-admin/vuexy-bootstrap-vue-laravel-template/' + demo + '/index.php'

var basePath = "$app->bind('path.public', function() {\n" + "return base_path('" + demo + "');\n" + '});'

if (fs.existsSync(filePath)) {
  fs.readFile(filePath, 'utf8', function(err, data) {
    if (err) {
      return console.log(err)
    }
    var result = data

    if (data.indexOf('base_path') >= 0) {
      result = data.replace(/base_path\(\'(.*)\'\)/, "base_path('" + demo + "')")
    } else {
      result = data.replace(/(\$app = require_once __DIR__.\'\/..\/bootstrap\/app.php\';)/, '$1\n\n' + basePath)
    }

    fs.writeFile(filePath, result, 'utf8', function(err) {
      if (err) return console.log(err)
    })
  })
}
