set -e
for i in {1..6}
do
    cd /Applications/XAMPP/htdocs/vuexy-admin/vuexy-bootstrap-vue-laravel-template/vuexy-shell-scripts
	node replace.js demo-$i
	cd ..
	npm run prod
	mv public demo-$i
	cd vuexy-shell-scripts
	node basepath.js demo-$i
    cd ..
	zip -r demo-$i.zip demo-$i -x "*.DS_Store"
	mv demo-$i public
done

cd vuexy-shell-scripts
node reset.js
