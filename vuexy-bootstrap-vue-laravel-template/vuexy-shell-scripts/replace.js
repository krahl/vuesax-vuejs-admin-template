var fs = require('fs')

var webpackFilePath = '/Applications/XAMPP/htdocs/vuexy-admin/vuexy-bootstrap-vue-laravel-template/webpack.mix.js'
var routerFilePath =
  '/Applications/XAMPP/htdocs/vuexy-admin/vuexy-bootstrap-vue-laravel-template/frontend/src/router/index.js'
var themeConfigFilePath =
  '/Applications/XAMPP/htdocs/vuexy-admin/vuexy-bootstrap-vue-laravel-template/frontend/themeConfig.js'
var appConfigFilePath =
  '/Applications/XAMPP/htdocs/vuexy-admin/vuexy-bootstrap-vue-laravel-template/frontend/src/store/app-config/index.js'

var demo = 'demo-1'
var demoArgs = process.argv.slice(2)

if (demoArgs[0] !== undefined) {
  demo = demoArgs[0]
}

// Webpack File Changes
if (fs.existsSync(webpackFilePath)) {
  fs.readFile(webpackFilePath, 'utf8', function(err, data) {
    if (err) {
      return console.log(err)
    }
    var result = data.replace(
      new RegExp(/(\/demo\/vuexy-vuejs-laravel-admin-template\/)(.*)\//, 'g'),
      '$1' + demo + '/'
    )

    fs.writeFile(webpackFilePath, result, 'utf8', function(err) {
      if (err) return console.log(err)
    })
  })
}

// Router File Changes
if (fs.existsSync(routerFilePath)) {
  fs.readFile(routerFilePath, 'utf8', function(err, data) {
    if (err) {
      return console.log(err)
    }
    var result = data.replace(
      new RegExp(/(base:\s*)((\')?.*(\')?)/, 'g'),
      "$1'/demo/vuexy-vuejs-laravel-admin-template/" + demo + "',"
    )

    fs.writeFile(routerFilePath, result, 'utf8', function(err) {
      if (err) return console.log(err)
    })
  })
}

// App Config File Changes for skin localstorage updation according to the current demo
if (fs.existsSync(appConfigFilePath)) {
  fs.readFile(appConfigFilePath, 'utf8', function(err, data) {
    if (err) {
      return console.log(err)
    }
    var result = data.replace(
      new RegExp(/(localStorage.(get|set)Item\(')(.*)('.*\))/, 'g'),
      '$1vuexy-bsv-laravel-' + demo + '-skin$4'
    )

    fs.writeFile(appConfigFilePath, result, 'utf8', function(err) {
      if (err) return console.log(err)
    })
  })
}

// Copy themeConfig file
if (fs.existsSync(themeConfigFilePath)) {
  fs.copyFile('./configs/' + demo + '/themeConfig.js', themeConfigFilePath, err => {
    if (err) {
      return console.log(err)
    }
  })
}
