# use in linux/unix system
echo 'use in linux/unix system'

# not properly work in git bash
echo 'You should run package.sh from parent folder. eg. ./scripts/package.sh'

rsync -aP  . ./full-version/ --exclude=node_modules --exclude=vendor --exclude=yarn.lock --exclude=composer.lock --exclude=public/images --exclude=public/css --exclude=public/fonts --exclude=public/js --exclude=.git --exclude=demo-* --exclude=vuexy-shell-scripts/

echo 'folders are compressing...'

# add zip in linux/unix system before use it.
# https://stackoverflow.com/questions/38782928/how-to-add-man-and-zip-to-git-bash-installation-on-windows

cd full-version
zip -r full-version.zip .
mv full-version.zip ../

cd ..

echo 'Zip Completed!'

rm -dr full-version

cd ../vuexy-bootstrap-vue-laravel-starter-kit/  || exit

echo 'Move in Starter-kit'

rsync -aP  . ./starter-kit/ --exclude=node_modules --exclude=vendor --exclude=yarn.lock --exclude=composer.lock --exclude=public/images --exclude=public/css --exclude=public/fonts --exclude=public/js --exclude=.git --exclude=demo-* --exclude=vuexy-shell-scripts/

echo 'folders are compressing...'

cd starter-kit

zip -r starter-kit.zip .

mv starter-kit.zip ../

cd ..

echo 'Zip Completed!'

rm -dr starter-kit

echo 'Task Done!'
