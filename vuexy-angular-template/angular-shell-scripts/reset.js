const fs = require('fs');

const angularJSONFilePath =
  '/Applications/XAMPP/xamppfiles/htdocs/vuesax-vuejs-admin-template/vuexy-angular-template/angular.json';
const configServiceFilePath =
  '/Applications/XAMPP/xamppfiles/htdocs/vuesax-vuejs-admin-template/vuexy-angular-template/src/@core/services/config.service.ts';
const navbarComponentFilePath =
  '/Applications/XAMPP/xamppfiles/htdocs/vuesax-vuejs-admin-template/vuexy-angular-template/src/app/layout/components/navbar/navbar.component.ts';
const appConfigFilePath =
  '/Applications/XAMPP/htdocs/vuesax-vuejs-admin-template/vuexy-angular-template/src/app/app-config.ts';

const demo = 'demo-1';

// Reset baseHrf to '/' in angular.json
if (fs.existsSync(angularJSONFilePath)) {
  fs.readFile(angularJSONFilePath, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    const result = data.replace(new RegExp(/("baseHref":\s)("(.*)")/, 'g'), '$1"/"');

    fs.writeFile(angularJSONFilePath, result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });
}

// Reset local storage key to 'config' in config.service.ts
if (fs.existsSync(configServiceFilePath)) {
  fs.readFile(configServiceFilePath, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    const result = data.replace(new RegExp(/(localStorage.(get|set)Item\(')(.*)('.*\))/, 'g'), `$1config$4`);

    fs.writeFile(configServiceFilePath, result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });
}

// reset local storage key to 'prevSkin' in navbar.component.ts
if (fs.existsSync(navbarComponentFilePath)) {
  fs.readFile(navbarComponentFilePath, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    const result = data.replace(
      new RegExp(/(localStorage.(get|set)Item\(')(.*prevSkin.*)('.*\))/, 'g'),
      `$1prevSkin$4`
    );

    fs.writeFile(navbarComponentFilePath, result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });
}

// Reset app-config file
if (fs.existsSync(appConfigFilePath)) {
  fs.copyFile(`./configs/${demo}/app-config.ts`, appConfigFilePath, err => {
    if (err) {
      return console.log(err);
    }
  });
}
