const fs = require('fs');

const angularJSONFilePath =
  '/Applications/XAMPP/xamppfiles/htdocs/vuesax-vuejs-admin-template/vuexy-angular-template/angular.json';
const configServiceFilePath =
  '/Applications/XAMPP/xamppfiles/htdocs/vuesax-vuejs-admin-template/vuexy-angular-template/src/@core/services/config.service.ts';
const navbarComponentFilePath =
  '/Applications/XAMPP/xamppfiles/htdocs/vuesax-vuejs-admin-template/vuexy-angular-template/src/app/layout/components/navbar/navbar.component.ts';
const appConfigFilePath =
  '/Applications/XAMPP/htdocs/vuesax-vuejs-admin-template/vuexy-angular-template/src/app/app-config.ts';

let demo = 'demo-1';
const demoArgs = process.argv.slice(2);

if (demoArgs[0] !== undefined) {
  demo = demoArgs[0];
}

// Update baseHrf in angular.json
if (fs.existsSync(angularJSONFilePath)) {
  fs.readFile(angularJSONFilePath, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    const result = data.replace(
      new RegExp(/("baseHref":\s)("(.*)")/, 'g'),
      `$1"/demo/vuexy-angular-admin-dashboard-template/${demo}/"`
    );

    fs.writeFile(angularJSONFilePath, result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });
}

// Replace local storage key 'config' with 'config-demo-*' in config.service.ts
if (fs.existsSync(configServiceFilePath)) {
  fs.readFile(configServiceFilePath, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    const result = data.replace(new RegExp(/(localStorage.(get|set)Item\(')(.*)('.*\))/, 'g'), `$1config12-${demo}$4`);

    fs.writeFile(configServiceFilePath, result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });
}

// Replace local storage key 'prevSkin' with 'prevSkin-demo-*'in navbar.component.ts
if (fs.existsSync(navbarComponentFilePath)) {
  fs.readFile(navbarComponentFilePath, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    const result = data.replace(
      new RegExp(/(localStorage.(get|set)Item\(')(.*prevSkin.*)('.*\))/, 'g'),
      `$1prevSkin12-${demo}$4`
    );

    fs.writeFile(navbarComponentFilePath, result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });
}

// Replace app-config file
if (fs.existsSync(appConfigFilePath)) {
  fs.copyFile(`./configs/${demo}/app-config.ts`, appConfigFilePath, err => {
    if (err) {
      return console.log(err);
    }
  });
}
