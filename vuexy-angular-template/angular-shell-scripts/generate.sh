set -e
for i in {1..6}
do
  cd /Applications/XAMPP/xamppfiles/htdocs/vuesax-vuejs-admin-template/vuexy-angular-template/angular-shell-scripts
	node replace.js demo-$i
	cd ..
	npm run build:prod
  cd dist
	mv vuexy demo-$i
	zip -r demo-$i.zip demo-$i
	rm -rf demo-$i
  cd ..
  cd angular-shell-scripts
  node reset.js
  cd ..
done
