import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ContentComponent } from './content/content.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [SidebarComponent, NavbarComponent, ContentComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [NavbarComponent, SidebarComponent, ContentComponent]
})
export class LayoutModule { }
