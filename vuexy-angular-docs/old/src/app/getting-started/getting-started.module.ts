import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IntroductionComponent } from "./introduction/introduction.component";
import { Routes, RouterModule } from "@angular/router";
import { MarkdownModule } from 'ngx-markdown';

// routing
const routes: Routes = [
  {
    path: "introduction",
    component: IntroductionComponent
  }
];

@NgModule({
  declarations: [IntroductionComponent],
  imports: [CommonModule, RouterModule.forChild(routes),
    MarkdownModule.forRoot()
  ]
})
export class GettingStartedModule {}
