import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { CoreCardComponent } from './core-card/core-card.component'
import { Routes, RouterModule } from '@angular/router'
import { MarkdownModule } from 'ngx-markdown'
import { CardSnippetComponent } from './card-snippet/card-snippet.component'
import { ContentHeaderComponent } from './content-header/content-header.component'
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component'
import { CardStatisticsComponent } from './card-statistics/card-statistics.component'
import { ToastsComponent } from './toasts/toasts.component'
import { CoreTouchspinComponent } from './core-touchspin/core-touchspin.component'

// routing
const routes: Routes = [
  {
    path: 'core-card',
    component: CoreCardComponent
  },
  {
    path: 'card-snippet',
    component: CardSnippetComponent
  },
  {
    path: 'content-header',
    component: ContentHeaderComponent
  },
  {
    path: 'breadcrumb',
    component: BreadcrumbComponent
  },
  {
    path: 'card-statistics',
    component: CardStatisticsComponent
  },
  {
    path: 'custom-toasts',
    component: ToastsComponent
  },
  {
    path: 'core-touchspin',
    component: CoreTouchspinComponent
  }
]

@NgModule({
  declarations: [
    CoreCardComponent,
    CardSnippetComponent,
    ContentHeaderComponent,
    BreadcrumbComponent,
    CardStatisticsComponent,
    ToastsComponent,
    CoreTouchspinComponent
  ],
  imports: [CommonModule, RouterModule.forChild(routes), MarkdownModule.forRoot()]
})
export class CustomComponentsModule {}
