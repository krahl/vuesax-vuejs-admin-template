import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "getting-started",
    loadChildren: () =>
      import("./getting-started/getting-started.module").then(
        (m) => m.GettingStartedModule
      ),
  },
  {
    path: "custom-components",
    loadChildren: () =>
      import("./custom-components/custom-components.module").then(
        (m) => m.CustomComponentsModule
      ),
  },
  {
    path: "custom-pipes",
    loadChildren: () =>
      import("./custom-pipes/custom-pipes.module").then(
        (m) => m.CustomPipesModule
      ),
  },
  {
    path: "**",
    redirectTo: "getting-started/introduction",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
