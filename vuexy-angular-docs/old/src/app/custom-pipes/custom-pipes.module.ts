import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FilterComponent } from "./filter/filter.component";
import { RouterModule, Routes } from "@angular/router";
import { MarkdownModule } from "ngx-markdown";
import { InitialsComponent } from './initials/initials.component';

// routing
const routes: Routes = [
  {
    path: "filter",
    component: FilterComponent,
  },
  {
    path: "initials",
    component: InitialsComponent,
  }
];

@NgModule({
  declarations: [FilterComponent, InitialsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MarkdownModule.forRoot(),
  ],
})
export class CustomPipesModule {}
