module.exports = {
  base: '/demo/vuexy-angular-admin-dashboard-template/documentation/',
  title: 'Vuexy - Angular Admin',
  description:
    'Vuexy - Angular Admin Dashboard Template is the most developer friendly & highly customizable Admin Dashboard Template based on Bootstrap 4, Bootstrap Vue',
  port: 7777,
  dest: '../public',
  head: [['link', { rel: 'icon', href: '/favicon.ico' }]],
  themeConfig: {
    lastUpdated: 'Last Updated',
    logo: '/logo.svg',
    nav: [
      { text: 'Guide', link: '/guide/' },
      { text: 'FAQ', link: '/faq/' },
      { text: 'Articles', link: '/articles/' },
      { text: 'Demo', link: 'https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/' },
      { text: 'Purchase', link: 'https://1.envato.market/vuexy_admin' }
    ],
    sidebar: {
      '/guide/': [
        ['', 'Welcome'],
        {
          title: 'Getting Started',
          collapsable: false,
          children: [
            '/guide/getting-started/introduction',
            '/guide/getting-started/starter-full',
            '/guide/getting-started/gitlab-access',
            '/guide/getting-started/support'
          ]
        },
        {
          title: 'Development',
          collapsable: false,
          children: [
            '/guide/development/installation',
            '/guide/development/folder-structure',
            '/guide/development/app-config',
            '/guide/development/splash-screen',
            // '/guide/development/layout',
            '/guide/development/translation',
            '/guide/development/template-styles',
            '/guide/development/routing',
            '/guide/development/route-animation',
            '/guide/development/navigation-menus',
            '/guide/development/role-based-jwt-auth'
          ]
        },
        {
          title: 'Layout',
          collapsable: false,
          children: [
            '/guide/layout/introduction',
            '/guide/layout/core-layouts',
            '/guide/layout/layout-components',
            '/guide/layout/layout-examples'
          ]
        },
        {
          title: 'Components',
          collapsable: false,
          children: [
            '/guide/components/alert',
            '/guide/components/button',
            '/guide/components/pill',
            '/guide/components/collapse',
            '/guide/components/rating',
            '/guide/components/modal'
          ]
        },
        {
          title: 'Forms',
          collapsable: false,
          children: ['/guide/forms/checkbox', '/guide/forms/radio', '/guide/forms/switch', '/guide/forms/form-wizard']
        },
        {
          title: 'Custom Components',
          collapsable: false,
          children: [
            '/guide/custom-components/card-actions',
            '/guide/custom-components/touch-spin',
            '/guide/custom-components/core-card-snippet'
          ]
        },
        {
          title: 'Directives',
          collapsable: false,
          children: ['/guide/directives/feather-icon', '/guide/directives/ripple-effect']
        },
        {
          title: 'Pipes',
          collapsable: false,
          children: ['/guide/pipes/filter', '/guide/pipes/initials', '/guide/pipes/safe', '/guide/pipes/strip-html']
        },
        {
          title: 'Services',
          collapsable: false,
          children: [
            '/guide/services/config',
            // '/guide/services/loading-screen',
            '/guide/services/media',
            '/guide/services/translation'
          ]
        }
      ]
    }
  },
  plugins: [
    [
      '@vuepress/medium-zoom',
      {
        selector: 'img.medium-zoom',
        options: {
          background: '#0e0e0ecc',
          margin: 50
        }
      }
    ]
  ]
}
