# How to add JWT in starter-kit?

We already provided Auth folder for developer usage in `starter-kit/src/app/auth` even if you are using starter-kit. It just isn't used in Login & Logout.
To implement JWT, please go through the below steps.
**Step 1** :- Update the `providers` array with below mentioned code & with their respective imports.
Navigate to file : `starter-kit/src/app/app.module.ts`.

<!-- prettier-ignore-start -->
```typescript{10-14}
import { HTTP_INTERCEPTORS } from '@angular/common/http'
import {  ErrorInterceptor, fakeBackendProvider, JwtInterceptor } from 'app/auth/helpers'
@NgModule({
   imports: [
     ...
     ...
   ],
   bootstrap: [AppComponent],
    providers: [
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
      fakeBackendProvider
    ]
  })
```
<!-- prettier-ignore-end -->

**Step 2** :- Implementing the authentication service on the login page.
Navigate to `starter-kit/src/app/main/pages/authentication/auth-login-v2/auth-login-v2.component.ts`.
Initialize the `AuthenticationService` in constructor, & update `onSubmit()` code with below mentioned code.

```typescript
import { first } from 'rxjs/operators'
import { AuthenticationService } from 'app/auth/service';
 constructor(
    private _authenticationService: AuthenticationService
  )
  onSubmit() {
    this.submitted = true
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return
    }
    // Login
    this.loading = true
    this._authenticationService
      .login(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this._router.navigate([this.returnUrl])
        },
        error => {
          this.error = error
          this.loading = false
        }
      )
  }
```

**Step 3** :- Implementing the authentication Service on Logout & dynamically update username & role in the navbar.
Navigate to file `starter-kit/src/app/layout/components/navbar/navbar.component.html`.
Replace the code which is inside the `<!-- User Dropdown --> <!--/ User Dropdown -->` comment with below mentioned code. This will update the `username` & `role` as per current user credentials & also will remove the Credentials on logout.

```html
<!-- User Dropdown -->
<li ngbDropdown class="nav-item dropdown-user">
  <a
    class="nav-link dropdown-toggle dropdown-user-link"
    id="dropdown-user"
    ngbDropdownToggle
    id="navbarUserDropdown"
    aria-haspopup="true"
    aria-expanded="false"
  >
    <ng-container *ngIf="this.currentUser">
      <div class="user-nav d-sm-flex d-none">
        <span class="user-name font-weight-bolder"
          >{{ this.currentUser.firstName }} {{ this.currentUser.lastName }}</span
        ><span class="user-status">{{ this.currentUser.role }}</span>
      </div>
      <span class="avatar"
        ><img
          class="round"
          src="assets/images/portrait/small/{{ this.currentUser.avatar }}"
          alt="avatar"
          height="40"
          width="40"/><span class="avatar-status-online"></span
      ></span>
    </ng-container>
  </a>
</li>
<!--/ User Dropdown -->
```

**Step 4** :- Implementing `AuthGuard`.
We have passed `AuthGuard` to `canActivate` property. If AuthGuard return true, navigation continues. If AuthGuard returns false, navigation is canceled.
_Example 1 :_
Here, the `sample` route is secured(any user who is **logged in** can access the sample page) by passing the `AuthGuard` to the `canActivate` property of the route.

```typescript{8}
import { AuthGuard } from 'app/auth/helpers'
import { Role } from 'app/auth/models'
const routes = [
  {
    path: 'sample',
    component: SampleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'home',
    component: HomeComponent
  }
]
```

Now we have successfully implemented the JWT authentication system on `SampleComponent`. If you try to click on Simple from the menu(navigation) It should redirect you to the login page.
Certainly, if you have any configuration regrading JWT then you might have to modify it a little bit. Please check our docs on [Role Based JWT Auth](/guide/development/role-based-jwt-auth.md).
