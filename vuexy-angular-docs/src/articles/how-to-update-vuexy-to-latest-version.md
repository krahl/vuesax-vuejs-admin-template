# How To Update Vuexy To Latest Version?

We are making template update as easy as possible from our side. We updated our folder structure so updating process can be easy than ever. 😍

There are two different ways to updating Vuexy Admin

1. [Fork our GitLab Repo](#_1-fork-our-gitlab-repo)
2. [Update manually](#_2-update-manually)

::: warning
`@core/` folder contains core files of the template which shall not get modified unless our support team guide you to do it. Apart from the `@core/` you can modify any files as per your requirement. By Replacing the `@core/` folder from the latest update you are able to get the core features update easily.
:::

## 1. Fork our GitLab Repo

Before starting your new project, join our [GitLab repo](/guide/getting-started/gitlab-access.html), fork it and build your app on top of that fork. This way, in the future, you can easily merge the changes from the main repo onto your fork. This will require merging a lot of changes manually, but it's the best way to keep the Vuexy updated.

Let's check how you can view each and every change in our template's latest release with ease.

## 2. Update manually

### Prerequisite

- [VS Code](https://code.visualstudio.com/download)
- VS Code Extension: [Compare Folders](https://marketplace.visualstudio.com/items?itemName=moshfeu.compare-folders)
- Vuexy Package you started your project with (Older version of vuexy package which you used)
- [Vuexy Latest Package](https://themeforest.net/downloads)

### Step 1

Install VS Code and install "Compare Folders" extension using above link.

### Step 2

unzip/extract/decompress Vuexy Latest Package and Vuexy Old package in your desired folder

### Step 3

Use compare folders extension and compare both packages for changes to adopt them in your project.
