# How to add i18n in starter-kit?

Vuexy Angular uses [ngx-translate](https://github.com/ngx-translate/core) internationalization (i18n) library for translation.

:::tip
Simple example using [ngx-translate](https://stackblitz.com/github/ngx-translate/example)
:::

Vuexy Angular admin provides built-in `CoreTranslationService` service integration in `starter-kit`. It is not required to integrate `ngx-translate` separately in it.

Just uncomment the below code from `starter-kit/src/app/layout/components/navbar/navbar.component.html` file. `starter-kit` provides example translation implementation in `SampleComponent` and `Menu`.

```html
<li ngbDropdown class="nav-item dropdown dropdown-language">
  <a class="nav-link dropdown-toggle" id="dropdown-flag" ngbDropdownToggle>
    <i class="flag-icon flag-icon-{{ languageOptions[_translateService.currentLang].flag }}"></i
    ><span class="selected-language">{{ languageOptions[_translateService.currentLang].title }}</span></a
  >
  <div ngbDropdownMenu aria-labelledby="dropdown-flag">
    <a *ngFor="let lang of _translateService.getLangs()" ngbDropdownItem (click)="setLanguage(lang)">
      <i class="flag-icon flag-icon-{{ languageOptions[lang].flag }}"></i> {{ languageOptions[lang].title }}
    </a>
  </div>
</li>
```

For more details refer [Translation (Multi Language)](/guide/development/translation.html).
