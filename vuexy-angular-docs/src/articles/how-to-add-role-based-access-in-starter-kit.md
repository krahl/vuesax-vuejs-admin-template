# How To Add Role-Based Access In Starter Kit?

Most of the applications required role-based user access, Vuexy Admin provides it built-in to use it easily. We've already implemented JWT with role-based access in `full-version` but not in `starter-kit`.
:::warning
Before implementing role-based access, it is required to implement JWT authentication first. Follow article [How to add JWT in starter-kit?](/articles/how-to-add-jwt-in-starter-kit.html)
:::
But, Vuexy Angular admin `starter-kit` has Auth folder `src/app/auth` which has JWT role-based access helpers, models & services which can be used easily.
:::warning IMPORTANT
Replace the `src/@core/core-menu` folder. Copy it folder from path : `full-version/src/@core/components/core-menu` & replace the folder that is on path : `starter-kit/src/@core/components/core-menu`. Starter-kit `core-menu/` folder do not include conditional functionality to display menu item based on role for that we need to copy it.
:::

### Setup role-based access on route

Now, let's implement role-based access on the route level to protect the routes based on user role.
In the below example We've set up `AuthGuard` on each route and added `data: { roles: [Role.Admin] }` to it to protect that route which can be accessed by admin user only.

```typescript{9}
import { AuthGuard } from 'app/auth/helpers'
import { Role } from 'app/auth/models'
const routes = [
  {
    path: 'sample',
    component: SampleComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] } // To set multiple role: [Role.Admin, Role.Client]
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  }
]
```

After protecting our routs based on role, it is required to add roles in `menu.ts` to display menu items based on role.
Added the `role: ['Admin']` property to `sample` page menu object in `menu.ts` file, by doing this **sample menu item** will only **visible** to the **admin user**.

### Setup menu item visibility

Open `starter-kit/src/app/menu/menu.ts` file and add the `role: ['Admin']` property to `Sample` page menu item object, by doing this **sample menu item** will only **visible** to the **admin user**.

Menu `role` property is used to display menu `type` i.e **items, collapsible & section** based on user role. For more details refer [Menu (navigation)](/guide/development/navigation-menus.html)

```typescript{19}
import { CoreMenu } from '@core/types'
export const menu: CoreMenu[] = [
  {
    id: 'home',
    title: 'Home',
    translate: 'MENU.HOME',
    type: 'item',
    icon: 'home',
    url: 'home'
  },
  {
    id: 'sample',
    title: 'Sample',
    translate: 'MENU.SAMPLE',
    type: 'item',
    icon: 'file',
    url: 'sample',
    role: ['Admin'] // To set multiple role: ['Admin', 'Client']
  }
]
```

## How to manage roles?

To manage roles, navigate to file `starter-kit/src/app/auth/models/role.ts`, here you can update roles. For more details refer [Role Based JWT Auth](/guide/development/role-based-jwt-auth.md).
