---
pageClass: page-article-list
---

# Articles

[How to add JWT in starter-kit?](/articles/how-to-add-jwt-in-starter-kit.md)

[How to make API call?](/articles/how-to-make-api-call.md)

[How can I use fullscreen layout?](/articles/how-can-i-have-fullscreen-layout.md)

[How To Add Role Based Access In Starter Kit?](/articles/how-to-add-role-based-access-in-starter-kit.md)

[How to provide role based access to component/element?](/articles/how-to-provide-role-based-access-to-component.md)

[How To Implement NodeJS Based Jwt Auth?](/articles/how-to-implement-node-based-jwt-auth.md)

[How to add i18n in starter-kit?](/articles/how-to-add-i18n-in-starter-kit.md)

[How To Update Vuexy To Latest Version?](/articles/how-to-update-vuexy-to-latest-version.md)
