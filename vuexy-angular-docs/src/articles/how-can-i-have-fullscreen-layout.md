# How can I use fullscreen layout?

Vuexy Angular default provide container layout in large screen by using bootstrap container class `.container-xxl`.

**Use fullscreen layout in single page.**

To create fullscreen layout in single page, wrap your `.content-body` with `.content-wrapper` class only.

```html
<!--? Add .container and .p-0 class to .content-wrapper div for boxed layout -->

<div class="content-wrapper">
  <div class="content-body">
    ...
  </div>
</div>
```

**Use fullscreen layout in whole application.**

To create fullscreen layout in whole application, find and replace following queries in `/src` folder.

1. Find: `content-area-wrapper container-xxl p-0` Replace with `content-area-wrapper`
2. Find: `content-wrapper container-xxl p-0` Replace with `content-wrapper`

Apart from this, It is also required to change following files changes.

In `vertical-layout.component.html` file remove the highlighted line `coreConfig.layout.navbar.type === 'floating-nav' ? 'container-xxl' : ''` as per below code.

```html {13}
<!-- NAVBAR -->
<ng-template #navbar>
  <app-navbar
    *ngIf="!coreConfig.layout.navbar.hidden"
    [ngClass]="[
      coreConfig.layout.navbar.customBackgroundColor === true
        ? coreConfig.layout.navbar.background +
          ' ' +
          coreConfig.layout.navbar.type +
          ' ' +
          coreConfig.layout.navbar.backgroundColor
        : coreConfig.layout.navbar.background + ' ' + coreConfig.layout.navbar.type,
      coreConfig.layout.navbar.type === 'floating-nav' ? 'container-xxl' : ''
    ]"
    class="header-navbar navbar navbar-expand-lg align-items-center navbar-shadow"
  >
  </app-navbar>
</ng-template>
<!--/ NAVBAR -->
```

In `horizontal-layout.component.html` file remove the highlighted line `coreConfig.layout.navbar.type === 'floating-nav' ? 'container-xxl' : ''` as per below code.

```html {15}
<!-- MENU -->
<ng-template #menu>
  <div class="horizontal-menu-wrapper">
    <!-- Horizontal menu: Visible above large screen only  -->
    <app-menu
      menuType="horizontal-menu"
      [ngClass]="[
        coreConfig.layout.navbar.customBackgroundColor === true
          ? coreConfig.layout.navbar.background +
            ' ' +
            coreConfig.layout.navbar.type +
            ' ' +
            coreConfig.layout.navbar.backgroundColor
          : coreConfig.layout.navbar.background + ' ' + coreConfig.layout.navbar.type,
        coreConfig.layout.navbar.type === 'floating-nav' ? 'container-xxl' : ''
      ]"
      class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-shadow menu-border d-none d-xl-block"
      *ngIf="!coreConfig.layout.menu.hidden"
    ></app-menu>
    <!-- Vertical overlay menu: Visible below large screen only  -->
    <core-sidebar
      name="menu"
      [collapsed]="coreConfig.layout.menu.collapsed"
      collapsibleSidebar="bs-gt-xl"
      *ngIf="!coreConfig.layout.menu.hidden"
      class="main-menu menu-fixed menu-light menu-accordio menu-shadow d-xl-none"
      overlayClass="sidenav-overlay"
      [ngClass]="[coreConfig.layout.menu.collapsed === true ? '' : 'expanded']"
    >
      <app-menu menuType="vertical-menu" class="vertical-menu"></app-menu>
    </core-sidebar>
  </div>
</ng-template>
<!--/ MENU -->
```

All done now 🎉 You can run the code and test the fullscreen layout.

We know this required little clumsy changes, we will be update this in future releases to customize easily.
