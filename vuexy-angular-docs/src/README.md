---
home: true
heroImage: /logo.svg
heroText: Vuexy - Angular Admin Dashboard Template
tagline: Best selling, Production Ready, Carefully Crafted, Extensive Angular Admin Template
actionText: Get Started →
actionLink: /guide/
footer: COPYRIGHT © 2021 Pixinvent, All rights Reserved
---

::: warning
Make sure you clear the browser [local storage](https://developer.chrome.com/docs/devtools/storage/localstorage/#delete) once you upgrade the template i.e from v6.5.0 to v6.6.0
:::

<div class="features">
  <div class="feature">
    <h2>Angular CLI + NgBootstrap</h2>
    <p>The Angular CLI makes it easy to create an application that already works, right out of the box.</p>
  </div>
  <div class="feature">
    <h2>AOT + Lazy Loading</h2>
    <p>Using AOT and Lazy Loading allow you to run your application with blazing fast speed.</p>
  </div>
  <div class="feature">
    <h2>Role based JWT Auth</h2>
    <p>Vuexy provides extendable JWT auth which can be configured easily with Role based access support.</p>
  </div>
</div>
