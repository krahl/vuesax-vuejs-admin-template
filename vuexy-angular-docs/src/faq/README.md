---
sidebar: 'auto'
---

# FAQ

::: tip
If you can't find your problem. Try searching related terms as well.
:::

::: tip I can't find my issue/problem/question listed here, What to do now?
If you can't find your issue/problem/question listed here then you can [raise support](/guide/getting-started/support.md) at our support portal. For **getting your solution quickly with minimum conversation**, please check out our getting support page which provides some tips on how to raise a perfect support ticket to get the solution quickly so our development team doesn't have to ask for things that take time and waste your precious development time.
:::

## Why I am getting npm install errors?

Causes of npm install or yarn install issue can be due to various things which include:

- Missing or inappropriate dependencies like node or some other environmental issues
- Dependency resolved by package manager (npm/yarn) conflict with other installed dependency
- The dependency of the package we use have an internal issue or that dependency has some issue with your environment
- Package or dependency of the package requires some additional step or configuration to work in your environment
- Downloaded package is broken or is got tampered

To resolve such installation issue:

- Please try again downloading fresh package/zip from ThemeForest and perform the installation again.
- Please make sure you are using the LTS version of the node which is recommended and not one with the latest features.
- run `npm cache clean`

After following about steps still, you are getting the error please [raise support](/guide/getting-started/support.md) at our support portal with the below details:

- Your OS information, Node version, npm/yarn version, Template/Package version
- Mention if you are able to run a fresh VueJS project using Vue CLI without our template
- Attache log file of the error you are getting in your console (Provide full log)
- Mention which command you are running
- Mention If you have any other machine, are you able to run our template on that machine

## Unable to resolve dependency tree errors

While installing node package, if you are getting error i.e Unable to resolve dependency tree errors then below solution might be helpful to you.

It seems that the older peer dependencies from your system causing this issue. This happens for some packages after updating to [npm 7](https://github.blog/2021-02-02-npm-7-is-now-generally-available/).
Paramter `--legacy-peer-deps` can help:

```bash
npm i --legacy-peer-deps
```

## FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed

Its probably because of a memory leak. Usually just increasing the memory allocated to Node will allow your program to run.

To increase the memory, in the terminal where you run your Node process:

```bash
# Linux/Other OS
export NODE_OPTIONS="--max-old-space-size=8192"

# Windows OS
set NODE_OPTIONS="--max-old-space-size=8192"
```

:::tip
Getting this every time? It's pain to use above code everytime while you run the app. Instead upgrade your node to LTS. It should fix this issue.

```bash
nvm install --lts
```

For more details [Install Node.js Locally with Node Version Manager (nvm)](https://heynode.com/tutorial/install-nodejs-locally-nvm/)

:::

## npm install warnings or installation warnings

You might get some warnings while running `npm install` or `yarn install` like below:

```bash
npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
npm, WARN deprecated chokidar@2.1.8: Chokidar 2 will break on node v14+. Upgrade to chokidar 3 with 15x less dependencies.
npm WARN deprecated har-validator@5.1.5: this library is no longer supported
npm WARN deprecated fsevents@1.2.13: fsevents 1 will break on node v14+ and could be using insecure binaries. Upgrade to fsevents 2.
npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
```

The warnings you are receiving while installing are from library/packages we used.

We always keep our packages up to date when we make major releases. However, the writer of that package may use an older dependency that is no longer maintained for backward compatibility or any other code-related issue. But, that's not an issue. Those packages will work fine with our template.

Even if you like to try you can install these packages in a fresh VueJS project without our template and you will get the same.

## How to update the template?

We are making template update as easy as possible from our side, so you can easily update it. Please read [Updating Vuexy](/articles/how-to-update-vuexy-to-latest-version.html)

## How to update App name, title, and logo?

You can update the app name and logo from `app-config.ts` file. Please update `app-config.ts` properties `app.appName`, `app.appTitle` and `app.appLogoImage` to update the text and logo. For more details refer [App Config](/guide/development/app-config.html)

## I have integrated toastr component in my project/starter-kit, why is display bottom-left behind the menu?

Either you have missed to include styles

```CSS
@import '@core/scss/angular/libs/toastr.component.scss';
```

**OR**

You may not add `encapsulation: ViewEncapsulation.None` in your ts file. For more details please check `toastr.component.ts`

## What is usage and purpose of core-card-snippet component?

This is custom component created by us to show code snippet (click code icon in right top corner). This component is just enhancement over card component for showing code, you don't have to use it.

**What to do with usage of this component in page?**

Just remove the `core-card-snippet` component from page and replace with `.card` to get bootstrap card, also need to remove the `CardSnippetModule` import from that specific module where you don't need.

Again, `core-card-snippet` is just for demo purpose, you can remove it. You can find more info from [here.](/guide/custom-components/core-card-snippet.html)

## Why are we using core-common-module?

`CoreCommonModule` has all the required modules that are needed frequently in the development of the application.

Here is the list of the modules that are imported in the core-common-module.

| Name                 | Imported                    |
| :------------------- | :-------------------------- |
| CommonModule         | @angular/common             |
| FlexLayoutModule     | @angular/flex-layout        |
| FormsModule          | @angular/forms              |
| ReactiveFormsModule  | @angular/forms              |
| CoreDirectivesModule | @core/directives/directives |
| CorePipesModule      | @core/pipes/pipes.module    |

`CoreDirectivesModule` imports all the directives which are used frequently for development, like `RippleEffect` & `FeatherIcon`.

`CorePipesModule` imports all the pipes which are used frequently for development, like `Initials`, `Filter`, `StripHtml`, `Safe`.
