# Introduction

Understanding template layouts will help you create page with your desired layout.

## Folder Structure

Let's understand the `layout/` folder structure first to understand the layouts and their components.

```
├── components                     -> Contains the layout components
│   ├── content                    -> Layout entry-point (router-outlet)
│   ├── content-header             -> Content header component (Includes Breadcrumb & Actions)
│   │   └── breadcrumb             -> Breadcrumb component
│   ├── footer                     -> Footer component
│   │   └── scroll-to-top          -> Scroll to top component to display scroll top button
│   ├── menu
│   │   ├── horizontal-menu        -> Horizontal menu
│   │   └── vertical-menu          -> Vertical menu
│   └── navbar                     -> Navbar component
│       ├── navbar-bookmark        -> Navbar's bookmark component
│       ├── navbar-cart            -> Navbar's cart component
│       ├── navbar-notification    -> Navbar's notification component
│       └── navbar-search          -> Navbar's search component
├── horizontal                     -> Horizontal Layout
├── vertical                       -> Vertical Layout
├── custom-breakpoints.ts          -> Flex layout break points overrides
└── layout.module.ts
```

### `layout/`

- `components/` folder contains the layout components i.e content, content-heasde, footer, navbar etc.. as explained above. For more details refer [Layout Components](/guide/layout/layout-components.html).
- `horizontal/` and `vertical/` folder contains the vertical and horizontal layout. For more details refer [Core Layout](/guide/layout/core-layout.html).
- `custom-breakpoints.ts` overrides flex layout break points.

::: tip
Vuexy Admin provides built-in [Layout Examples](/guide/layout/layout-components.html).
:::
