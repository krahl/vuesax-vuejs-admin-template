# Core Layout

Vuexy Angular has two core layouts `vertical` & `horizontal` which includes all the layouts components.

## Vertical layout

**Vertical layout** is one of the core layout which uses vertical-menu and layout components i.e navbar, content-header, footer etc..

<img :src="$withBase('/images/images/layouts/vertical-layout.png')" class="medium-zoom rounded bordered" alt="core-touchspin">

Open `layout/vertical/vertical-layout.component.html` file. It has pretty good comments to understand the usage of components and their template, so we will not get in to that too much.

## Horizontal layout

**Horizontal layout** is one of the core layout which uses horizontal-menu and layout components i.e navbar, content-header, footer etc..

<img :src="$withBase('/images/images/layouts/horizontal-layout.png')" class="medium-zoom rounded bordered" alt="core-touchspin">

Open `layout/horizontal/horizontal-layout.component.html` file. It has pretty good comments to understand the usage of components and their template, so we will not get in to that too much.

:::tip
Use [App Config](/guide/development/app-config.html) file to setup preferred menu type.
:::
