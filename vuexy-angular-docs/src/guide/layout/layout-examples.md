# Layout Examples

Vuexy Admin uses the `CoreConfigService` to set and override layout configs for specific page. To learn more on read App Config [Usage](/guide/development/app-config.html#usage) and [Page specific cutomization](/guide/development/app-config.html#page-specific-cutomization)

:::tip
Use [App Config](/guide/development/app-config.html#usage) setting to apply the layout configuration for entire application.
:::

## Blank Layout

Blank Layout is very useful while creating authentication and error type of pages where it do not use any layout component apart from content.

Vuexy Angular provides this layout example in `full-version/` in `ui/page-layouts/layout-blank/layout-blank.component.ts` To create blank layout, it is required to setup `CoreConfigService` configuration as shown in below code.

```ts

import { CoreConfigService } from '@core/services/config.service';
...

export class LayoutBlankComponent implements OnInit {

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   */
  constructor(private _coreConfigService: CoreConfigService) {

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        menu: {
          hidden: true
        },
        customizer: false,
        enableLocalStorage: false
      }
    };
  }
...

}
```

## Boxed Layout

A boxed layout means that the web page appears in a box in the browser, with space appearing around the box if your browser screen is sized larger than the box size.

Vuexy Angular provides this layout example in `full-version/` in `ui/page-layouts/boxed-layout/boxed-layout.component.html`. To create boxed layout add .container and .p-0 class to .content-wrapper div as shown in below example code.

```html
<div class="content-wrapper container p-0">
  <div class="content-body">
    <app-content-header [contentHeader]="contentHeader"></app-content-header>

    ...

    <app-ecommerce></app-ecommerce>
  </div>
</div>
```

## Collapsed Menu

Collapsed menu is not layout exactly, It's help-full if you want to have collapsed menu for specific page.

Vuexy Angular provides this layout example in `full-version/` in `ui/page-layouts/collapsed-menu/collapsed-menu.component.ts` To create collapsed layout, it is required to setup `CoreConfigService` configuration as shown in below code.

:::tip
Set [App Config](/guide/development/app-config.html#usage) menu collapsed value `true` to have collapsed menu for entire application.
:::

```ts

import { CoreConfigService } from '@core/services/config.service';
...

export class CollapsedMenuComponent implements OnInit {

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   */
  constructor(private _coreConfigService: CoreConfigService) {

  // Configure the layout
    this._coreConfigService.config = {
      layout: {
        menu: {
          collapsed: true
        },
        customizer: true,
        enableLocalStorage: false
      }
    };
  }
...

}
```

## Layout without Menu

Layout without Menu will hide the menu from the specific page. This layout will display navbar, content and footer.

Vuexy Angular provides this layout example in `full-version/` in `ui/page-layouts/without-menu/without-menu.component.ts` To create layout without menu, it is required to setup `CoreConfigService` configuration as shown in below code.

:::tip
Set [App Config](/guide/development/app-config.html#usage) menu `hidden` property value `true` to hide menu for entire application.
:::

```ts

import { CoreConfigService } from '@core/services/config.service';
...

export class WithoutMenuComponent implements OnInit {

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   */
  constructor(private _coreConfigService: CoreConfigService) {

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        menu: {
          hidden: true
        },
        customizer: true,
        enableLocalStorage: false
      }
    };
  }
...

}
```

## Layout without Navbar

Layout without Navbar will hide the Navbar from the specific page. This layout will display menu, content and footer.

To create layout without menu, it is required to setup `CoreConfigService` configuration as shown in below code.

:::tip
Set [App Config](/guide/development/app-config.html#usage) Navbar `hidden` property value `true` to hide menu for entire application.
:::

```ts

import { CoreConfigService } from '@core/services/config.service';
...

export class WithoutNavbarComponent implements OnInit {

  /**
   * Constructor
   *
   * @param {CoreConfigService} _coreConfigService
   */
  constructor(private _coreConfigService: CoreConfigService) {

    // Configure the layout
    this._coreConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        customizer: true,
        enableLocalStorage: false
      }
    };
  }
...

}
```
