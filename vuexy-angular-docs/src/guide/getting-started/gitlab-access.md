# GitLab Access

GitLab access will give you a direct access to the repository on gitlab.com. You have to have a GitLab account in order to use this feature.

Please fill form in the below link to get GitLab Access:

[Form Link](https://pixinvent.com/gitlab-access-provider-for-envato/)

::: warning
Only one GitLab user can get access using single purchase code.
:::

::: tip NOTE
GitLab repo have specific branch for development. For Vuexy v6.x there is v6.0 branch to check development updates.
:::
