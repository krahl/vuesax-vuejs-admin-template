# Starter-kit vs Full Package

## Starter-kit

Starter-kit is minimal template from where you can start your project quickly instead of removing stuff which isn't needed. Below is quick notes on starter-kit:

- No Auth
- No ACL
- No i18n
- Simple Navbar
- Four pages (Two sample pages + 404 + Login)
- No Customizer
- No Scroll to top

Use it if you don't want to clutter your project with extra libs which isn't required.

Don't worry about configuring third party libs which if you want to use them in starter-kit, our template is so flexible that all you have to do is just import lib file and provided third party package is ready to use 😍.

::: tip
Use starter-kit to provide reproduction steps if you raise any technical [support ticket](/guide/getting-started/support.md).
:::

## Full Package

This setup have all the things which you can see in live demo. Except that red Buy Now button 😅.

With this you have to remove things which you don't use or replace them with your content.

## Conclusion

According to us, starter-kit is easy to get started with minimal setup and our folder and code structure will help you adding libs support in starter-kit **more easily than ever**. Still if your project becomes easy with full package go for it.
