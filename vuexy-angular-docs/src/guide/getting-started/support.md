# Getting Support

::: warning
**Before submitting support ticket**

- Make sure that you are running the latest version of theme.
- Make sure that you deeply checked theme’s documentation and try to search for public tickets in case of someone else asked the same question before.

:::

In this page you will find how to get support and how to raise perfect support ticket to resolve your issues quickly with minimum conversation. We value your time 🕝

## Raising Support Ticket

Raising support ticket in support portal is as simple as commenting on post. However, If you want get your issues resolved quickly please follow below guide:

### Finding existing solution

There might be case that you issue is already resolved and you are the one who is facing due to some issue. So before creating new support ticket please search it in our [FAQ](/faq/) OR [Article](/articles/) page where we listed most common questions/issues asked in our support portal.

### Ticket Type

Add ticket type to your ticket title to let community understand what your ticket is about. This also helps to find related tickets using ticket type.

So, your ticket till will be like `[question] how to change theme?`

Some common ticket types are listed below:

- **question** - If you are asking question
- **installation** - If you have anything related to installation
- **auth** - If you have anything related to JWT or Auth
- **acl** - If you have anything related to Access Control
- **feat-req** - If you have any feature request for our template
- **bug** - If you find any bug in template
- **other** - if your ticket doesn't related to above mentioned ticket types

Good examples can be: 💯

- [question] How to change theme?
- [question] How to change i18n locale?
- [installation] Getting errors in npm run serve
- [auth] Unexpected behavior after login
- [other] Collapse component is not working as expected
- [acl] How to disable certain routes for some user
- [auth] I get redirected to login page even after login on refresh

### New Issue - New ticket

Following above suggestion, it will be good if you create another support ticket if you have any other question/issue which is not related to your current ticket.

Let's say you opened ticket for installation issue and want to ask about some component usage then it will be better to close installation ticket and open another ticket for that component to make support portal more accurate and easy find place.

### Template Version

Mentioning which version of our template you are using will help us to answer more accurately and will mitigate one question from our side.

Mentioning template version include if you are using Angular only, Angular + Laravel, Angular Starter-kit. Also, if you are using some other technology with our template then mention it will also help us in resolving issue quickly. [e.g. Vuexy Angular + JWT]

Good examples can be: 💯

- Angular
- Angular Starter kit
- Angular + JWT
- HTML + Laravel

### Sharing Code or Snippet

If you want to show us some code it will be better if you can format it properly and use code block so your pasted code get highlighted.

### Reproducing Issue/Bug - Starter-kit

If you have ticket where we need to have a look at your code like some component's certain feature is working in our live demo but not in your downloaded package then it will better you give us actual issue or reproduction steps.

Best solution for this is using our Starter-kit where we can test it without any dependency. This will also help you to clear your issue before sending it to us.

You can create zip and share with us using private link.

::: warning
Never upload our package or modified version of our package on public sites where others can view or download it.
:::

## Support Portal

Once you have purchase our template, You can get support at our [support portal](https://pixinvent.ticksy.com/) by raising support ticket.

If you have any template related question raise support ticket with our **Raising Support Ticket** guide and use **Ticket Template** to create **_perfect support ticket_**.

::: tip Don't want to waste time?
Creating perfect support ticket (Following our Guide and using ticket template) will increase the chances of getting issue resolved with minimum conversation and time.
:::

### Framework Support

To get the support for the framework, You can raise the issue at their [Github](https://github.com/ng-bootstrap/ng-bootstrap) repo.

Good candidates for framework support are:

1. How to use x component (x means any NgBootstrap Component)
2. x component not working as expected

### Third Party Package Support

Same as framework support, if you have any query regarding how to use any provided package other than their UI you can ask it in their respective **Github** repo issue section.

[Create Support Ticket](https://pixinvent.ticksy.com/)
