# Introduction

Vuexy – Angular Admin Dashboard Template – is the most developer friendly & highly customisable Admin Dashboard Template based on Bootstrap 4, (NgBootstrap). This is NOT just another admin template, it's an Angular app written entirely using Typescript with no jQuery dependency.

Vuexy Angular is perfect application for your next project with beginner level of Angular knowledge. Using Vuexy you can learn angular and craft Angular apps in no time.

Here you can find a list of core libraries, design specifications and coding standards that we use in Vuexy:

## Major Dependencies

- [Angular](https://angular.io/): Angular is an application design framework and development platform for creating efficient and sophisticated single-page apps.
- [Angular CLI](https://cli.angular.io/): The Angular CLI makes it easy to create an application that already works, right out of the box. It already follows our best practices!
- [NgBootStrap](https://ng-bootstrap.github.io/): Angular widgets built from the ground up using only Bootstrap 4 CSS with APIs designed for the Angular ecosystem.
- [Flex-layout](https://github.com/angular/flex-layout): Provides HTML UI layout for Angular applications; using Flexbox and a Responsive API

## Browser support

Vuexy is built to work best in the latest desktop and mobile and tablet browsers,

- Chrome (latest)
- Firefox (latest)
- Safari (latest)
- Opera (latest)
- Microsoft Edge
