# Welcome

Thanks for purchasing our template 🙂

Vuexy - Angular Admin Dashboard Template is NOT just another admin template, it's an Angular app written entirely using Typescript with no jQuery dependency.

Vuexy Angular is perfect application for your next project with beginner level of Angular knowledge. Using Vuexy you can learn angular and craft Angular apps in no time.

## How to use docs?

This document will help you to use Vuexy - Angular Admin Dashboard Template's custom Components, Directives, Services, Pipes, JWT Auth, Translation (i18n), Theme Config and many more...

- **Quick search:** Search your query and get the quick search result to save your time to find anything from the documentation. Press `/` key to search.
- **Navigation:** Left side navigation allows you to navigate easily to specific documentation page page.
  - **Getting started**
  - **Development**
  - **Layouts**
  - **Custom Components**
  - **Directives**
  - **Services**
- **FAQs:** We have created list of common [FAQs](/faq/) which developers ask while using our template.
- **Articles:** Includes How to, Migration and Update related helpful details in [Articles](/articles/).

::: tip
This doc doesn't cover NgBootstrap components documentation. Please refer NgBootstrap's official [documentation](https://ng-bootstrap.github.io/) for that.
:::

## Updating Vuexy

::: warning
Please keep track of which version of vuexy you are using in your project to easily update your project with latest template release.
:::

Please refer to this [article](/articles/how-to-update-vuexy-to-latest-version.md).

## Getting Support

If you have any question related to our template feel free to raise support ticket at our support portal. Please check our guide on [How to create perfect support ticket?](/guide/getting-started/support.md).
