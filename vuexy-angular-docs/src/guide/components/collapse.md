# Collapse

The Collapse component is slightly modified to make it more beautiful. Let's have a glance.

Use class `.collapse-default` as a wrap it with `.collapse-icon` to get default collapse component styles.

```html
<div class="collapse-icon">
  <div class="collapse-default">
    <div class="card">
      <div
        (click)="isCollapsed1 = !isCollapsed1"
        [attr.aria-expanded]="!isCollapsed1"
        aria-controls="collapseExample"
        class="card-header collapse-title"
        data-toggle="collapse"
      >
        Collapse Item 1
      </div>
      <div [ngbCollapse]="isCollapsed1">
        <div class="card">
          <div class="card-body">
            Pie dragée muffin. Donut cake liquorice marzipan carrot cake topping powder candy. Sugar plum brownie
            brownie cotton candy. Tootsie roll cotton candy pudding bonbon chocolate cake lemon drops candy. Jelly
            marshmallow chocolate cake carrot cake bear claw ice cream chocolate. Fruitcake apple pie pudding jelly
            beans pie candy canes candy canes jelly-o. Tiramisu brownie gummi bears soufflé dessert cake.
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div
        (click)="isCollapsed2 = !isCollapsed2"
        [attr.aria-expanded]="!isCollapsed2"
        aria-controls="collapseExample1"
        class="card-header collapse-title"
        data-toggle="collapse"
      >
        Collapse Item 2
      </div>
      <div [ngbCollapse]="isCollapsed2">
        <div class="card">
          <div class="card-body">
            Jelly-o brownie marshmallow soufflé I love jelly beans oat cake. I love gummies chocolate bar marshmallow
            sugar plum. Pudding carrot cake sweet roll marzipan I love jujubes. Sweet roll tart sugar plum halvah donut.
            Cake gingerbread tart. Tootsie roll soufflé danish powder marshmallow sugar plum halvah sweet chocolate bar.
            Jujubes cupcake I love toffee biscuit.
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div
        (click)="isCollapsed3 = !isCollapsed3"
        [attr.aria-expanded]="!isCollapsed3"
        aria-controls="collapseExample2"
        class="card-header collapse-title"
        data-toggle="collapse"
      >
        Collapse Item 3
      </div>
      <div [ngbCollapse]="isCollapsed3">
        <div class="card">
          <div class="card-body">
            Pudding lollipop dessert chocolate gingerbread. Cake cupcake bonbon cupcake marshmallow. Gummi bears carrot
            cake bonbon cake. Sweet roll fruitcake bear claw soufflé. Apple pie ice cream liquorice sesame snaps
            brownie. Donut marshmallow donut pudding chupa chups.
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div
        (click)="isCollapsed4 = !isCollapsed4"
        [attr.aria-expanded]="!isCollapsed4"
        aria-controls="collapseExample3"
        class="card-header collapse-title"
        data-toggle="collapse"
      >
        Collapse Item 4
      </div>
      <div id="collapseExample3" [ngbCollapse]="isCollapsed4">
        <div class="card">
          <div class="card-body">
            Brownie sweet carrot cake dragée caramels fruitcake. Gummi bears tootsie roll croissant gingerbread dragée
            tootsie roll. Cookie caramels tootsie roll pie. Sesame snaps cookie cake donut wafer. Wafer cookie jelly-o
            candy muffin cake. Marzipan topping lollipop. Gummies chocolate sugar plum.
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
```

```typescript
  public isCollapsed1 = true;
  public isCollapsed2 = true;
  public isCollapsed3 = false;
  public isCollapsed4 = true;
```

_Result:_

<img :src="$withBase('/images/images/collapse/collapse.gif')" alt="collapse" class="medium-zoom rounded bordered">

You can check demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/components/collapse) page.
