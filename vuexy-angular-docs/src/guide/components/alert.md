# Alert

The Alert component is slightly modified to make it more beautiful.

::: tip
We have used [alert component](https://ng-bootstrap.github.io/#/components/alert/examples) of ng bootstrap.
:::

## Alert Structure

We have added `.alert-heading` class for heading and `.alert-body` for body-content. Also we have added `.alert-link` class for link inside alert.

```html
<ngb-alert [type]="'warning'" [dismissible]="false">
  <h4 class="alert-heading">Lorem ipsum dolor sit amet</h4>
  <div class="alert-body">
    Lorem ipsum dolor sit amet
    <a href="javascript:void(0)" class="alert-link">
      consectetur
    </a>
  </div>
</ngb-alert>
```

_Result:_

<img :src="$withBase('/images/images/alert/alert.jpg')" alt="alert-structure" class="medium-zoom rounded bordered">

## Alert Colors

To change the color of alert, use `[type]="{colorName}"` along with `ngb-alert` compoent.

```html
<ngb-alert [type]="'primary'" [dismissible]="false">
  <h4 class="alert-heading">Primary</h4>
  <div class="alert-body">
    Tootsie roll lollipop lollipop icing. Wafer cookie danish macaroon. Liquorice fruitcake apple pie I love cupcake
    cupcake.
  </div>
</ngb-alert>

<ngb-alert [type]="'secondary'" [dismissible]="false">
  <h4 class="alert-heading">Secondary</h4>
  <div class="alert-body">
    Tootsie roll lollipop lollipop icing. Wafer cookie danish macaroon. Liquorice fruitcake apple pie I love cupcake
    cupcake.
  </div>
</ngb-alert>

<ngb-alert [type]="'success'" [dismissible]="false">
  <h4 class="alert-heading">Success</h4>
  <div class="alert-body">
    Tootsie roll lollipop lollipop icing. Wafer cookie danish macaroon. Liquorice fruitcake apple pie I love cupcake
    cupcake.
  </div>
</ngb-alert>

<ngb-alert [type]="'danger'" [dismissible]="false">
  <h4 class="alert-heading">Danger</h4>
  <div class="alert-body">
    Tootsie roll lollipop lollipop icing. Wafer cookie danish macaroon. Liquorice fruitcake apple pie I love cupcake
    cupcake.
  </div>
</ngb-alert>

<ngb-alert [type]="'warning'" [dismissible]="false">
  <h4 class="alert-heading">Warning</h4>
  <div class="alert-body">
    Tootsie roll lollipop lollipop icing. Wafer cookie danish macaroon. Liquorice fruitcake apple pie I love cupcake
    cupcake.
  </div>
</ngb-alert>

<ngb-alert [type]="'info'" [dismissible]="false">
  <h4 class="alert-heading">Info</h4>
  <div class="alert-body">
    Tootsie roll lollipop lollipop icing. Wafer cookie danish macaroon. Liquorice fruitcake apple pie I love cupcake
    cupcake.
  </div>
</ngb-alert>

<ngb-alert [type]="'dark'" [dismissible]="false">
  <h4 class="alert-heading">Dark</h4>
  <div class="alert-body">
    Tootsie roll lollipop lollipop icing. Wafer cookie danish macaroon. Liquorice fruitcake apple pie I love cupcake
    cupcake.
  </div>
</ngb-alert>
```

| Property | Class                  | Description                                                                                         |
| :------- | :--------------------- | :-------------------------------------------------------------------------------------------------- |
| Type     | `[type]="{colorName}"` | To change the alert color. `colorName` : `primary / secondary / success / danger / warning / info`. |

_Result:_

<img :src="$withBase('/images/images/alert/alert-colors.jpg')" alt="alert-structure" class="medium-zoom rounded bordered">

You can check the demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/components/alerts) page.
