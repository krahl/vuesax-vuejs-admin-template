# Modal

The Modal component is slightly modified to make it more beautiful. Let's have a glance.

## Modal Themes

Wrap modal with class `modal-{colorName}"` to change theme of modal.

```html
<div class="modal-primary d-inline-block">
  <!-- Button trigger modal -->
  <button type="button" class="btn btn-outline-primary" (click)="modalOpenPrimary(modalPrimary)" rippleEffect>
    Primary
  </button>

  <!-- Modal -->
  <ng-template #modalPrimary let-modal>
    <div class="modal-header">
      <h5 class="modal-title" id="myModalLabel160">Primary Modal</h5>
      <button type="button" class="close" (click)="modal.dismiss('Cross click')" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body" tabindex="0" ngbAutofocus>
      Tart lemon drops macaroon oat cake chocolate toffee chocolate bar icing. Pudding jelly beans carrot cake pastry
      gummies cheesecake lollipop. I love cookie lollipop cake I love sweet gummi bears cupcake dessert.
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" (click)="modal.close('Accept click')" rippleEffect>
        Accept
      </button>
    </div>
  </ng-template>
  <!-- / Modal -->
</div>
```

```typescript
modalOpenPrimary(modalPrimary) {
  this.modalService.open(modalPrimary, {
    centered: true,
    windowClass: 'modal modal-primary'
  });
}
```

| Property    | Class                      | Description                                                                                         |
| :---------- | :------------------------- | :-------------------------------------------------------------------------------------------------- |
| windowClass | `.modal.modal-{colorName}` | To change the modal color. `colorName` : `primary / secondary / success / danger / warning / info`. |

_Result :_

<img :src="$withBase('/images/images/modal/primary.jpg')" alt="button-outline" class="medium-zoom rounded bordered">

You can check demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/components/modals) page.
