# Rating

The Rating component is slightly modified to make it more beautiful. Let's have a glance.

## Basic

Wrap `ngb-rating` with class `.rating` to apply custom styles.

```html
<div class="rating">
  <ngb-rating [(rate)]="basicCurrentRate" class="outline-none"></ngb-rating>
</div>
```

_Result :_

<img :src="$withBase('/images/images/rating/basic.jpg')" alt="button-outline" class="medium-zoom rounded bordered">

## Custom Icons

```html
<div class="rating">
  <ngb-rating [(rate)]="iconsCurrentRate">
    <ng-template let-fill="fill" let-index="index"
      ><span class="fa fa-star-o" [class.fa-star]="fill === 100"></span
    ></ng-template>
  </ngb-rating>
</div>
```

_Result :_

<img :src="$withBase('/images/images/rating/icons.jpg')" alt="button-outline" class="medium-zoom rounded bordered">

## Sizes

```html
<div class="rating rating-sm">
  <ngb-rating [(rate)]="sizeSMCurrentRate" class="outline-none"></ngb-rating>
</div>

<div class="rating">
  <ngb-rating [(rate)]="sizeCurrentRate" class="outline-none"></ngb-rating>
</div>

<div class="rating rating-lg">
  <ngb-rating [(rate)]="sizeLGCurrentRate" class="outline-none"></ngb-rating>
</div>
```

_Result:_

<img :src="$withBase('/images/images/rating/sizes.jpg')" alt="button-outline" class="medium-zoom rounded bordered">

You can check demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/components/ratings) page.
