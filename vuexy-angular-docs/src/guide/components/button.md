# Button

The Button component is slightly modified to make it more beautiful. Let's have a glance.

## Button with Ripple Effect

Use a prop `rippleEffect` to quickly create a ripple effect on click.

```html
<button type="button" class="btn btn-primary" rippleEffect>Primary</button>
```

_Result:_

<img :src="$withBase('/images/images/button/button.gif')" alt="button-outline" class="medium-zoom rounded bordered">

You can check demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/components/buttons) page.
