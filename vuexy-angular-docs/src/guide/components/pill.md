# Pill

The Pill component is slightly modified to make it more beautiful. Let's have a glance.

Use class `.nav.nav-pills` with `[ngbNav]` attribute to apply pill style.

```html
<ul ngbNav #nav="ngbNav" class="nav nav-pills">
  <li ngbNavItem>
    <a ngbNavLink>Home</a>
    <ng-template ngbNavContent>
      <p>
        Pastry gummi bears sweet roll candy canes topping ice cream. Candy canes fruitcake cookie carrot cake pastry.
        Lollipop caramels sesame snaps pie tootsie roll macaroon dessert. Muffin jujubes brownie dragée ice cream
        cheesecake icing. Danish brownie pastry cotton candy donut. Cheesecake donut candy canes. Jelly beans croissant
        bonbon cookie toffee. Soufflé croissant lemon drops tootsie roll toffee tiramisu.
      </p>
    </ng-template>
  </li>
  <li ngbNavItem>
    <a ngbNavLink>Profile</a>
    <ng-template ngbNavContent>
      <p>
        Pudding candy canes sugar plum cookie chocolate cake powder croissant. Carrot cake tiramisu danish candy cake
        muffin croissant tart dessert. Tiramisu caramels candy canes chocolate cake sweet roll liquorice icing
        cupcake.Bear claw chocolate chocolate cake jelly-o pudding lemon drops sweet roll sweet candy. Chocolate sweet
        chocolate bar candy chocolate bar chupa chups gummi bears lemon drops.
      </p>
    </ng-template>
  </li>
</ul>
```

_Result:_

<img :src="$withBase('/images/images/pill/pill.gif')" alt="pill" class="medium-zoom rounded bordered">

You can check demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/components/pills) page.
