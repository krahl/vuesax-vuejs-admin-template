# Config

Config service will get initialized whenever the application gets rendered.

Config service helps to load all the required configurations such as layout, app, etc.

```typescript
export const coreConfig: CoreConfig = {
  app: {
    appName: 'Vuexy', // App Name
    appTitle: 'Vuexy - Angular 11+ Bootstrap Admin Template', // App Title
    appLogoImage: 'assets/images/logo/logo.svg' // App Logo
  },
  layout: {
    skin: 'default', // default, dark, bordered, semi-dark
    type: 'vertical', // vertical, horizontal
    menu: {
      hidden: false, // Boolean: true, false
      collapsed: false // Boolean: true, false
    },
    // ? For horizontal menu, navbar type will work for navMenu type
    navbar: {
      hidden: false, // Boolean: true, false
      type: 'floating-nav', // navbar-static-top, fixed-top, floating-nav, d-none
      background: 'navbar-light', // navbar-light. navbar-dark
      customBackgroundColor: true, // Boolean: true, false
      backgroundColor: '' // BS color i.e bg-primary, bg-success
    },
    footer: {
      hidden: false, // Boolean: true, false
      type: 'footer-static', // footer-static, footer-sticky, d-none
      background: 'footer-light', // footer-light. footer-dark
      customBackgroundColor: false, // Boolean: true, false
      backgroundColor: '' // BS color i.e bg-primary, bg-success
    },
    enableLocalStorage: true,
    customizer: true, // Boolean: true, false (Enable theme customizer)
    scrollTop: true, // Boolean: true, false (Enable scroll to top button)
    buyNow: true // Boolean: true, false (Set false in real project, For demo purpose only)
  }
}
```

We have default configuration on file `app-config.ts`

Path : `root\src\app\app-config.ts`

:::tip
You can modify this file as per your requirement.
:::
