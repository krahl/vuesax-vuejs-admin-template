# Translation

Translate service is used to translate words, phrases, web pages, etc.

:::tip
We have used **@ngx-translate** to translate content. For details, you can visit the official page from [here](https://github.com/ngx-translate/core).  
You can translate content from navbars translate dropdown.
:::

## Requirements

To use Translate service, we need to `import` the `TranslateModule` to that specific module where we want translate functionality.

```typescript
import { TranslateModule } from '@ngx-translate/core';
...
...
@NgModule({
declarations: [
...
],
imports: [
 ...
 TranslateModule
],
providers: []
})
```

## Usage

Use pipe `translate` with template variable to translate that specific variable content.

_Example :_

```html
<div class="card-localization border rounded mt-3 p-2">
  <h5 class="mb-1">{{ 'CARD.TITLE' | translate }}</h5>
  <div class="card-text">{{ 'CARD.TEXT' | translate }}</div>
</div>
```

You can check the demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/extensions/i18n) page.
