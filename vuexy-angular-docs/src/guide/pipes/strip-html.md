# Strip HTML

We have created Strip HTML pipe to strip out HTML tags and only render content. Lets have a look on it.

## Requirements

:::tip
You can import `CorePipesModule` directly or import our `CoreCommonModule` which has all the required modules.
:::

To use Strip HTML pipe, we need to `import` the `CorePipesModule` to that specific module where we want filter functionality.

Example :

```typescript
import { CorePipesModule } from '@core/pipes/pipes.module';
...
...
@NgModule({
declarations: [
...
],
imports: [
 ...
 CorePipesModule
],
providers: []
})
```

## Usage

Use pipe `striphtml` with html content to only get content without html code.

_Example :_

```html
<p class="text-truncate mb-0">{{ email.message | striphtml }}</p>
```

You can check the demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/apps/email/inbox) page.
