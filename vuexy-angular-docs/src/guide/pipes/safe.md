# Safe

We have created a safe pipe implementation to bypass DomSanitizer stripping out content. Lets have a look on it.

## Requirements

:::tip
You can import `CorePipesModule` directly or import our `CoreCommonModule` which has all the required modules.
:::

To use safe pipe, we need to `import` the `CorePipesModule` to that specific module where we want filter functionality.

```typescript
import { CorePipesModule } from '@core/pipes/pipes.module';
...
...
@NgModule({
declarations: [
...
],
imports: [
 ...
 CorePipesModule
],
providers: []
})
```

## Usage

Use pipe `safe` with `src` attribute to bypass DomSanitizer stripping out content.

_Example :_

```html
<div *ngIf="postRef.postVid">
  <iframe [src]="postRef.postVid | safe: 'resourceUrl'" class="w-100 rounded border-0 height-250 mb-50"></iframe>
</div>
```

Here `resourceUrl` is used to Sanitize `bypassSecurityTrustResourceUrl`.

## API

| Name        | type          | Description                          |
| :---------- | :------------ | :----------------------------------- |
| Html        | `html`        | To bypass Security Trust Html        |
| Style       | `style`       | To bypass Security Trust Style       |
| Script      | `script`      | To bypass Security Trust Script      |
| Url         | `url`         | To bypass Security Trust Url         |
| Resourceurl | `resourceUrl` | To bypass Security Trust ResourceUrl |

You can check the demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/pages/profile) page.
