# Initials

We have created an initials pipe to get the first letter of two words( ie. full name). Lets have a look on it.

## Requirements

:::tip
You can import `CorePipesModule` directly or import our `CoreCommonModule` which has all the required modules.
:::

To use initials pipe, we need to `import` the `CorePipesModule` to that specific module where we want filter functionality.

```typescript
import { CorePipesModule } from '@core/pipes/pipes.module';
...
...
@NgModule({
declarations: [
...
],
imports: [
 ...
 CorePipesModule
],
providers: []
})
```

## Usage

Use pipe `initials` with template variables to get initials.

_Example :_

```html
<div class="avatar">
  <div class="avatar-content">{{ name | initials }}</div>
</div>
```

<img :src="$withBase('/images/images/initials-pipe/initials-pipe.jpg')" alt="button-outline" class="medium-zoom rounded bordered">

Here `name` is a template variable where initials will get applied.

You can check the demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/apps/invoice/list) page.
