# Filter

We have created a filter pipe to make the filter functionality easy to implement. Lets have a look on it.

## Requirements

:::tip
You can import `CorePipesModule` directly or import our `CoreCommonModule` which has all the required modules.
:::

To use filter pipe, we need to `import` the `CorePipesModule` to that specific module where we want filter functionality.

```typescript
import { CorePipesModule } from '@core/pipes/pipes.module';
...
...
@NgModule({
declarations: [
...
],
imports: [
 ...
 CorePipesModule
],
providers: []
})
```

## Usage

Use pipe `filter` with `*ngFor` structural directive to implement filter functionality.

_Example :_

```html
<input
  type="text"
  class="form-control"
  [(ngModel)]="searchText"
  id="searchbar"
  name="searchbar"
  placeholder="Ask a question..."
/>

<div
  *ngFor="let knowledgeBaseRef of data.knowledgeBase | filter: searchText:'title'"
  class="col-md-4 col-sm-6 col-12 kb-search-content"
>
  <div class="card">
    <a routerLink="/pages/knowledge-base/{{ knowledgeBaseRef.category }}">
      <img src="{{ knowledgeBaseRef.img }}" class="card-img-top" alt="knowledge-base-image" />
      <div class="card-body text-center">
        <h4>{{ knowledgeBaseRef.title }}</h4>
        <p class="text-body mt-1 mb-0">{{ knowledgeBaseRef.desc }}</p>
      </div>
    </a>
  </div>
</div>
```

Here `searchText` is a variable where we get the updated input text. `'title'` is what we are filtering.

You can check demo in navbar on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/pages/knowledge-base) page.
