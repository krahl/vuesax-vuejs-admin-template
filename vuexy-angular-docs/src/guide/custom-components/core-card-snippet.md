# Core card Snippet

To use `core-card-snippet` component, import `CardSnippetModule` to the respective `NgModule`.

## Requirements

```typescript
import { CardSnippetModule } from ''@core/components/card-snippet/card-snippet.module';
...
...
@NgModule({
  declarations: [
  ...
  ],
  imports: [
    ...
    CardSnippetModule
  ],
  providers: []
})
```

## Usage

_Example :_

```html
<core-card-snippet [snippetCode]="customVariable">
  <span class="card-snippet-title">Title</span>
  <div class="card-snippet-body">
    Body
  </div>
</core-card-snippet>
```

```typescript
import { snippetCode } from '@core/components/card-snippet/card-snippet.component';
...
public customVariable: snippetCode = {
  html: `
  <p> Your HTML code here...</p>
  `,
  ts:`
  your typescript code here...
  `,
  scss:`
  your scss here...
  `,
  json:`
  Json code here...
  `
};
```

<img :src="$withBase('/images/images/core-card-snippet/core-card-snippet.gif')" class="img-fluid" alt="core card snippet">

## API

### Component

| Name                     | Selector            | Input                           | Description     |
| :----------------------- | :------------------ | :------------------------------ | :-------------- |
| CoreCardSnippetComponent | `core-card-snippet` | `snippetCode` Type: snippetCode | Create \*\*HTML | TS | SCSS | JSON\*\* code snippets with tabs by passing `{ html | ts | scss | json }` as key value pair to the `snippetCode` input Property. |

### Property

| Property | Class                 | Description                                      |
| :------- | :-------------------- | :----------------------------------------------- |
| Title    | `.card-snippet-title` | To add Title to the core-card-snippet component. |
| Body     | `.card-snippet-body`  | To add Body to the core-card-snippet component.  |

You can check the demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/components/alerts) page.
