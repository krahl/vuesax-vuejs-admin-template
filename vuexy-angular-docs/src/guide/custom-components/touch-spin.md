# Core Touchspin

To use `core-touchspin` component, import `CoreTouchspinModule` to the respective `NgModule`.

## Requirements

```typescript
import { CoreTouchspinModule } from '@core/components/core-touchspin/core-touchspin.module';
...
...
@NgModule({
declarations: [
...
],
imports: [
...
CoreTouchspinModule
],
providers: []
})
```

## Usage

_Example :_

```html
<core-touchspin [numberValue]="50" (onChange)="countChange($event)"></core-touchspin>
```

_Get value on change_

```ts
  /***
   * On touchspin count change
   */
  countChange(value) {
    // this.dateValue = value;
    console.log(value);
  }
```

### Core Touchspin (Default)

  <img :src="$withBase('/images/images/core-touchspin/core-touchspin.gif')" class="img-fluid mt-1" alt="core-touchspin">

## API

### Component

| Name                   | Selector         | Description                                                                                    |
| :--------------------- | :--------------- | :--------------------------------------------------------------------------------------------- |
| CoreTouchspinComponent | `core-touchspin` | Create basic touchspin component by using `<core-touchspin>` selector element. Default is `0`. |

### Property

| Name        | Type      | Description                                                                                            |
| :---------- | :-------- | :----------------------------------------------------------------------------------------------------- |
| numberValue | `number`  | To show the initial default value.                                                                     |
| onChange    | `number`  | To get the value.                                                                                      |
| iconChevron | `boolean` | To use chevron icons. Default icons are `+` & `-`.                                                     |
| disable     | `boolean` | To disable touchspin. Default is `false`                                                               |
| size        | `string`  | To make size small or large use `'sm'` or `'lg'`. Default is medium.                                   |
| color       | `string`  | To change the background color of buttons. Use `[color]="'colorName'"`                                 |
| stepValue   | `number`  | To set incremental & decremental steps. Eg. set `[stepValue]="5"` to increment or decrement by 5 step. |
| maxValue    | `number`  | To set max value for touchspin input number.                                                           |
| minValue    | `number`  | To set min value for touchspin input number.                                                           |

You can check the demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/forms/form-elements/number-input) page.
