# Ripple Effect

We have created a Custom Directive to add a ripple Effect for buttons. Lets have a look on it.

:::tip
We can use ripple Effect on all of the variants of button. you just need to add `rippleEffect` attribute.
:::

## Requirements

:::tip
You can import `CoreDirectivesModule` directly or import our `CoreCommonModule` which has all the required modules.
:::

To use ripple effect, we need to `import` the `CoreDirectivesModule` to that specific module.

Example :

```typescript
import { CoreDirectivesModule } from '@core/directives/directives';
...
...
@NgModule({
declarations: [
...
],
imports: [
 ...
 CoreDirectivesModule
],
providers: []
})
```

## Usage

Use attribute `rippleEffect` with `button` or `a` tag to get ripple Effect on click.

Example :

```html
<button class="btn btn-outline-primary" type="button" rippleEffect></button>
```

_Result:_

<img :src="$withBase('/images/images/button/button.gif')" alt="button-outline" class="medium-zoom rounded bordered">

You can check the demo on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/components/buttons) page.
