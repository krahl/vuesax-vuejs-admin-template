# Feather Icon

We have created a Custom Directive to render feather SVG icons. Let's have a look on it.

:::warning
Feather SVG icons won't render conditionally. In that case if you want to render icons conditionally you can use Feather font icons. Example : Use class `.feather.icon-{icon-name}`. Example : `<i class="feather icon-copy font-medium-2"></i>`
:::

:::tip INFO
You can check all available icons on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/ui/icons/feather) page.
:::

## Requirements

:::tip
You can import `CoreDirectivesModule` directly or import our `CoreCommonModule` which has all the required modules.
:::

To use the feather SVG icons we need to `import` the `CoreDirectivesModule` to that specific module where we want feather SVG icons.

```typescript
import { CoreDirectivesModule } from '@core/directives/directives';
...
...
@NgModule({
declarations: [
...
],
imports: [
 ...
 CoreDirectivesModule
],
providers: []
})
```

## Usage

Use attribute `data-feather="icon-name"` to render SVG icon.

_Example :_

```html
<i data-feather="bell"></i>
```

## Size

Use prop `[size]="size-in-px"` to change SVG icon size.

:::tip
Default size is `14`px.
:::

_Example :_

```html
<i data-feather="bell" [size]="22"></i>
```

<img :src="$withBase('/images/images/feather-icon/icon-size.jpg')" class="img-fluid mt-1" alt="feather">

## Class

Use prop `[class]="'your-class-name'"` to add class to SVG icon.

_Example :_

```html
<i data-feather="bell" [class]="'text-warning'"></i>
```

<img :src="$withBase('/images/images/feather-icon/icon-class.jpg')" class="img-fluid mt-1" alt="feather">

## Inner

Use prop `[inner]="true/false"` to toggle between innerHTML or outerHTML.

:::tip
Default inner prop is `false`. So, It will render as outerHTML.
:::

_Example :_

```html
<i data-feather="bell" [inner]="true"></i>
```

## API

| Property | Class     | Description                                                             |
| :------- | :-------- | :---------------------------------------------------------------------- |
| name     | `[name]`  | Name of the icon to render.                                             |
| class    | `[class]` | Class name to add.                                                      |
| size     | `[size]`  | Icon size.                                                              |
| inner    | `[inner]` | If `true` it will render as innerHTML else it will render as outerHTML. |
