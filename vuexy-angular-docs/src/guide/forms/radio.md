# Radio

The Radio component is slightly modified to make it more beautiful

## Custom Radios

Wrap input with class `.custom-control.custom-radio` .

```html
<div class="custom-control custom-radio">
  <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input" checked />
  <label class="custom-control-label" for="customRadio1">Checked</label>
</div>
```

_Result:_

<img :src="$withBase('/images/images/radio/radio.jpg')" alt="checkbox-colors" class="medium-zoom rounded bordered">

## Colored Checkboxes

Wrap input with class `.custom-control.custom-radio` & `.custom-control-{colorName}` .

```html
<div class="custom-control custom-control-primary custom-radio">
  <input type="radio" id="customColorRadio1" name="customColorRadio1" class="custom-control-input" checked />
  <label class="custom-control-label" for="customColorRadio1">Primary</label>
</div>

<div class="custom-control custom-control-secondary custom-radio">
  <input type="radio" id="customColorRadio2" name="customColorRadio2" class="custom-control-input" checked />
  <label class="custom-control-label" for="customColorRadio2">Secondary</label>
</div>

<div class="custom-control custom-control-success custom-radio">
  <input type="radio" id="customColorRadio3" name="customColorRadio3" class="custom-control-input" checked />
  <label class="custom-control-label" for="customColorRadio3">Success</label>
</div>

<div class="custom-control custom-control-danger custom-radio">
  <input type="radio" id="customColorRadio5" name="customColorRadio5" class="custom-control-input" checked />
  <label class="custom-control-label" for="customColorRadio5">Danger</label>
</div>

<div class="custom-control custom-control-warning custom-radio">
  <input type="radio" id="customColorRadio4" name="customColorRadio4" class="custom-control-input" checked />
  <label class="custom-control-label" for="customColorRadio4">Warning</label>
</div>

<div class="custom-control custom-control-info custom-radio">
  <input type="radio" id="customRadio6" name="customColorRadio6" class="custom-control-input" checked />
  <label class="custom-control-label" for="customRadio6">Info</label>
</div>
```

_Result:_

<img :src="$withBase('/images/images/radio/radio-theme.jpg')" alt="checkbox-colors" class="medium-zoom rounded bordered">

| Class                         | Value                                                   | Description                        |
| :---------------------------- | :------------------------------------------------------ | :--------------------------------- |
| `.custom-control-{colorName}` | primary / secondary / success / danger / warning / info | To get the desired color of radio. |
