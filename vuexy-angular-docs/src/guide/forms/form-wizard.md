# Form Wizard

The bs-stepper component is slightly modified to make it more beautiful. Let's have a glance.

:::tip
For more information on bs-stepper you can visit official page from [here](https://github.com/Johann-S/bs-stepper)
:::

## Basic Wizard

We have modified the look of the wizard as per the template theme.

_Result:_

<img :src="$withBase('/images/images/form-wizard/basic.jpg')" alt="checkbox-colors" class="medium-zoom rounded bordered">

## Vertical Wizard

Add class `.vertical` along with class `.bs-stepper` to get vertical Wizard.

_Result:_

<img :src="$withBase('/images/images/form-wizard/vertical-basic.jpg')" alt="checkbox-colors" class="medium-zoom rounded bordered">

## Horizontal Modern wizard

Add class `.wizard-modern` along with class `.bs-stepper` to get vertical Wizard.

_Result:_

<img :src="$withBase('/images/images/form-wizard/modern-horizontal.jpg')" alt="checkbox-colors" class="medium-zoom rounded bordered">

## Vertical Modern Wizard

Add class `.vertical.wizard-modern` along with class `.bs-stepper` to get vertical Wizard.

_Result:_

<img :src="$withBase('/images/images/form-wizard/modern-vertical.jpg')" alt="checkbox-colors" class="medium-zoom rounded bordered">

## Usage

To use bs-stepper we need to initialize the Stepper.

```html
<div id="stepper1" class="bs-stepper horizontal-wizard-example">
  <div class="bs-stepper-header">
    <div class="step" data-target="#account-details">
      <button class="step-trigger">
        <span class="bs-stepper-box">1</span
        ><span class="bs-stepper-label"
          ><span class="bs-stepper-title">Account Details</span
          ><span class="bs-stepper-subtitle">Setup Account Details</span></span
        >
      </button>
    </div>
    <div class="line"><i data-feather="chevron-right" class="font-medium-2"></i></div>
    <div class="step" data-target="#personal-info">
      <button class="step-trigger">
        <span class="bs-stepper-box">2</span
        ><span class="bs-stepper-label"
          ><span class="bs-stepper-title">Personal Info</span
          ><span class="bs-stepper-subtitle">Add Personal Info</span></span
        >
      </button>
    </div>
  </div>
  <div class="bs-stepper-content">
    <form (ngSubmit)="(HWForm.form.valid)" #HWForm="ngForm">
      <div id="account-details" class="content">
        <form #accountDetailsForm="ngForm">
          <div class="content-header">
            <h5 class="mb-0">Account Details</h5>
            <small class="text-muted">Enter Your Account Details.</small>
          </div>
        </form>
      </div>
      <div id="personal-info" class="content">
        <form #personalInfoForm="ngForm">
          <div class="content-header">
            <h5 class="mb-0">Personal Info</h5>
            <small>Enter Your Personal Info.</small>
          </div>
        </form>
      </div>
    </form>
  </div>
</div>
```

```typescript
import Stepper from 'bs-stepper';
...
...

private horizontalWizardStepper: Stepper;

...
...
...

ngOnInit() : void {
  this.horizontalWizardStepper = new Stepper(document.querySelector('#stepper1'), {});
}
```

## Integration with ngModel

We have used `[(ngModel)]` that creates a FormControl instance from a domain model and binds it to a form control element.

:::tip
The `[(ngModel)]` instance tracks the value, user interaction, and validation status of the control and keeps the view synced with the model
:::

_Example : _

We have an input filed with name & email. We will bind the name & email input field with ngModel.

```html
<input [(ngModel)]="nameVar" type="email" name="email" class="form-control" />

<input [(ngModel)]="emailVar" email type="email" name="email" class="form-control" />
```

```typescript
public nameVar = '';
public emailVar = '';
```

Now, you can dynamically update the `nameVar` & `emailVar` as per your usage. We can also update the variables as of API response.

You can find all demos on [this](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/forms/form-wizard) page.
