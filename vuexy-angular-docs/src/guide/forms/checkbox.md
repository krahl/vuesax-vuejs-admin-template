# Checkbox

The Checkbox component is slightly modified to make it more beautiful

## Custom Checkboxes

Wrap input with class `.custom-control.custom-checkbox` .

```html
<div class="custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="customCheck1" checked />
  <label class="custom-control-label" for="customCheck1">Checked</label>
</div>
```

_Result:_

<img :src="$withBase('/images/images/checkbox/checkbox.gif')" alt="checkbox-colors" class="medium-zoom rounded bordered">

## Colored Checkboxes

Wrap input with class `.custom-control.custom-checkbox` & `.custom-control-{colorName}` .

```html
<div class="custom-control custom-control-primary custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="colorCheck1" checked />
  <label class="custom-control-label" for="colorCheck1">Primary</label>
</div>

<div class="custom-control custom-control-secondary custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="colorCheck2" checked />
  <label class="custom-control-label" for="colorCheck2">Secondary</label>
</div>

<div class="custom-control custom-control-success custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="colorCheck3" checked />
  <label class="custom-control-label" for="colorCheck3">Success</label>
</div>

<div class="custom-control custom-control-danger custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="colorCheck5" checked />
  <label class="custom-control-label" for="colorCheck5">Danger</label>
</div>

<div class="custom-control custom-control-warning custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="colorCheck4" checked />
  <label class="custom-control-label" for="colorCheck4">Warning</label>
</div>

<div class="custom-control custom-control-info custom-checkbox">
  <input type="checkbox" class="custom-control-input" id="colorCheck6" checked />
  <label class="custom-control-label" for="colorCheck6">Info</label>
</div>
```

_Result:_

<img :src="$withBase('/images/images/checkbox/checkbox-themes.jpg')" alt="checkbox-colors" class="medium-zoom rounded bordered">

| Class                         | Value                                                   | Description                           |
| :---------------------------- | :------------------------------------------------------ | :------------------------------------ |
| `.custom-control-{colorName}` | primary / secondary / success / danger / warning / info | To get the desired color of checkbox. |
