# Installation

## Prerequisites

::: tip
This section will help you get ready in order to install the Vuexy Admin. Skip to the [Installation](#installation-2) section to start installing the template as we have already covered Prerequisites there.
:::

#### Node.js & NPM

First of all, make sure you have installed [Node](https://nodejs.org/en/) and [npm](https://www.npmjs.com/).
We won't get in too much detail here as it's out of theme documentation scope. For more details refer to [Downloading and installing Node.js and npm
](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

#### Angular CLI

Vuexy Admin is built using [Angular CLI](https://cli.angular.io/). Angular CLI is nicely documented. You don't need to install or configure tools like Webpack or Babel. They are preconfigured and hidden so that you can focus on the code.

Simply **Angular CLI** makes it easy to create an application that already works, right out of the box. It already follows our best practices!

#### GIT

To install and use Vuexy Admin, you will also need [Git](https://git-scm.com/) installed on your computer. Git is required for npm to work correctly.

## Installation

::: warning
Please make sure you use the node's LTS version which is recommended by the official node site and not one with the latest feature.
:::

### Installing Prerequisites

1. Download & install the latest(LTS) [Node.js](https://nodejs.org/en/) and [npm](https://www.npmjs.com/).
2. Download and install the latest [Git](https://git-scm.com/).
3. Install the Angular CLI: Open Terminal/Command Prompt and run the following command and wait until it finishes.

```bash
npm install -g @angular/cli
```

:::tip
Refer [CLI Overview and Command Reference](https://angular.io/cli) for Angular CLI usage.
:::

### Installing Vuexy Angular

Now, You are ready to install Vuexy Angular Admin. 🎉

1. After downloading zip from ThemeForest, unzip it in your desired location.
2. In the uncompressed folder you will find a folder named `angular-version/`. In this a folder you will find Full Version(full-version.zip) and Starter Kit(starter-kit.zip). It also includes [documentation.html](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/documentation/) file for live documentation for Vuexy Angular.
3. Extract starter-kit.zip/full-version.zip to your app folder. It's preferable to use Starter Kit in order to get started your project from scratch, for more details refer [Starter-kit vs Full Package](/guide/getting-started/starter-full.md).
4. Open Terminal/Command Prompt and navigate to your app folder and run the below command and wait until it finishes.

```bash
# Recommended using --legacy-peer-deps
npm install --legacy-peer-deps

# OR

npm install
```

::: tip
Getting error [Unable to resolve dependency tree errors](/faq/#unable-to-resolve-dependency-tree-errors).

Getting error [FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed](/faq/#fatal-error-ineffective-mark-compacts-near-heap-limit-allocation-failed).

If you have any issues regarding installation please search your issue in our [FAQ](/faq/) section for instant solution and still you can't find a solution, please follow our guide on [getting support](/guide/getting-started/support.md) for our product.
:::

The above command will install all the required Node.js modules into the node_modules directory inside your app folder. While installing this command you may see few warnings, don't worry about that for more details refer to our FAQ [npm install warnings or installation warnings](/faq/#npm-install-or-yarn-install-warnings-or-installation-warnings).

## Server

In your application folder, run the following command in Terminal/Command to run your application

```bash
ng serve --open
```

The `--open` (or just `-o`) option automatically opens your browser to http://localhost:4200/.

**OR**

```bash
ng serve
```

The `ng serve` command launches the server, watches your files, and rebuilds the app as you make changes to those files.

Now you will find some output after running above command in console as below:

<img :src="$withBase('/images/@core/npm-run-serve-result.png')" alt="console-output-of-development-server" class="rounded">

Open [http://localhost:4200/](http://localhost:4200/) to check your application. 🚀

::: warning
If you have another process running at port `4200` then you might have some other URL (PORT) that screenshot.
:::

If the default port 4200 is not available, you can specify another port with the port flag as in the following example:

```bash
ng serve --port 4201
```

To access the application in the the local network, this will allow your application to access all devices which are in the same network.

```bash
ng serve --host=0.0.0.0 --disable-host-check
```

::: tip
Check `package.json` file for other available commands in Vuexy Admin.
:::

## Deployment

When you are ready to deploy your Angular application to a remote server, you have various options for deployment.

#### Basic deployment to a remote server

For the simplest deployment, create a production build and copy the output directory to a web server.

1. Start with the production build:

   ```bash
   ng build --configuration production

   ## using npm
   npm run build:prod
   ```

   This command creates a `dist` folder in the application root directory with all the files that a hosting service needs for serving your application.

2. Copy everything within the output folder (`dist/vuexy/` by default) to a folder on the server.
3. Configure the server to redirect requests for missing files to index.html. Learn more about server-side redirects [below](https://angular.io/guide/deployment#fallback).

This is the simplest production-ready deployment of your application.

:::tip
When you are ready to deploy your Angular application to a remote server, refer official guide for [deployment](https://angular.io/guide/deployment).
:::

#### Running unit tests

Tests will execute after a build is executed via [Karma](http://karma-runner.github.io/6.1/index.html), and it will automatically watch your files for changes.

You can run tests a single time via `--watch=false` or `--single-run`

```bash
ng test
```

#### Running end-to-end tests

Before running the tests make sure you are serving the app via `ng serve`. End-to-end tests are run via [Protractor](https://protractor.angular.io/).

```bash
ng e2e
```
