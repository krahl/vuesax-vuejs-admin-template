# Menu (Navigation)

In this page you will find how to add/update menu(navigation) items. Vuexy Admin use `menu.ts` file to manage the template menu.

:::tip
Both Vertical and Horizontal menu type use the single menu file `menu.ts` for menu data.
:::

## Interface

Before updating the menu, first let's understand the Menu Interface (Options). Each menu item has below options are available.

```ts
export interface CoreMenuItem {
  id: string
  title: string
  url?: string
  type: 'section' | 'collapsible' | 'item'
  role?: Array<string>
  translate?: string
  icon?: string
  disabled?: boolean
  hidden?: boolean
  classes?: string
  exactMatch?: boolean
  externalUrl?: boolean
  openInNewTab?: boolean
  badge?: {
    title?: string
    translate?: string
    classes?: string
  }
  children?: CoreMenuItem[]
}
```

Refer the below table for the more details on menu Interface properties.

| Properties              | Data Type                          | Description                                    |
| ----------------------- | ---------------------------------- | ---------------------------------------------- |
| id                      | string                             | Unique menu item id                            |
| title                   | string                             | Menu item title (name)                         |
| url (optional)          | string                             | Menu item url                                  |
| type                    | 'section' , 'collapsible' , 'item' | Menu item type                                 |
| role (optional)         | Array                              | Set role based permission to display menu item |
| translate (optional)    | string                             | Translate id used in i18n language file        |
| icon (optional)         | string                             | Menu item icon                                 |
| disabled (optional)     | boolean                            | Set true to disabled menu item                 |
| hidden (optional)       | boolean                            | Set true to hide menu item                     |
| classes (optional)      | string                             | Set Custom class for menu item                 |
| exactMatch (optional)   | boolean                            | Set routerLinkActiveOptions                    |
| externalUrl (optional)  | boolean                            | Set true for external menu item url            |
| openInNewTab (optional) | boolean                            | Set true to open menu item url in new tab      |
| badge (optional)        | object                             | Set menu item badge                            |

## Usage

Vuexy Angular admin use `menu.ts` file to easily manage the template menu. This file can be use for the Vertical or Horizontal menu type.

Below is the example code which will help you to craft menu with possible menu options. In this example we have used Menu Interface options such as id, types, title, translation, role, icon, url, badge, disabled, externalUrl and openInNewTab.

::: tip
Use `menu/i18n/en.ts | fr.ts | de.ts | pt.ts` files to setup translation based on language. Set the translations for the menu from `src/app/app.component.ts` file. For more detail on translation refer [Translation](/guide/development/translation.html)
:::

```ts

{
  id: 'charts-maps',
  type: 'section',
  title: 'Charts & Maps',
  translate: 'MENU.CM.SECTION',
  icon: 'bar-chart-2',
  children: [
    {
      id: 'charts',
      title: 'Charts',
      translate: 'MENU.CM.CHARTS.COLLAPSIBLE',
      type: 'collapsible',
      // role: ['Admin'], // To hide collapsible based on user role
      icon: 'pie-chart',
      badge: {
        title: '2',
        translate: 'MENU.CM.CHARTS.BADGE',
        classes: 'badge-light-danger badge-pill'
      },
      children: [
        {
          id: 'apex',
          title: 'Apex',
          translate: 'MENU.CM.CHARTS.APEX',
          role: ['Admin'], // To set multiple role: ['Admin', 'Client']
          type: 'item',
          icon: 'circle',
          url: 'charts-and-maps/apex'
        },
        {
          id: 'chartJs',
          title: 'ChartJS',
          translate: 'MENU.CM.CHARTS.CHARTJS',
          type: 'item',
          icon: 'circle',
          url: 'charts-and-maps/chartjs'
        }
      ]
    },
    {
      id: 'disabled-menu',
      title: 'Disabled Menu',
      translate: 'MENU.OTHERS.DISABLED',
      icon: 'eye-off',
      type: 'item',
      url: '#',
      disabled: true
    },
    {
      id: 'documentation',
      title: 'Documentation',
      translate: 'MENU.OTHERS.DOCUMENTATION',
      icon: 'file-text',
      type: 'item',
      url: 'https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/documentation',
      externalUrl: true,
      openInNewTab: true
    },
  ]
},
```

::: warning
Menu `role` property is used to display menu `type` i.e **items, collapsible & section** based on user role.

It is not same as managing routes with role based access. Router can actually redirect user based on role.
:::
