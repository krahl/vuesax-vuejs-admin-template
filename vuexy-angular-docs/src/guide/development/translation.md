# Translation (Multi Language)

Vuexy Angular uses [ngx-translate](https://github.com/ngx-translate/core) internationalization (i18n) library for translation.

:::tip
Simple example using [ngx-translate](https://stackblitz.com/github/ngx-translate/example)
:::

### Usage

To use the translation, it is required to create translation file with in the module you want to use translation. For example, Vuexy Angular use translation in `src/app/main/dashoard`. You need to create translation files for each different language under `dashboard/i18n` folder.

It is important that you follow the structure of the translation file and it must define the language id along with the translation data:

```ts
// i18n/en.ts
export const locale = {
  lang: 'en',
  data: {
    SAMPLE: {
      CONGRATULATIONS: 'Congratulations',
      BADGE: 'You have won gold medal'
    }
  }
}

// i18n/fr.ts
export const locale = {
  lang: 'fr',
  data: {
    SAMPLE: {
      CONGRATULATIONS: 'Toutes nos félicitations',
      BADGE: "Vous avez remporté la médaille d'or"
    }
  }
}
```

Once you create the translation file, use `CoreTranslationService` to easily translate your application. For dashboard open `ecommerce.component.ts` file

```ts
// Import translation service
import { CoreTranslationService } from '@core/services/translation.service';
// Import the locale files
import { locale as english } from 'app/main/dashboard/i18n/en';
import { locale as french } from 'app/main/dashboard/i18n/fr';
import { locale as german } from 'app/main/dashboard/i18n/de';
import { locale as portuguese } from 'app/main/dashboard/i18n/pt';

export class EcommerceComponent implements OnInit {
  /**
   * Constructor
   * ...
   * @param {CoreTranslationService} _coreTranslationService
   */
  constructor(
    private _coreTranslationService: CoreTranslationService
  ) {
    ...

    this._coreTranslationService.translate(english, french, german, portuguese);
  }

  ...

}

```

:::tip
If you are using starter-kit, It is not required to integrate [ngx-translate](https://github.com/ngx-translate/core) as we have not removed [ngx-translate](https://github.com/ngx-translate/core) from it. Just commented Language selection code.
:::

## Change default language

Vuexy Admin allows you to easily change the Application default language from `app/app-config.ts` file. Let's assume that we want to set the default French then change `appLanguage` property value to `fr`.

::: danger Important
If the enableLocalStorage option is true then make sure you clear the browser [local storage](https://developers.google.com/web/tools/chrome-devtools/storage/localstorage#delete). Otherwise, it will not take the below config changes and use stored config from local storage.
:::

```ts{7}
// prettier-ignore
export const coreConfig: CoreConfig = {
  app: {
    appName     : 'Vuexy',                                        // App Name
    appTitle    : 'Vuexy - Angular 11+ Bootstrap Admin Template', // App Title
    appLogoImage: 'assets/images/logo/logo.svg',                  // App Logo
    appLanguage : 'en',                                           // App Default Language (en, fr, de, pt etc..)
  },
  layout: {
    ...
  }
}
```
