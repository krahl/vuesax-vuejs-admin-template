# Routing

This section introduces the basic procedure for configuring a lazy-loaded route.

:::tip
Vuexy Angular Admin Template provides built-in route transition animation. For more click [here](/guide/development/route-animation.html).
:::

## Overview

Vuexy Angular admin uses the [lazy loading](https://angular.io/guide/lazy-loading-ngmodules).
Find our template's main router configuration in `src/app/app.module.ts` file. It includes the sub-module file, which has its own routes (i.e dashboard module).

::: tip
Refer to official Angular Route [Properties](https://angular.io/api/router/Route)
:::

```ts
const appRoutes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('./main/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: '',
    redirectTo: '/dashboard/ecommerce',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/pages/miscellaneous/error' //Error 404 - Page not found
  }
  ....
]
```

- Refer to the `dashboard` routs and follow the same pattern to create Routs for different modules.
- `path: ''` handles the application initial redirect, which means once you open load app it will redirect to `/dashboard/ecommerce` change it as per your preference.
- `path: '**'` handles the application URL which is not valid and redirects to page not found (Error 404).

## Route Protection (Role-Based)

Add route protection with role-based access for the particular route. You can add Router Protection using Auth Guard service.

```ts
const routes = [
  {
    path: 'analytics',
    component: AnalyticsComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] },
    resolve: {
      css: DashboardService,
      inv: InvoiceListService
    }
  },
  {
    path: 'ecommerce',
    component: EcommerceComponent,
    canActivate: [AuthGuard],
    resolve: {
      css: DashboardService
    }
  }
]
```

::: tip
Refer [Authentication](/guide/development/authentication.html#jwt) for more details on JWT Role-Based access usage.
:::

Refer to the above code which we have used in `dashboard.module.ts`.

- Use `canActivate` property to enable router protection (Can't accessible unless the user is logged in). Use it on root/submodule as per requirement.
- Use `data` property to defines the `role` based access. i.e In Analytics Dashboard we have to assign Admin Role `data: { roles: [Role.Admin] }` to make it accessible to Admin users only.
  - If you do not use this property then the route is accessible to every logged-in user.
  - You can also assign specific routes to multiple users i.e `data: { roles: [Role.Admin, Role.Client] }`
- Don't use Router Protection for pages that should be accessible in order to use your application i.e Login, Register, Page not found, etc...
