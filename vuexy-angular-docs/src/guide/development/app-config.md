# App Config

Customize the template easily with the help of `app-config.ts` file.

## Usage

Customize your application with the help of `app-config.ts`. It has `app` and `layout` objects.

- `app` object properties define the application name, title, and logo as shown in the below code.
- `layout` object properties define the skin, layout type, menu, navbar, footer, and other misc options.

`app-config.ts` options and their valid values are already listed here. As it is self-explanatory. Just changed configuration as your requirement and you are done.

::: tip
Use the template customizer in our live demo to check the config options before defining app config options.
:::

::: danger Important
If the enableLocalStorage option is true then make sure you clear the browser [local storage](https://developers.google.com/web/tools/chrome-devtools/storage/localstorage#delete). Otherwise, it will not take the below config changes and use stored config from local storage.
:::

```ts
// prettier-ignore
export const coreConfig: CoreConfig = {
  app: {
    appName     : 'Vuexy',                                        // App Name
    appTitle    : 'Vuexy - Angular 11+ Bootstrap Admin Template', // App Title
    appLogoImage: 'assets/images/logo/logo.svg',                  // App Logo
    appLanguage : 'en',                                           // App Default Language (en, fr, de, pt etc..)
  },
  layout: {
    skin  : 'default',                        // default, dark, bordered, semi-dark
    type  : 'vertical',                       // vertical, horizontal
    animation : 'fadeIn',                     // fadeInLeft, zoomIn , fadeIn, none
    menu : {
      hidden               : false,           // Boolean: true, false
      collapsed            : false,           // Boolean: true, false
    },
    // ? For horizontal menu, navbar type will work for navMenu type
    navbar: {
      hidden               : false,           // Boolean: true, false
      type                 : 'floating-nav',  // navbar-static-top, fixed-top, floating-nav, d-none
      background           : 'navbar-light',  // navbar-light. navbar-dark
      customBackgroundColor: true,            // Boolean: true, false
      backgroundColor      : ''               // BS color i.e bg-primary, bg-success
    },
    footer: {
      hidden               : false,           // Boolean: true, false
      type                 : 'footer-static', // footer-static, footer-sticky, d-none
      background           : 'footer-light',  // footer-light. footer-dark
      customBackgroundColor: false,           // Boolean: true, false
      backgroundColor      : ''               // BS color i.e bg-primary, bg-success
    },
    enableLocalStorage: true,
    customizer  : true,                       // Boolean: true, false (Enable theme customizer)
    scrollTop   : true,                       // Boolean: true, false (Enable scroll to top button)
    buyNow      : false                       // Boolean: true, false (Set false in real project, For demo purpose only)
  }
}
```

## Page specific cutomization

`app-config.ts` config options customize the template globally(In the whole application).
If you want to override/customize per page/component you can also achieve it. To set the [collapsed menu](https://pixinvent.com/demo/vuexy-angular-admin-dashboard-template/demo-1/ui/page-layouts/collapsed-menu) on the specific page, first of all, you need to import `CoreConfigService`

```ts
import { CoreConfigService } from '@core/services/config.service'
```

And create the `CoreConfigService` constructor and set up the config options.

```ts
/**
 * Constructor
 *
 * @param {CoreConfigService} _coreConfigService
 */
constructor(private _coreConfigService: CoreConfigService) {
  this._unsubscribeAll = new Subject();

  // Configure the layout
  this._coreConfigService.config = {
    layout: {
      menu: {
        collapsed: true
      },
      customizer: true,
      enableLocalStorage: false
    }
  };
}
```
