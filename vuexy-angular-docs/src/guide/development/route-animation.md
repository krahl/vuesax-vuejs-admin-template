# Route Animation

Routing enables users to navigate between different routes in an application. When a user navigates from one route to another, the Angular router maps the URL path to a relevant component and displays its view. Animating this route transition can greatly enhance the user experience.

## Usage

Route Transition animation is already included in full version and starter-kit so you don't have to include it separately.

:::tip
If you do not want to use route transition animation then, you can **disable** it by just passing `none` to `animation` property in `app-config.ts`. _Example:_ `animation: 'none'`

Fore more details refer [App Config](/guide/development/app-config.html)
:::

:::warning
Route transition animation will only work when you pass a unique name to `animation` property to routes array. For _Example :_

```typescript{5}
const routes: Routes = [
  {
    path: 'example',
    component: exampleComponent,
    data: { animation: 'uniqueName' }
  }
]
```

:::

## Default Route Animation

By default we have used `fadeIn` animation.

## Change Route Animation

::: warning
Make sure you clear the browser [local storage](https://developers.google.com/web/tools/chrome-devtools/storage/localstorage#delete). If the enableLocalStorage option is true, else it will not take the below config changes and use stored config from local storage.
:::

:::tip
Vuexy Angular Admin provides 3 in-built route transition animation `fadeInLeft`, `zoomIn` & `fadeIn`. You can use anyone of them.
:::

To change the route transition animation navigate to :
`vuexy-angular-full-version\src\app\app-config.ts` (full-version)

or

`vuexy-angular-starter-kit\src\app\app-config.ts` (starter-kit-version)

In `coreConfig` you will find `animation` property inside `layout` object.

Just update the default(`fadeIn`) to the desired `fadeInLeft`, `zoomIn` , `fadeIn`, `none`.

```typescript{10}
export const coreConfig: CoreConfig = {
  app: {
    appName: 'Vuexy', // App Name
    appTitle: 'Vuexy - Angular 11+ Bootstrap Admin Template', // App Title
    appLogoImage: 'assets/images/logo/logo.svg' // App Logo
  },
  layout: {
    skin: 'default', // default, dark, bordered, semi-dark
    type: 'vertical', // vertical, horizontal
    animation: 'fadeIn', // fadeInLeft, zoomIn , fadeIn, none
    menu: {
      hidden: false, // Boolean: true, false
      collapsed: false // Boolean: true, false
    },
    // ? For horizontal menu, navbar type will work for navMenu type
    navbar: {
      hidden: false, // Boolean: true, false
      type: 'floating-nav', // navbar-static-top, fixed-top, floating-nav, d-none
      background: 'navbar-light', // navbar-light. navbar-dark
      customBackgroundColor: true, // Boolean: true, false
      backgroundColor: '' // BS color i.e bg-primary, bg-success
    },
    footer: {
      hidden: false, // Boolean: true, false
      type: 'footer-static', // footer-static, footer-sticky, d-none
      background: 'footer-light', // footer-light. footer-dark
      customBackgroundColor: false, // Boolean: true, false
      backgroundColor: '' // BS color i.e bg-primary, bg-success
    },
    enableLocalStorage: true,
    customizer: true, // Boolean: true, false (Enable theme customizer)
    scrollTop: true, // Boolean: true, false (Enable scroll to top button)
    buyNow: true // Boolean: true, false (Set false in real project, For demo purpose only)
  }
}
```

Learn more about app-config from [here.](/guide/development/app-config.html)

## Customization

**Step 1** : Add your custom animation.

Navigate to file :

`vuexy-angular-full-version\src\@core\animations\core.animation.ts` (full-version)

or

`vuexy-angular-starter-kit\src\@core\animations\core.animation.ts` (starter-kit-version)

Here, add your custom animation.

**Step 2** : Update animations options on route component.

Navigate to file :

`vuexy-angular-full-version\src\app\layout\components\content\content.component.html` (full-version)

or

`vuexy-angular-starter-kit\src\app\layout\components\content\content.component.html` (starter-kit-version)

```html{5}
<div
  [@zoomIn]="zoomIn(outlet)"
  [@fadeInLeft]="fadeInLeft(outlet)"
  [@fadeIn]="fadeIn(outlet)"
  [@exampleCustomAnimation]="exampleCustomAnimation(outlet)"
>
  <router-outlet #outlet="outlet"></router-outlet>
</div>
```

Navigate to file :

`vuexy-angular-full-version\src\app\layout\components\content\content.component.ts` (full-version)

or

`vuexy-angular-starter-kit\src\app\layout\components\content\content.component.ts` (starter-kit-version)

```typescript{5,24-30}
@Component({
  selector: 'content',
  templateUrl: './content.component.html',
  encapsulation: ViewEncapsulation.None,
  animations: [fadeInLeft, zoomIn, fadeIn, exampleCustomAnimation] // Add your custom animation here
})

...
...
...

  /**
   * Fade In Animation
   *
   * @param outlet
   */
  fadeIn(outlet) {
    if (this.animate === 'fadeIn') {
      return outlet.activatedRouteData.animation;
    }
    return null;
  }

// your customAnimation Method
  exampleCustomAnimation(outlet) {
    if (this.animate === 'exampleCustomAnimation') {
      return outlet.activatedRouteData.animation;
    }
    return null;
  }

...
...

```

**Step 3** : Set custom animation in app-config.

Navigate to :

`vuexy-angular-full-version\src\app\app-config.ts` (full-version)

or

`vuexy-angular-starter-kit\src\app\app-config.ts` (starter-kit-version)

In `coreConfig` you will find `animation` property inside `layout` object.

Just add the `animation : exampleCustomAnimation`.

_Example :_

```typescript{7}
export const coreConfig: CoreConfig = {
  ...
  ...
  layout: {
    skin  : 'default',                        // default, dark, bordered, semi-dark
    type  : 'vertical',                       // vertical, horizontal
    animation : 'exampleCustomAnimation',     // fadeInLeft, zoomIn , fadeIn, none, exampleCustomAnimation
    menu : {
      hidden               : false,           // Boolean: true, false
      collapsed            : false,           // Boolean: true, false
    },
  ...
  ...
  }
}
```

You have successfully integrated custom route transition animation.
