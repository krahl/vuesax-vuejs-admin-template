# Splash(Loading) Screen

Vuexy now provides splash screen for heavy projects which requires more time to load. Splash screen service will get initialized whenever the application gets initialized. Splash screen will appear until all the template gets rendered completely.

## Usage

Splash screen is already included in full version and starter-kit so you don't have to include it separately.

## Customization

### Logo

Please update your company logo in public directory to show your desired loading. One can easily change the loading screen logo & animation(wit hthe help of css animation).

Open `src\index.html` file and update logo name/path.

```html
<!-- Vuexy loading Screen -->
<core-loading-screen id="loading-bg">
  <div class="loading-logo">
    <img src="assets/images/logo/logo.png" alt="Logo" />
  </div>
  <div class="loading">
    <div class="effect-1 effects"></div>
    <div class="effect-2 effects"></div>
    <div class="effect-3 effects"></div>
  </div>
</core-loading-screen>
<!-- / Vuexy loading Screen -->
```

In the same `src\index.html` file it has loading screen animation style, update it as per your requirement.

```html
<!-- Vuexy loading Screen CSS -->
<style type="text/css"></style>
<!--/ Vuexy loading Screen CSS -->
```
