# Folder Structure

Vuexy Angular has a very simple folder structure to understand it easily.

::: warning
`@core` folder contains core files of the template. It is not recommended to change anything inside `@core` folder unless you know it OR our support team guide you to do it.
:::

Below is the Vuexy Angular **full-version.zip** folder structure explanation for better understanding. Once you unzip it you will find the belwo folder structure.

```
├── dist                     -> Contains the built app
├── e2e                      -> End-to-end tests
├── src                      -> Source files for the root-level app project
│   ├── @core                -> Contains core elements of Vuexy
│   ├── @fake-db             -> Fake Database for Angular in-memory-web-api (Fake API Calls)
│   ├── app                  -> Your app folder
│   │   ├── auth             -> Contains the authentication files
│   │   ├── layout           -> Contains the template layouts elements
│   │   │   ├── components   -> Layout components i.e navbar, menu, footer etc..
│   │   │   ├── horizontal   -> Horizontal layout setup
│   │   │   └── vertical     -> Vertical layout setup
│   │   ├── main             -> Contains Vuexy's ready-to-use apps, pages, dashboards, forms, etc...
│   │   ├── menu             -> Application main menu (navigation)
│   │   │   ├── i18n         -> Contains menu translation files
│   │   │   └── menu.ts      -> Contains menu data (structure)
│   │   ├── app-config.ts    -> App config for app customization
│   │   └── colors.const.ts  -> Colors constants used in app (i.e used in charts)
│   ├── assets               -> Contains app assets
│   │   ├── fonts
│   │   ├── images
│   │   ├── scss            -> Contains scss files to override variables and styles
│   │   │   ├── variables
│   │   │   │   ├── _variables-components.scss -> Custom component variable
│   │   │   │   └── _variables.scss            -> Bootstrap variable
│   │   │   └── styles.scss -> Custom styles
│   ├── environments
│   ├── favicon.ico         -> App favicon
│   ├── index.html          -> The main HTML page that is served
│   └── styles.scss         -> Lists CSS files that supply styles for a project.
├── .browserslistrc         -> Configures sharing of target browsers
├── angular.json            -> Angular CLI configuration
├── LICENSE                 -> Theme license file
├── package.json            -> Configures npm package dependencies
├── README.md               -> Introductory documentation
├── tsconfig.json           -> TypeScript configuration
└── tslint.json             -> TSLint configuration
```

::: tip
Vuexy Angular documentation can not explain every small part of the angular app. For detailed information [Workspace and project file structure
](https://angular.io/guide/file-structure)
:::

## `src/`

This folder contains source files for the root-level application project.

- `@core/` folder contains core files of the template which **shall not get modified** unless our support team guide you to do it. Apart from the `@core` you can modify any files as per your requirement.
- `@fake-db/` folder just contains dummy data which we get in response of fake API call. This enables us to take step forward in providing **API ready template**.

## `app/`

- `auth/` folder contains Authentication `helpers/`, `modals/` & `services/` which help us to manage JWT authentication with role-based access. For more detail refer [Authentication](/guide/development/authentication.html#jwt)
- `layout/` folder contains `vertical` & `horizontal` layouts and components i.e navbar, menu, footer etc... For more details refer [Layout](/guide/development/layout.html#layout)
- `main/` folder contains Vuexy Angular ready to use apps, dashboard, pages, charts, cards and forms.
- `menu/` folder contains main menu(navigation) data with translations. Translations files are in `i18n/` folder. For more details refer [Menu](/guide/development/navigation-menus.html)
- `app-config.ts` The config file for configuring the Vuexy template. For more details refer [App Config](/guide/development/app-config.html#app-config)
- `colors.const.ts` The color constant file which are used in app (i.e used in charts).

<!--// prettier-ignore
## @core folder

`@core` folder is core of our template which includes core files like layouts, composition functions, template components, etc.

**@core folder isn't meant to get modified.** When you will update of our template replacing this `@core` folder will hopefully update the template with minimum changes. This is improvement over our old structure which will ease your update process.

It's good idea to have a look at it and know what it contains to use stuff we already invented so **you don't have to reinvent the wheel**.

e.g. we created composition function for form validation which make 3rd party validation (vee-validate) easy to use.

### Understanding Core folder

Understanding `@core` folder will save your development time and you will know how to get most of our template.

- **app-config**

This folder contains composition function for app-config store module which make modifying themeConfig easy.

All you have to do is import this and update the property. Under the hood it will handle all store mutations and other actions like updating localStorage or updating other config.

```js{5,8}
import useAppConfig from '@core/app-config/useAppConfig'

export default {
  setup() {
    const { skin } = useAppConfig()

    const setDarkTheme = () => {
      skin.value = 'dark'
    }

    return { setDarkTheme }
  }
}
```

::: tip
Refer to [theme configuration](/guide/development/theme-configuration.md) section to get all valid values for config option.
:::

- **assets**

Currently this folder just contains feather font icons.

:::danger
In next release, feather font icon assets may get moved outside of @core folder.
:::

- **auth**

This folder contains auth related files. Currently, It has JWT service and other supporting files. Please refer to [JWT page](/guide/development/authentication.md) for getting detailed information on this.

- **comp-functions**

It contains some useful composition function. You can find detailed explanation on this in [composition function](/#) page.

- **components**

It contains core component of template. Make sure to check them all in our custom components section.

- **directives**

As the name suggest this contains directives of our template. Currently, it only contains animation directive and we only have one directive for alert for now. You can find usage of this directive in our [alert animation demo](/guide/components/alert.md#alert-animation).

- **layouts**

This contains layouts of our template and layout components. You can find detailed information in [layout](/guide/development/layout.md) page.

- **mixins**

This contains vue mixins. It there till we completely migrate to Vue 3 where we will convert them to composition functions.

- **scss**

This folder contains core styles of template. You can find more details and folder structure of it in [SCSS folder structure](/guide/development/template-styles.md) section.

- **utils**

It has some utils which can be useful in your development. It's `validations` folder contains vee-validate validators which you can use in form validation. Other than this everything else in folder is self explanatory.

However, `filter.js` file is just filter (Vue 2 template filter) functions. -->
