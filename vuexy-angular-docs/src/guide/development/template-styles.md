# Template Styles

This section will help you to understand the template styles and how to customize/override them.

## Core styles

`@core/scss` folder contains the template's core styles. In this folder you will:

- `base/` Folder contains the template's base style except for any framework (In this case - Angular). e.g. Bootstrap components' styles, template layout styles, etc. Import `base/` folder scss (i.e `base/pages/page-pricing.scss`) in page/component and extend or override it.
- `angular/` Folder contains styles for Angular version-specific styles which isn't part of base styles. e.g. Some Angular extension styles, Ng bootstrap extends Angular specific page styles, etc.
- `core.scss` This file will get included in `styles.scss` file. This file imports `base/`, required plugins, and `angular/` styles. Consider it as the main file for template styles.

::: tip
All SCSS files starting with `_`[underscore] (SCSS partials) are already imported in template styles so there's no need to import it. Other SCSS Files are meant to get imported only if they are required.
:::

::: warning
It is not recommended to change any scss file inside the `@core/` folder, to customize the template variable and style read the below section **Style Customization**.
:::

## Style Customization

Easily customize the template variables and styles to change the look and feel of the template as per the branding/requirement.

Under the `src/assets/scss` folder you can use

- `style.scss`file to write/override your own styles. This file is imported in `src/style.scss` which is the main entry point style of the template.
- `variables/_variables.scss`: Use this file to override bootstrap variables using this file. (i.e change \$primary color)
- `variables/_variables-components.scss`: Use this file to override component variables. (i.e change \$base-font-size)
